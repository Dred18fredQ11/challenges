import { useState, useEffect } from 'react'
import BannerWidget from 'components/BannerWidget'
import NotificationWidget from 'components/NotificationWidget'
import { emitWidgetInit, subscribeToInit } from 'socket'

function App() {
  // State
  const [isLoading, setIsLoading] = useState(true)
  const [widgetApp, setWidgetApp] = useState()
  const [elementMap, setElementMap] = useState()
  const [challenge, setChallenge] = useState()

  // Fetch widget data
  useEffect(() => {
    const url = new URL(window.location)
    
    // Subscribe to events
    subscribeToInit(({ widgetApp, elementList, challenge }) => {
      const elementMap = elementList.reduce((acc, element) => {
        return { ...acc, [element.name]: element }
      }, {})
      setWidgetApp(widgetApp)
      setElementMap(elementMap)
      setChallenge(challenge)
      setIsLoading(false)
    })

    // Emit events
    emitWidgetInit({ token: url.searchParams.get('token') })
  }, [])

  if (isLoading) {
    return <div></div>
  }

  return (
    <div>
      {widgetApp?.name === 'main' && (
        <BannerWidget elementMap={elementMap} challenge={challenge} />
      )}

      {widgetApp?.name === "notification" && (
        <NotificationWidget elementMap={elementMap} />
      )}
    </div>
  )
}

export default App
