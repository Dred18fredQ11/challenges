import io from 'socket.io-client'

// Socket instance
const socket = io(process.env.REACT_APP_SOCKET)

/**
 * Emit wiget data request
 * @param {{ token: string }} values 
 */
export function emitWidgetInit({ token }) {
  socket.emit('widget:init', token)
}

/**
 * Subsribe callback function to socket event `widget:init`
 * @param {Function} callback
 */
export function subscribeToInit(callback) {
  if (!socket) return
  socket.on('widget:init', response => callback(response))
}

/**
 * Subscribe callback function to socket event `widget:notification`
 * @param {Function} callback 
 */
export function subscribeToNotification(callback) {
  if (!socket) return
  socket.on('widget:notification', response => callback(response))
}

/**
 * Subscribe callback function to socket event `widget:show`
 * @param {Function} callback 
 */
export function subscribeToShow(callback) {
  if (!socket) return
  socket.on('widget:show', response => callback(response))
}

/**
 * Subscribe callback function to socket event `widget:hide`
 * @param {Function} callback 
 */
export function subscribeToHide(callback) {
  if (!socket) return
  socket.on('widget:hide', response => callback(response))
}
