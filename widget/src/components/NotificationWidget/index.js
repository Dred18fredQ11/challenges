import { useState, useEffect } from 'react'
import { subscribeToNotification } from 'socket'
import { soundManager } from 'soundmanager2'
import classnames from 'classnames'
import styles from './styles.module.css'

function mapFontStyles(element) {
  return {
    color: element?.color,
    fontFamily: element?.fontFamily,
    fontSize: element?.fontSize,
    fontStyle: element?.fontStyle,
    fontWeight: element?.fontWeight,
    textShadow: element?.textShadow,
  }
}

export default function NotificationWidget({ elementMap }) {
  const ANIMATION_TIME = 300;
  const SHOW_TIME = 10300;
  
  const [isTimeout, setIsTimeout] = useState(false)
  const [isActive, setIsActive] = useState(false)
  const [title, setTitle] = useState('')
  const [text, setText] = useState('')
  const [challengeList, setChallengeList] = useState([])

  useEffect(() => {
    soundManager.setup({ })

    subscribeToNotification(({ description, creatorName, speachUrl }) => {
      console.log('notification')
      setChallengeList(list => {
        console.log(description, creatorName, speachUrl)
        return [...list, { description, creatorName, speachUrl }]
      })
    })
  }, [])

  useEffect(() => {
    if (challengeList.length === 0 || isTimeout) return
    setIsTimeout(true)
    setTimeout(() => {
      setTitle(challengeList[0].creatorName)
      setText(challengeList[0].description)
      setIsActive(true)

      try {
        const sound = soundManager.createSound({
          url: challengeList[0].speachUrl
        })
        sound.play()
      } catch(err) {
        console.log(err)
      }

      // if (soundManager.canPlayURL(challengeList[0].speachUrl)) {
      // } else { console.log('CANNOT PLAY') }

      setTimeout(() => {
        setIsActive(false)
        setChallengeList(list => list.slice(1))
        setIsTimeout(false)
      }, SHOW_TIME)
    }, ANIMATION_TIME)
  }, [challengeList, isTimeout])

  const wrapperClass = classnames(styles.wrapper, isActive && styles.active)

  return (
    <div className={wrapperClass}>
      <h2 style={mapFontStyles(elementMap.title)}>{title}</h2>
      <p style={mapFontStyles(elementMap.text)}>{text}</p>
    </div>
  )
}
