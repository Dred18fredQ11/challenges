import { useState, useEffect } from 'react'
import { subscribeToShow, subscribeToHide, subscribeToInit } from 'socket'
import classnames from 'classnames'
import styles from './styles.module.css'

function mapFontStyles(element) {
  return {
    color: element?.color,
    fontFamily: element?.fontFamily,
    fontSize: element?.fontSize,
    fontStyle: element?.fontStyle,
    fontWeight: element?.fontWeight,
    textShadow: element?.textShadow,
  }
}

export default function BannerWidget({ elementMap, challenge }) {
  console.log(challenge)
  const [isActive, setIsActive] = useState(Boolean(challenge))
  const [title, setTitle] = useState(challenge?.creatorName ?? '')
  const [text, setText] = useState(challenge?.description ?? '')

  useEffect(() => {
    subscribeToShow(({ description, creatorName }) => {
      console.log(description, creatorName)
      setTitle(creatorName)
      setText(description)
      setIsActive(true)
    })

    subscribeToHide(() => {
      setIsActive(false)
    })
  }, [])

  const wrapperClass = classnames(styles.wrapper, isActive && styles.active)
  return (
    <div className={wrapperClass}>
      <h2 style={mapFontStyles(elementMap.title)}>{title}</h2>
      <p style={mapFontStyles(elementMap.text)}>{text}</p>
    </div>
  )
}
