import axios from 'axios'
import { appendJWT } from 'services/utils'

export const fetchUser = async () => {
  const res = await axios.post(`/api/auth?_=${Date.now()}`, {}, appendJWT())
  return res?.data?.data
}

export const requestLogout = async () => {
  const config = { headers: { 'Cache-Control': 'no-cache' }}
  await axios.get(`/api/auth/logout?_=${Date.now()}`, config)
}

export async function setMinPaymentSum({ sum, currency }) {
  const config = { headers: { 'Content-Type': 'application/json' } }
  const body = { sum, currency }
  const res = await axios.put('/api/user/minPayment', body, appendJWT(config))
  return res?.data?.data
}
