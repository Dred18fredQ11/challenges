import axios from 'axios'
import { appendJWT } from 'services/utils'

export async function fetchWidgetAppList() {
  const config = { headers: { 'Cache-Control': 'no-cache' } }
  const url = `/api/widget/appList?_=${Date.now()}`
  const res = await axios.get(url, appendJWT(config))
  return res?.data?.data
}

export async function fetchWidgetList() {
  const config = { headers: { 'Cache-Control': 'no-cache' } }
  const res = await axios.get(`/api/widget/widgetList?_=${Date.now()}`, appendJWT(config))
  return res?.data?.data
}

export async function fetchWidgetElementList() {
  const config = { headers: { 'Cache-Control': 'no-cache' } }
  const res = await axios.get(`/api/widget/elementList?_=${Date.now()}`, appendJWT(config))
  return res?.data?.data
}

//---

export async function fetchWidgetApp({ widgetAppId }) {
  const config = { headers: { 'Cache-Control': 'no-cache' } }
  const url = `/api/widget/app/${widgetAppId}`
  const res = await axios.get(url, appendJWT(config))
  return res?.data?.data
}

export async function fetchWidget({ widgetAppId, widgetId }) {
  const config = { headers: { 'Cache-Control': 'no-cache' } }
  const url = `/api/widget/app/${widgetAppId}/widget/${widgetId}`
  const res = await axios.get(url, appendJWT(config))
  return res?.data?.data
}

export async function showWidget({  widgetId, challengeId }) {
  const config = { headers: { 'Content-Type': 'application/json' } }
  const body = { challengeId }
  const url = `/api/widget/widget/${widgetId}/show`
  const res = await axios.put(url, body, appendJWT(config))
  return res?.data?.data
}

export async function hideWidget({  widgetId }) {
  const url = `/api/widget/widget/${widgetId}/hide`
  const res = await axios.put(url, {}, appendJWT())
  return res?.data?.data
}

export async function fetchWidgetElement({ widgetAppId, widgetId, elementId }) {
  const config = { headers: { 'Cache-Control': 'no-cache' } }
  const url =
    `/api/widget/app/${widgetAppId}/widget/${widgetId}` +
    `/element/${elementId}`
  const res = await axios.get(url, appendJWT(config))
  return res?.data?.data
}

export async function updateWidgetElement({ elementId, fields }) {
  const config = { headers: { 'Content-Type': 'application/json' } }
  const body = { fields }
  const res = await axios.put(`/api/widget/element/${elementId}`, body, appendJWT(config))
  return res?.data?.data
}
