import axios from 'axios'
import { appendJWT } from 'services/utils'

export async function fetchChallengeCount() {
  const config = { headers: { 'Cache-Control': 'no-cache' } }
  const url = `/api/challenge/count?_=${Date.now()}`
  const res = await axios.get(url, appendJWT(config))
  return res?.data?.data
}

export async function fetchChallengeList({ limit, offset }) {
  const config = {
    headers: { 'Cache-Control': 'no-cache' },
    params: { limit, offset },
  }
  const url = `/api/challenge/challengeList?_=${Date.now()}`
  const res = await axios.get(url, appendJWT(config))
  return res?.data?.data
}

export async function updateChallengeStatus({ challengeId, newStatus }) {
  const config = { headers: { 'Content-Type': 'application/json' } }
  const body = { newStatus }
  const url = `/api/challenge/challenge/${challengeId}/updateStatus`
  const res = await axios.put(url, body, appendJWT(config))
  return res?.data?.data
}

export async function hideChallenge({ challengeId }) {
  const url = `/api/challenge/challenge/${challengeId}/hide`
  const res = await axios.put(url, {}, appendJWT())
  return res?.data?.data
}

export async function speachChallenge({ challengeId }) {
  const url = `/challenge/${challengeId}/speach`
  const res = await axios.post(url, {}, appendJWT())
  return res?.data?.data
}
