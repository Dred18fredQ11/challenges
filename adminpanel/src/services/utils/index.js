export function getPathURL(path) {
  return `${window.location.protocol}//${window.location.host}/${path}`
}

export function selectTextElementCSSFields({ element }) {
  const { fontFamily, color, fontSize, fontWeight, fontStyle } = element
  return { fontFamily, color, fontSize, fontWeight, fontStyle }
}

/**
 * Appends JWT to request options.
 * @param {object=} options
 * @returns {object} options with JWT token.
 */
export function appendJWT(options = {}) {
  const token = localStorage.getItem('jwt')
  return {
    ...options,
    headers: {
      ...(options.headers ?? {}),
      Authorization: `Bearer ${token}`,
    },
  }
}

function addLeadingZero(value) {
  return value < 10 ? `0${value}` : value
}

/**
 * Format date to hh:mm dd/mm/yy
 * @param {Date | string} date
 * @returns {string} date string
 */
export function formatDate(date) {
  if (typeof date === 'string') date = new Date(date)
  if (!(date instanceof Date)) return ''
  const year = date.getFullYear()
  const month = addLeadingZero(date.getMonth() + 1)
  const day = addLeadingZero(date.getDate())
  const hours = addLeadingZero(date.getHours())
  const minutes = addLeadingZero(date.getMinutes())
  return `${hours}:${minutes} ${day}/${month}/${year}`
}

/**
 * Check if two objects have equal structure and types
 * @param {object} objA first object
 * @param {object} objB second object
 * @param {boolean} isNull `null` attribute value is allowed
 * @returns {boolean} is object structures and types are equal
 */
export function isObjectTypesEqual(objA, objB, isNull = false) {
  if (
    (typeof objA !== 'object' || objA === null)
    || (typeof objB !== 'object' || objB === null)
  ) {
    return false
  }
  
  const keysA = Object.keys(objA)
  const keysB = Object.keys(objB)
  const keys = keysA.length > keysB.length ? keysA : keysB

  return !Boolean(keys.find(key => {
    return (
      typeof objA[key] !== typeof objB[key]
      && (
        (isNull ? objA[key] !== null : true)
        || (isNull ? objB[key] !== null : true)
      )
    )
  }))
}
