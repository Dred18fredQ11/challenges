import { combineReducers } from 'redux'
import auth from 'reducer/auth'
import modal from 'reducer/modal'
import widget from 'reducer/widget'
import challenge from 'reducer/challenge'

export default combineReducers({
  auth,
  modal,
  widget,
  challenge,
})
