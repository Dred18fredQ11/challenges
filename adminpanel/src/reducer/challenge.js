import {
  CHALLENGE_SET_COUNT,
  CHALLENGE_SET_LIST,
  CHALLENGE_SET_ITEM,
  CHALLENGE_SET_FILTER,
} from 'constants/actionTypes'
import { isObjectTypesEqual } from 'services/utils'

const initialState = {
  count: 0,
  challengeMap: {},
  sorting: {
    isNewFirst: true,
  },
  allowedStatusMap: (() => {
    const storageData = localStorage.getItem('allowedStatusMap')
    const storageValue = storageData ? JSON.parse(storageData) : {}
    const initValue = {
      pending: true,
      declined: true,
      approved: true,
      failed: true,
      done: true,
    }
    if (!isObjectTypesEqual(storageValue, initValue, true)) {
      localStorage.removeItem('allowedStatusMap')
      return initValue
    }
    return { ...initValue, ...storageValue }
  })(),
}

export default function challenge(state = initialState, { type, payload }) {
  switch (type) {
    case CHALLENGE_SET_COUNT: {
      const { count } = payload
      return { ...state, count }
    }

    case CHALLENGE_SET_LIST: {
      const { challengeList } = payload
      const challengeMap = challengeList.reduce((acc, challenge) => {
        return { ...acc, [challenge.id]: challenge }
      }, {})
      return { ...state, challengeMap }
    }

    case CHALLENGE_SET_ITEM: {
      const { challenge } = payload
      const challengeMap = { ...state.challengeMap, [challenge.id]: challenge }
      return { ...state, challengeMap }
    }

    case CHALLENGE_SET_FILTER: {
      const allowedStatusMap = { ...state.allowedStatusMap, ...payload }
      localStorage.setItem('allowedStatusMap', JSON.stringify(allowedStatusMap))
      return { ...state, allowedStatusMap }
    }

    default:
      return state
  }
}
