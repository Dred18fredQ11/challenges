const initnalState = {
  sidebar: {
    isOpen: false,
  },
  widgetCustomization: {
    isOpen: false,
    widgetId: null,
  }
}

export default function modal(state = initnalState, { type, payload }) {
  switch (type) {

    case 'SIDEBAR_OPEN': {
      return { ...state, sidebar: { ...state.sidebar, isOpen: true } }
    }

    case 'SIDEBAR_CLOSE': {
      return { ...state, sidebar: { ...state.sidebar, isOpen: false } }
    }

    case 'MODAL_WIDGET_CUSTOMIZATION_OPEN': {
      const { widgetId } = payload
      const widgetCustomization = { isOpen: true, widgetId }
      return { ...state, widgetCustomization }
    }

    case 'MODAL_WIDGET_CUSTOMIZATION_CLOSE': {
      const widgetCustomization = { isOpen: false, widgetId: null }
      return { ...state, widgetCustomization }
    }

    default: return state;
  }
}
