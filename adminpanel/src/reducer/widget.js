import {
  WIDGET_CUSTOMIZATION_OPEN,
  WIDGET_CUSTOMIZATION_CLOSE,
  WIDGET_CUSTOMIZATION_ELEMENT,
  WIDGET_SET_APP,
  WIDGET_SET_APP_LIST,
  WIDGET_SET_WIDGET,
  WIDGET_SET_WIDGET_LIST,
  WIDGET_SET_ELEMENT,
  WIDGET_SET_ELEMENT_LIST,
} from 'constants/actionTypes'

const initialState = {
  customization: {
    isOpen: false,
    widgetId: null,
    element: null,
  },
  widgetAppMap: {},
  widgetMap: {},
  elementMap: {},
}

export default function auth(state = initialState, { type, payload }) {
  switch (type) {
    // Data load actions
    case WIDGET_SET_APP: {
      const { widgetApp } = payload
      const widgetAppMap = { ...state.widgetAppMap, [widgetApp.id]: widgetApp }
      return { ...state, widgetAppMap }
    }

    case WIDGET_SET_APP_LIST: {
      const widgetAppMap = payload.reduce((acc, widgetApp) => {
        return { ...acc, [widgetApp.id]: widgetApp }
      }, {})
      return { ...state, widgetAppMap }
    }

    case WIDGET_SET_WIDGET: {
      const { widget } = payload
      const widgetMap = { ...state.widgetMap, [widget.id]: widget }
      return { ...state, widgetMap }
    }

    case WIDGET_SET_WIDGET_LIST: {
      const widgetMap = payload.reduce((acc, widget) => {
        return { ...acc, [widget.id]: widget }
      }, {})
      return { ...state, widgetMap }
    }

    case WIDGET_SET_ELEMENT: {
      const { element } = payload
      const elementMap = { ...state.elementMap, [element.id]: element }
      return { ...state, elementMap }
    }

    case WIDGET_SET_ELEMENT_LIST: {
      const elementMap = payload.reduce((acc, element) => {
        return { ...acc, [element.id]: element }
      }, {})
      return { ...state, elementMap }
    }

    // Widget customization actions
    case WIDGET_CUSTOMIZATION_OPEN: {
      const { widgetId } = payload
      const customization = { isOpen: true, widgetId }
      return { ...state, customization }
    }

    case WIDGET_CUSTOMIZATION_CLOSE: {
      const customization = { isOpen: false, widgetId: null }
      return { ...state, customization }
    }

    case WIDGET_CUSTOMIZATION_ELEMENT: {
      const { element } = payload
      const customization = { ...state.customization, element }
      return { ...state, customization }
    }

    default:
      return state
  }
}
