const initnalState = {
  user: null,
}

export default function auth(state = initnalState, action) {
  switch (action.type) {
    case 'AUTH': {
      return { ...state, user: action.payload }
    }
    case 'AUTH_LOGOUT': {
      localStorage.removeItem('jwt')
      return { ...state, user: null }
    }
    default: return state;
  }
}
