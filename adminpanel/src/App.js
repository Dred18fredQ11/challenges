// Libs
import Cookies from 'js-cookie'

// React
import { Suspense, useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

// Atoms
import ExternalRedirect from 'components/atoms/ExternalRedirect'
import LoaderScreen from 'components/atoms/LoaderScreen'

// Containers
import Header from 'components/layout/Header'
import Sidebar from 'components/containers/Sidebar'

// Pages
import HomePage from 'pages/HomePage'
import SettingsPage from 'pages/SettingsPage'
import WidgetPage from 'pages/WidgetPage'
import PaymentPage from 'pages/PaymentPage'
import TournamentPage from 'pages/TournamentPage'

// Actions
import { fetchUserAction } from 'actions/auth'
import { fetchWidgetInfrastractureAction } from 'actions/widget'

export default function App() {
  const dispatch = useDispatch()

  // Redux Store State
  const user = useSelector(state => state.auth.user)
  
  // State
  const [isLoading, setIsLoading] = useState(true)
  const [isError, setIsError] = useState(false)

  // Fetch account & widget information
  useEffect(() => {
    if (!localStorage.getItem('jwt')) {
      setIsLoading(false)
      return setIsError(true)
    }

    const fetchData = async () => {
      try {
        await Promise.all([
          dispatch(fetchUserAction()),
          dispatch(fetchWidgetInfrastractureAction()),
        ])
      } catch(err) {
        console.log(err)
        setIsError(true)
      } finally {
        setIsLoading(false)
      }
    }

    return fetchData()
  }, [dispatch])

  if (isLoading) {
    return <LoaderScreen />
  }

  // if (isError || !user) {
  //   const url = `${window.location.protocol}//${window.location.host}`
  //   return <ExternalRedirect url={url} />
  // }

  return (
    <Suspense fallback={'loading...'}>
      <BrowserRouter basename="/adminpanel">
        <Header />
        <Sidebar />
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route path="/settings">
            <SettingsPage />
          </Route>
          <Route path="/not-implemented">
            <PaymentPage />
          </Route>
          <Route path="/widget">
            <WidgetPage />
          </Route>
          <Route path="/tournament">
            <TournamentPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Suspense>
  )
}
