import { useTranslation } from 'react-i18next'
import styles from './styles.module.css'

export default function NotImplemented() {
  const { t } = useTranslation()
  return (
    <main className={styles.page}>
      {t('common.not_implemented')}
    </main>
  )
}
