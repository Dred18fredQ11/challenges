import { Container } from 'react-bootstrap'
import { useTranslation } from 'react-i18next'
import Block from 'components/atoms/Block'
import SwitchButton from 'components/atoms/Switch'
import ChallengeLink from 'components/containers/ChallengeLink'
import MinPaymentSum from 'components/payment/MinPaymentSum'
import styles from './styles.module.css'

export default function SettingsPage() {
  const { t } = useTranslation()
  return (
    <main className={styles.page}>
      <Container fluid>
        <Block>
          <h2>{t('settings.challenge_link')}:</h2>
          <ChallengeLink />
        </Block>

        <Block>
          <MinPaymentSum />
        </Block>

        <Block>
          <h1>{t('settings.h1')}</h1>
          <p>*{t('common.development')}</p>
          
          <div className={styles.fieldRow}>
            <p>{t('settings.delete_links')}:</p>
            <SwitchButton />
          </div>
          
          <div className={styles.field}>
            <h3>{t('settings.banned_words')}:</h3>
            <textarea className={styles.textarea} disabled></textarea>
          </div>

          <div className={styles.field}>
            <h3>{t('settings.blacklist')}:</h3>
            <textarea className={styles.textarea} disabled></textarea>
          </div>
        </Block>
      </Container>
    </main>
  )
}
