import { Container, Row, Col } from 'react-bootstrap'
import Block from 'components/atoms/Block'

import ChallengeContainer from 'components/challenge/ChallengeContainer'
import FilterSelection from 'components/challenge/FilterSelection'

export default function HomePage() {
  return (
    <main>
      <Container fluid className="d-flex justify-content-end mb-3">
        <FilterSelection />
      </Container>

      <Container fluid>
        <Row>
          <Col>
            <Block>
              <ChallengeContainer />
            </Block>
          </Col>
        </Row>
      </Container>
    </main>
  )
}