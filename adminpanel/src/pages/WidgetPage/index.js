import { useSelector } from 'react-redux'
import { Container, Row, Col } from 'react-bootstrap'

import WidgetAppManager from 'components/widget/WidgetAppManager'
import WidgetManager from 'components/widget/WidgetManager'
import CustomizationModal from 'components/widget/CustomizationModal'

import Layout from 'components/atoms/Layout'
import Block from 'components/atoms/Block'

import { createSelector } from 'reselect'

// Selectors
const defaultValue = {}

const selectWidgetAppId = createSelector(
  state => state?.widget?.widgetAppMap ?? defaultValue,
  (_, name) => name,
  (widgetAppMap, name) => Object.values(widgetAppMap)
    .find(widgetApp => widgetApp.name === name)
    ?.id
)

const selectWidgetIdList = createSelector(
  state => state?.widget?.widgetMap ?? defaultValue,
  (_, widgetAppId) => widgetAppId,
  (widgetMap, widgetAppId) => Object.values(widgetMap)
    .filter(widget => widget.widgetAppId === widgetAppId)
    .map(widget => widget.id)
)

/**
 * Widget configuration page
 */
export default function WidgetPage() {
  // Redux Store Selectors
  const mainWidgetAppId = useSelector(state => {
    return selectWidgetAppId(state, 'main')
  })

  const notificationWidgetAppId = useSelector(state => {
    return selectWidgetAppId(state, 'notification')
  })

  const mainWidgetIdList = useSelector(state => {
    return selectWidgetIdList(state, mainWidgetAppId)
  })

  const notificationWidgetIdList = useSelector(state => {
    return selectWidgetIdList(state, notificationWidgetAppId)
  })

  return (
    <Layout>
      <Container fluid>
        <Block>
          <WidgetAppManager widgetAppId={mainWidgetAppId} />
        </Block>

        <Row className="mb-5">
          {mainWidgetIdList?.map(widgetId => (
            <Col key={widgetId} sm={6} md={4} lg={3}>
              <Block>
                <WidgetManager widgetId={widgetId} />
              </Block>
            </Col>
          ))}
        </Row>

        <Block>
          <WidgetAppManager widgetAppId={notificationWidgetAppId} />
        </Block>

        <Row>
          {notificationWidgetIdList?.map(widgetId => (
            <Col key={widgetId} sm={6} md={4} lg={3}>
              <Block>
                <WidgetManager widgetId={widgetId} />
              </Block>
            </Col>
          ))}
        </Row>
      </Container>

      <CustomizationModal />
    </Layout>
  )
}
