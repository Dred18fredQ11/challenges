// import { useSelector } from 'react-redux'
import { Container } from 'react-bootstrap'
import { useTranslation } from 'react-i18next'

import Layout from 'components/atoms/Layout'
import Block from 'components/atoms/Block'

export default function PaymentPage() {
  const {t} = useTranslation()

  return (
    <Layout>
      <Container fluid>
        <Block>
          <h2>Вывод средств</h2>
          <p>
            *В данный момент автоматический вывод средств в разработке.
            Обратитесь за помощью к администратору -{' '}
            <a target="__blank" href="https://discord.gg/KyjunKMWJ9">
              {t('sidebar.help')}
            </a>
          </p>
        </Block>
      </Container>
    </Layout>
  )
}
