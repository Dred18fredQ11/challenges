import { Container } from 'react-bootstrap'
import Layout from 'components/atoms/Layout'
import Block from 'components/atoms/Block'
import styles from './styles.module.css'

export default function TournamentPage() {
  return (
    <Layout className={styles.page}>
      <Container fluid>
        <Block>
          <h1>Турнир 1х1 на Shadow Fiend</h1>
          <h2>Условия:</h2>
          <ul>
            <li>Зарегистрироваться на платформе <a target="__blank" href="https://dropchallenge.com">dropchallenge.com</a></li>
            <li>Быть участником дискорда <a target="__blank" href="https://discord.gg/ZfBZYUtduJ">https://discord.gg/ZfBZYUtduJ</a></li>
            <li>Ссылка на участие в турнире - <a target="__blank" href="https://www.faceit.com/ru/championship/80155518-1b0f-43bc-b7c2-5a4070662d3e/dropchallenge%20com">https://www.faceit.com/ru/championship/80155518-1b0f-43bc-b7c2-5a4070662d3e/dropchallenge%20com</a></li>
          </ul>
          <h2>Правила:</h2>
          <ul>
            <li>Герой Shadow Fiend</li>
            <li>Души на первом уровне</li>
            <li>Без БОТЛА, РЕЙНДРОПОВ, ЛЕСА, БЛОКА ПЕРВОЙ ПАЧКИ, СЕНТРЕЙ, ТРАНКВИЛОВ</li>
            <li>До сноса вышки или 2ух убийств</li>
          </ul>
          <h2>Призы:</h2>
          <ul>
            <li>Demon Eater (аркана)</li>
            <li>Inscribed Arms of Desolation</li>
            <li>Дота плюс или 500 рублей (на выбор)</li>
          </ul>
          <p><strong>ВНИМАНИЕ</strong>: Все игры должны происходить до 1/32, победители в 1/64 продолжат турнир на стриме. Дата проведения: <time dateTime="2021-06-01">01.06.2021</time></p>
        </Block>
      </Container>
    </Layout>
  )
}
