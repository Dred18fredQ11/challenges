import {
  CHALLENGE_SET_COUNT,
  CHALLENGE_SET_ITEM,
  CHALLENGE_SET_LIST,
  CHALLENGE_SET_SORT,
  CHALLENGE_SET_FILTER,
} from 'constants/actionTypes'
import {
  fetchChallengeCount,
  fetchChallengeList,
  updateChallengeStatus,
} from 'services/api/challenge'
import {
  fetchUserAction,
} from './auth'

/**
 * Set challenge list sorting strategy. Thunk Redux Action.
 * TODO: Add date period selection
 * @param {{ isNewFirst: boolean }} values
 */
export function setChallengeSortAction({ isNewFirst }) {
  return dispatch => {
    const payload = { isNewFirst }
    dispatch({ type: CHALLENGE_SET_SORT, payload })
  }
}

/**
 * Set challenge list filter strategy. Thunk Redux Action.
 * @param {object} values
 */
export function setChallengeFilterAction(values) {
  return dispatch => {
    const payload = values
    dispatch({ type: CHALLENGE_SET_FILTER, payload })
  }
}











export function fetchChallengeCountAction() {
  return async dispatch => {
    const count = await fetchChallengeCount()
    const payload = { count }
    dispatch({ type: CHALLENGE_SET_COUNT, payload })
    return payload
  }
}

export function fetchChallengeListAction({ limit, offset }) {
  return async dispatch => {
    const challengeList = await fetchChallengeList({ limit, offset })
    const payload = { challengeList }
    dispatch({ type: CHALLENGE_SET_LIST, payload })
    return payload
  }
}

export function updateChallengeStatusAction({ challengeId, newStatus }) {
  return async dispatch => {
    const challenge = await updateChallengeStatus({ challengeId, newStatus })
    const payload = { challenge }
    dispatch({ type: CHALLENGE_SET_ITEM, payload })
    dispatch(fetchUserAction()) // TODO: for update balance
    return payload
  }
}
