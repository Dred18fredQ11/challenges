import { fetchUser, requestLogout, setMinPaymentSum } from 'services/api/auth'

export const fetchUserAction = () => async dispatch => {
  return dispatch({ type: 'AUTH', payload: await fetchUser() })
}

export const requestLogoutAction = () => async dispatch => {
  await requestLogout()
  return dispatch({ type: 'AUTH_LOGOUT' })
}

export function setMinPaymentSumAction({ sum, currency }) {
  return async dispatch => {
    const payload = await setMinPaymentSum({ sum, currency })
    dispatch({ type: 'AUTH', payload })
    return payload
  }
}
