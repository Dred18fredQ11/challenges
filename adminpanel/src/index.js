import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from 'store'
import App from 'App'
import reportWebVitals from 'reportWebVitals'
import './i18n'

// Styles
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'
import 'assets/css/nucleo-icons.css'
import 'styles/variables.css'
import 'styles/globals.css'

if (process.env.REACT_APP_MOCK_API) {
  require('js-cookie').set('jwt', 'mock-token')
  require('mockAPI')
}

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)

reportWebVitals()
