export default function widgetMockAPI(mock) {
  mock.onGet('/api/widget/appList').reply(200, {
    message: 'Ok',
    data: [
      {
        id: 1,
        name: 'main',
        token: 'CHl7LsEUbK7X0NrCzoJZQ',
        type: 'mainWidgetApp',
        userId: 1,
      },
    ],
  })

  mock.onGet(/\/api\/widget\/app\/[0-9]+\/widgetList/).reply(200, {
    message: 'Ok',
    data: [
      {
        id: 1,
        name: 'challengeBanner',
        displayName: 'Challenge Banner',
        type: 'challengeBanner',
        widgetAppId: 1,
        challengeId: null,
      },
    ],
  })

  mock.onGet('/api/widget/app/1/widget/1/elementList').reply(200, {
    message: 'Ok',
    data: [
      {
        id: 1,
        name: 'title',
        displayName: 'Title',
        type: 'text',
        widgetId: 1,
        fontFamily: 'Roboto',
        color: '#fb8c2b',
        fontSize: 60,
        fontWeight: 'bold',
        fontStyle: 'normal',
        textShadow:
          '0px 0px 1px rgb(0 0 0),0px 0px 2px rgb(0 0 0),0px 0px 3px rgb(0 0 0),0px 0px 4px #000,0px 0px 5px rgb(0 0 0)',
      },
      {
        id: 2,
        name: 'text',
        displayName: 'Text',
        type: 'text',
        widgetId: 1,
        fontFamily: 'Roboto',
        color: '#ffffff',
        fontSize: 25,
        fontWeight: 'normal',
        fontStyle: 'normal',
        textShadow:
          '0px 0px 1px rgb(0 0 0),0px 0px 2px rgb(0 0 0),0px 0px 3px rgb(0 0 0),0px 0px 4px #000,0px 0px 5px rgb(0 0 0)',
      },
      {
        id: 3,
        name: 'notification',
        displayName: 'Notification Sound',
        type: 'notification',
        widgetId: 1,
      },
    ],
  })

  mock
    .onPut(/\/api\/widget\/app\/[0-9]*\/widget\/[0-9]*\/show/)
    .reply(({ data }) => {
      const { challengeId } = JSON.parse(data)
      return [
        200,
        {
          message: 'Ok',
          data: {
            id: 1,
            name: 'challengeBanner',
            displayName: 'Challenge Banner',
            type: 'challengeBanner',
            widgetAppId: 1,
            challengeId: challengeId,
          },
        },
      ]
    })

  mock.onPut(/\/api\/widget\/app\/[0-9]*\/widget\/[0-9]*\/hide/).reply(() => {
    return [
      200,
      {
        message: 'Ok',
        data: {
          id: 1,
          name: 'challengeBanner',
          displayName: 'Challenge Banner',
          type: 'challengeBanner',
          widgetAppId: 1,
          challengeId: null,
        },
      },
    ]
  })
}
