export default function authMockAPI(mock) {
  mock.onGet('/api/auth').reply(200, {
    message: 'Ok',
    data: {
      id: 1,
      twitchId: '669872606',
      email: 'alex.khmil37@gmail.com',
      login: 'alexkhmil37',
      displayName: 'alexkhmil37',
      profileImageUrl:
        'https://static-cdn.jtvnw.net/user-default-pictures-uv/13e5fa74-defa-11e9-809c-784f43822e80-profile_image-300x300.png',
      balance: 0,
      createdAt: '2021-04-19T06:47:11.000Z',
      updatedAt: '2021-04-19T06:47:11.000Z',
    },
  })
}