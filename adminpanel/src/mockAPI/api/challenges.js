import challengeList from 'mockAPI/data/challenges.json'

export default function challengesMockAPI(mock) {
  mock.onGet('/api/challenge/count').reply(200, {
    message: 'Ok',
    data: challengeList.length,
  })

  mock.onGet('/api/challenge/challengeList').reply(({ params }) => {
    const { limit, offset } = params
    return [
      200,
      {
        message: 'Ok',
        data: challengeList.slice(offset ?? 0, (offset ?? 0) + (limit ?? -1)),
      },
    ]
  })

  mock
    .onPut(/\/api\/challenge\/challenge\/[0-9]*\/updateStatus/)
    .reply(({ url, data }) => {
      const challengeId = parseInt(url.split('/')[4])
      const { newStatus } = JSON.parse(data)
      return [
        200,
        {
          message: 'Ok',
          data: {
            ...challengeList.find(x => x.id === challengeId),
            status: newStatus,
          },
        },
      ]
    })
}
