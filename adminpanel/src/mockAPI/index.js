import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import authMockAPI from 'mockAPI/api/auth'
import widgetMockAPI from 'mockAPI/api/widget'
import challengesMockAPI from 'mockAPI/api/challenges'

const mock = new MockAdapter(axios)
authMockAPI(mock)
widgetMockAPI(mock)
challengesMockAPI(mock)
