import { useState } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'

import UpdateStatusButton from 'components/challenge/UpdateStatusButton'
import WidgetViewButton from 'components/challenge/WidgetViewButton'
import DeleteChallengeButton from 'components/challenge/DeleteChallengeButton'
// import VoiceChallengeButton from 'components/challenge/VoiceChallengeButton'

import { formatDate } from 'services/utils'

import styles from './styles.module.css'

/**
 * Utils function to select next status for Challenge
 * @param {object} values
 * @param {string} values.status
 * @param {boolean} values.isRight 
 */
function selectUpdateStatus({ status, isRight }) {
  switch (status) {
    case 'pending':
      return !isRight ? 'approved' : 'declined'
    case 'approved':
      return !isRight ? 'done' : 'failed'
    default:
      return ''
  }
}

/**
 * Component wrapper for Challenge fields
 * @param {{ challengeId: number, forceUpdate: Function }} props
 */
export default function ChallengeItem({ challengeId, forceUpdate }) {
  const { t } = useTranslation()

  // Redux Store Component State
  const challenge = useSelector(state => {
    return state.challenge.challengeMap[challengeId]
  })

  // Local Component State
  const [isLoading, setIsLoading] = useState(false)

  // Utils
  const leftNewStatus = selectUpdateStatus({ status: challenge.status })
  const rightNewStatus = selectUpdateStatus({
    status: challenge.status,
    isRight: true,
  })

  const sum = challenge?.payerSum?.toFixed(2) ?? '0.00'
  const commission = ((challenge?.payerSum ?? 0)
    - (challenge?.profit ?? 0)
  )?.toFixed(2) ?? '0.00'

  return (
    <div className={styles.wrapper}>
      <div className={styles.nickname}>
        {challenge.creatorName}
      </div>
      <div className={styles.price}>
        <span>{sum} RUB</span>
        <span>-{commission} RUB</span>
      </div>
      <div className={styles.status}>
        {t(`status.${challenge.status}`)}
      </div>
      <div className={styles.date}>
        {formatDate(challenge.createdAt)}
      </div>
      <div className={styles.submit}>
        {leftNewStatus && (
          <UpdateStatusButton
            challengeId={challenge.id}
            disabled={isLoading}
            newStatus={leftNewStatus}
            setIsLoading={setIsLoading}
          />
        )}
      </div>
      <div className={styles.decline}>
        {rightNewStatus && (
          <UpdateStatusButton
            challengeId={challenge.id}
            disabled={isLoading}
            newStatus={rightNewStatus}
            setIsLoading={setIsLoading}
          />
        )}
      </div>
      <div className={styles.voice}>
        {/* <VoiceChallengeButton challengeId={challengeId} /> */}
      </div>
      <div className={styles.view}>
        <WidgetViewButton challengeId={challengeId} />
      </div>
      <div className={styles.delete}>
        <DeleteChallengeButton
          challengeId={challengeId}
          forceUpdate={forceUpdate}
        />
      </div>
      <div className={styles.description}>{challenge.description}</div>
    </div>
  )
}
