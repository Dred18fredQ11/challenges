import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { setChallengeFilterAction } from 'actions/challenge'

import classnames from 'classnames'
import styles from './styles.module.css'

/**
 * Controls loaded statuses of challenge
 */
export default function ChallengeFilterSelection() {
  const dispatch = useDispatch()
  const { t } = useTranslation()

  // Redux Store state
  const allowedStatusList = useSelector(state => {
    return Object.entries(state?.challenge?.allowedStatusMap ?? {})
      .filter(item => item[1]).map(item => item[0])
  })

  // Local state
  const [isOpen, setIsOpen] = useState(false)

  // Utils
  const statusList = ['pending', 'declined', 'approved', 'failed', 'done']
  const getCheckboxClass = status => classnames(
    styles.checkbox,
    allowedStatusList.includes(status) && styles.checked,
  )

  // Action handlers
  const handleOpen = () => setIsOpen(!isOpen)
  const handleCheck = status => () => {
    const allow = !allowedStatusList.includes(status)
    dispatch(setChallengeFilterAction({ [status]: allow }))
  }

  // CSS Classes
  const buttonClass = classnames(styles.button, isOpen && styles.isOpen)
  const dropdownClass = classnames(styles.dropdown, isOpen && styles.isOpen)

  return (
    <div className={styles.wrapper}>
      <button className={buttonClass} onClick={handleOpen}>
        {t(`challenge.utils.filter`)}
      </button>
      <div className={dropdownClass}>
        {statusList.map(status => (
          <button
            key={status}
            className={getCheckboxClass(status)}
            onClick={handleCheck(status)}>
            {t(`challenge.status.${status}`)}
          </button>
        ))}
      </div>
    </div>
  )
}
