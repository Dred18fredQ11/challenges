import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { showWidgetAction, hideWidgetAction } from 'actions/widget'
import classnames from 'classnames'
import styles from './styles.module.css'

/**
 * Component used to show component on widget `challengeBanner`
 * @param {object} props - react props
 * @param {number} props.challengeId
 */
export default function WidgetViewButton({ challengeId }) {
  const dispatch = useDispatch()

  // Redux Store Component State
  const { isActive, widgetId, widgetAppId } = useSelector(state => {
    const widgetList = Object.values(state.widget.widgetMap)
    const widget = widgetList?.find(widget => widget.name === 'challengeBanner')
    const isActive = widget?.challengeId === challengeId
    return {
      isActive,
      widgetId: widget?.id,
      widgetAppId: widget?.widgetAppId,
    }
  })

  // Local Component State
  const [isDisabled, setIsDisabled] = useState(false)

  // Utils
  const handleClickAction = !isActive
    ? showWidgetAction({ widgetAppId, widgetId, challengeId })
    : hideWidgetAction({ widgetAppId, widgetId })

  // Event listeners
  const handleClick = async e => {
    setIsDisabled(true)
    try {
      await dispatch(handleClickAction)
    } catch (err) {
      console.log(err)
    } finally {
      setIsDisabled(false)
    }
  }

  const buttonClass = classnames(
    styles.button,
    isActive && styles.active,
    isDisabled && styles.disabled,
  )
  return (
    <button
      className={buttonClass}
      onClick={handleClick}
      disabled={isDisabled}
    ></button>
  )
}
