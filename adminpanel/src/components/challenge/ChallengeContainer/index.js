import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import LoaderPlaceholder from 'components/layout/LoaderPlaceholder'
import ChallengeItem from 'components/challenge/ChallengeItem'
import {
  fetchChallengeCountAction,
  fetchChallengeListAction,
} from 'actions/challenge'
import styles from './styles.module.css'

/**
 * Component that wraps components with challenges and pagination
 */
export default function ChallengeContainer() {
  const dispatch = useDispatch()
  const { t } = useTranslation()
  
  // Redux Store
  const { challengeList } = useSelector(state => {
    const allowedStatusList = Object
      .entries(state?.challenge?.allowedStatusMap ?? {})
      .filter(item => item[1])
      .map(item => item[0])

    const challengeList = Object.values(state?.challenge?.challengeMap)
      .filter(item => allowedStatusList.includes(item?.status))
      .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
    return { challengeList }
  })

  // Local State
  const [isLoading, setIsLoading] = useState(true)
  const [updateAnchor, setUpdateAnchor] = useState(1)
  const forceUpdate = () => setUpdateAnchor(updateAnchor + 1)

  // Fetch challenge list
  useEffect(() => {
    const fetchData = async () => {
      try {
        await Promise.all([
          dispatch(fetchChallengeCountAction()),
          dispatch(fetchChallengeListAction({})),
        ])
      } catch (err) {
        console.log(err)
      } finally {
        setIsLoading(false)
      }
    }
    fetchData()
  }, [dispatch, updateAnchor])

  if (isLoading) {
    return (
      <div className={styles.wrapper}>
        <LoaderPlaceholder />
      </div>
    )
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.head}>
         <div className={styles.item}>
           {t('challenges.nickname')}
         </div>
         <div className={styles.item}>
           {t('challenges.sum')}
         </div>
         <div className={styles.item}>
           {t('challenges.status')}
         </div>
       </div>

      {challengeList.map(challenge => (
        <ChallengeItem
          key={challenge.id}
          challengeId={challenge.id}
          forceUpdate={forceUpdate}
        />
      ))}
    </div>
  )
}
