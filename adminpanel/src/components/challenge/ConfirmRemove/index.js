import { useState } from 'react'
import { Button,  Row, Col, Form } from 'react-bootstrap'
import { deleteChallenge } from 'services/api/challenge'

export default function ConfirmRemove({ challengeId, onHide, forceUpdate }) {
  const [isLoading, setIsLoading] = useState(false)
  const onSubmit = e => {
    e.preventDefault()
    setIsLoading(true)
    deleteChallenge({ challengeId })
      .then(data => {
        setIsLoading(false)
        onHide()
        forceUpdate()
      })
      .catch(err => console.log(err))
  }

  return (
    <Form onSubmit={onSubmit}>
      <h2>Вы действительно хотите стереть этот челлендж?</h2>
      <Row>
        <Col>
          <Button
            type="submit"
            className="w-100"
            disabled={isLoading}>
            Да
          </Button>
        </Col>
        <Col>
          <Button
            className="w-100"
            onClick={onHide}>
            Нет
          </Button>
        </Col>
      </Row>
    </Form>
  )
}
