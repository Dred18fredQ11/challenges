import { useState } from 'react'
import { speachChallenge } from 'services/api/challenge'
import styles from './styles.module.css'

export default function VoiceChallengeButton({ challengeId }) {
  const [isDisabled, setIsDisabled] = useState(false)

  const handleClick = async () => {
    setIsDisabled(true)
    try {
      await speachChallenge({ challengeId })
    } catch(err) {
      console.log(err)
    } finally {
      setIsDisabled(false)
    }
  }

  return (
    <button
      className={styles.button}
      disabled={isDisabled}
      onClick={handleClick}
      title="Воспроизвести на виджете"
    ></button>
  )
}
