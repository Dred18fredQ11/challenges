import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { updateChallengeStatusAction } from 'actions/challenge'
import { showWidgetAction } from 'actions/widget'
import classnames from 'classnames'
import styles from './styles.module.css'

export default function UpdateStatusButton({
  challengeId,
  newStatus,
  disabled,
  setIsLoading,
}) {
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const { widgetId } = useSelector(state => {
    const widgetList = Object.values(state.widget.widgetMap)
    const widget = widgetList.find(widget => widget.name === 'challengeBanner')
    const widgetId = widget.id
    return { widgetId }
  })

  const handleClick = async () => {
    setIsLoading(true)
    try {
      await dispatch(updateChallengeStatusAction({ challengeId, newStatus }))
      if (newStatus === 'approved') {
        await dispatch(showWidgetAction({ challengeId, widgetId }))
      }
    } catch (err) {
      console.log(err)
    } finally {
      setIsLoading(false)
    }
  }

  // TODO: Refactor styles assigment
  const isRight = ['declined', 'failed'].includes(newStatus)
  const buttonClass = classnames(
    styles.button,
    !isRight ? styles.purple : styles.red
  )

  return (
    <button className={buttonClass} onClick={handleClick} disabled={disabled}>
      {t(`challenges.${newStatus}`)}
    </button>
  )
}
