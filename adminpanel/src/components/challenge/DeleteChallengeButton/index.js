import { useState } from 'react'
import { hideChallenge } from 'services/api/challenge'
import styles from './styles.module.css'

export default function DeleteChallengeButton({ challengeId, forceUpdate }) {
  const [isLoading, setIsLoading] = useState(false)

  const handleClick = async () => {
    setIsLoading(true)
    try {
      await hideChallenge({ challengeId })
      forceUpdate()
    } catch (err) {
      console.log(err)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <button
      className={styles.button}
      onClick={handleClick}
      disabled={isLoading}
    ></button>
  )
}
