import { useSelector, useDispatch } from 'react-redux'
import classnames from 'classnames'
import CloseButton from 'components/layout/CloseButton'
import Block from 'components/layout/Block'
import CustomizationSelector from 'components/widget/CustomizationSelector'
import CustomizationPreview from 'components/widget/CustomizationPreview'
import CustomizationTextForm from 'components/widget/CustomizationTextForm'
import CustomizationSpeachForm from 'components/widget/CustomizationSpeachForm'
import { closeWidgetCustomizationAction } from 'actions/widget'
import styles from './styles.module.css'

/**
 * Utils function to select proper component according to element's type.
 * @param {'text'} type - type of the element
 */
function SelectCustomizationForm({ type }) {
  switch(type) {
    case 'text': return <CustomizationTextForm />
    case 'speach': return <CustomizationSpeachForm />
    default: return ''
  }
}

/**
 * Modal component that contains components for widget customization.
 * State of component stored in the Redux Store.
 */
export default function CustomizationModal() {
  const dispatch = useDispatch()

  const { isOpen, element } = useSelector(state => {
    const { isOpen, element } = state.widget.customization
    return { isOpen, element }
  })

  const modalClass = classnames(styles.modal, isOpen && styles.active)

  const handleClose = () => dispatch(closeWidgetCustomizationAction())

  return (
    <div className={modalClass}>
      <CloseButton className={styles.close} onClick={handleClose} />
      <div className={styles.left}>
        <div className={styles.leftContent}>
          <Block>
            <CustomizationSelector />
          </Block>
          {element && (
            <Block>
              <SelectCustomizationForm type={element.type} />
            </Block>
          )}
        </div>
      </div>
      <div className={styles.right}>
        <CustomizationPreview />
      </div>
    </div>
  )
}
