import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import Select from 'react-select'
import TooltipWrapper from 'components/layout/TooltipWrapper'
import { setCustomizationElementAction } from 'actions/widget'
import { createSelector } from 'reselect'
import styles from './styles.module.css'

// Selectors
const selectElementList = createSelector(
  state => state?.widget?.customization?.widgetId,
  state => state?.widget?.elementMap,
  (widgetId, elementMap) => Object.values(elementMap)
    .filter(element => element.widgetId === widgetId),
)

/**
 * Component that contains list of widget elements and allows select one to
 * customization.
 */
export default function CustomizationSelector() {
  const dispatch = useDispatch()
  const { t } = useTranslation()

  // Redux Store Selectors
  const elementList = useSelector(selectElementList)

  // Utils
  const options = elementList.map(element => ({
    value: element.id,
    label: t(`widget.${element.name}`)
  }))

  // Event handlers
  const handleSelect = ({ value }) => {
    const elementId = value !== 'null' ? parseInt(value) : null
    const element = elementList.find(element => element.id === elementId)
    dispatch(setCustomizationElementAction({ element }))
  }

  return (
    <div>
      <h2 className="d-flex align-items-end">
        {t('widget.element_customization')} <TooltipWrapper text={t('widget.tooltip.selector')} />:
      </h2>
      <Select
        className={styles.select}
        options={options}
        onChange={handleSelect}
      />
    </div>
  )
}
