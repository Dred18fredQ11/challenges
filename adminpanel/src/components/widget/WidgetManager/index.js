import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import CustomizeButton from 'components/widget/CustomizationButton'
import styles from './styles.module.css'

/**
 * Component for manage widget preferences
 * @param {{ widgetId: number }} props 
 */
export default function WidgetManager({ widgetId }) {
  const { t } = useTranslation()

  const widget = useSelector(state => state?.widget?.widgetMap[widgetId])

  return (
    <div className={styles.wrapper}>
      <h5>{t(`widget.${widget.name}`)}</h5>
      <CustomizeButton widgetId={widgetId} />
    </div>
  )
}