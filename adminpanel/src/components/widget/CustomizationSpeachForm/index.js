import { useTranslation } from 'react-i18next'

export default function CustomizationSpeachForm() {
  const { t } = useTranslation()

  return (
    <div>
      {t('common.development')}
    </div>
  )
}
