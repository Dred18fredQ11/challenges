import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { Row, Col, Button, Form } from 'react-bootstrap'
import Select from 'react-select'
import TooltipWrapper from 'components/layout/TooltipWrapper'
import {
  setCustomizationElementAction,
  updateWidgetElementAction,
  closeWidgetCustomizationAction,
} from 'actions/widget'
import { selectTextElementCSSFields } from 'services/utils'
import styles from './styles.module.css'

/**
 * Form component for customization of element-text of widget
 */
export default function CustomizationTextForm() {
  const { t } = useTranslation()
  const dispatch = useDispatch()

  // Redux Store Selectors
  const element = useSelector(state => state?.widget?.customization?.element)

  // State
  const [isLoading, setIsLoading] = useState(false)

  // Utils
  const fontFamilyOptions = [
    { value: 'Roboto', label: 'Roboto' },
    { value: 'Open Sans', label: 'Open Sans' },
    { value: 'Lobster', label: 'Lobster' },
    { value: 'Ubuntu', label: 'Ubuntu' },
    { value: 'Roboto Mono', label: 'Roboto Mono' },
    { value: 'Source Sans Pro', label: 'Source Sans Pro' },
    { value: 'Comic Neue', label: 'Comic Neue' },
    { value: 'Poppins', label: 'Poppins' },
    { value: 'Fira Sans', label: 'Fira Sans' },
    { value: 'Montserrat', label: 'Montserrat' },
  ]
  const fontFamilyStyles = {
    option: (styles, { data: { value } }) => {
      return { ...styles, fontFamily: `"${value}"` }
    }
  }

  const fontStyleOptions = [
    { value: 'normal', label: 'Normal' },
    { value: 'italic', label: 'Italic' },
  ]
  const fontWeightOptions = [
    { value: 'lighter', label: 'Lighter' },
    { value: 'normal', label: 'Normal' },
    { value: 'bold', label: 'Bold' },
  ]

  // Event handlers
  const handleChange = valuesMap => {
    const newElement = { ...element, ...valuesMap }
    dispatch(setCustomizationElementAction({ element: newElement }))
  }

  // TODO: Write data validation
  const handleChangeFontFamily = ({ value }) => {
    handleChange({ fontFamily: value })
  }

  const handleChangeColor = e => {
    handleChange({ color: e.target.value })
  }

  const handleChangeFontSize = e => {
    handleChange({ fontSize: parseInt(e.target.value) })
  }

  const handleChangeFontWeight = ({ value }) => {
    handleChange({ fontWeight: value })
  }

  const handleChangeFontStyle = ({ value }) => {
    handleChange({ fontStyle: value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setIsLoading(true)
    try {
      const fields = selectTextElementCSSFields({ element })
      await dispatch(
        updateWidgetElementAction({
          elementId: element.id,
          fields,
        }),
      )
    } catch (err) {
      console.log(err)
    } finally {
      setIsLoading(false)
    }
  }

  const handleClose = e => {
    dispatch(closeWidgetCustomizationAction())
  }

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group as={Row} className="mb-3">
        <Form.Label className="d-flex align-items-end">
          {t('widget.font_family')}
          <TooltipWrapper text={t('widget.tooltip.fontFamily')} />:
        </Form.Label>
        <Col>
          <Select
            className={styles.select}
            styles={fontFamilyStyles}
            options={fontFamilyOptions}
            defaultValue={fontFamilyOptions.find(({ value }) => {
              return value === element?.fontFamily
            })}
            onChange={handleChangeFontFamily}
            disabled={isLoading}
          />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3">
        <Form.Label className="d-flex align-items-end">
          {t('widget.color')}
          <TooltipWrapper text={t('widget.tooltip.color')} />:
        </Form.Label>
        <Row>
          <Col sm={9} className="d-flex align-items-center">
            <Form.Control
              type="text"
              value={element?.color}
              onChange={handleChangeColor}
              disabled={isLoading}
            />
          </Col>
          <Col sm={3} className="d-flex align-items-center">
            <Form.Control
              type="color"
              value={element?.color}
              onChange={handleChangeColor}
              disabled={isLoading}
            />
          </Col>
        </Row>
      </Form.Group>

      <Form.Group as={Row} className="mb-3">
        <Form.Label className="d-flex align-items-end">
          {t('widget.font_size')}
          <TooltipWrapper text={t('widget.tooltip.fontSize')} />:
        </Form.Label>
        <Row>
          <Col sm={9} className="d-flex align-items-center">
            <Form.Control
              type="range"
              className="w-100"
              value={element?.fontSize}
              onChange={handleChangeFontSize}
              min={8}
              max={100}
              disabled={isLoading}
            />
          </Col>
          <Col sm={3} className="d-flex align-items-center">
            <Form.Control
              type="number"
              className="w-100"
              value={element?.fontSize}
              onChange={handleChangeFontSize}
              min={8}
              max={100}
              disabled={isLoading}
            />
          </Col>
        </Row>
      </Form.Group>

      <Form.Group as={Row} className="mb-3">
        <Form.Label className="d-flex align-items-end">
          {t('widget.font_style')}
          <TooltipWrapper text={t('widget.tooltip.fontStyle')} />:
        </Form.Label>
        <Col>
          <Select
            className={styles.select}
            options={fontStyleOptions}
            defaultValue={fontStyleOptions.find(({ value }) => {
              return value === element?.fontStyle
            })}
            onChange={handleChangeFontStyle}
            disabled={isLoading}
          />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3">
        <Form.Label className="d-flex align-items-end">
          {t('widget.font_weight')}
          <TooltipWrapper text={t('widget.tooltip.fontWeight')} />:
        </Form.Label>
        <Col>
          <Select
            className={styles.select}
            options={fontWeightOptions}
            defaultValue={fontWeightOptions.find(({ value }) => {
              return value === element?.fontWeight
            })}
            onChange={handleChangeFontWeight}
            disabled={isLoading}
          />
        </Col>
      </Form.Group>

      <Form.Group as={Row}>
        <Col className="d-flex justify-content-between align-items-center">
          <Button
            variant="primary"
            type="submit"
            disabled={isLoading}
          >
            {t('widget.submit')}
          </Button>
          <Button
            variant="danger"
            onClick={handleClose}
            disabled={isLoading}
          >
            {t('widget.customization.close')}
          </Button>
        </Col>
      </Form.Group>
    </Form>
  )
}
