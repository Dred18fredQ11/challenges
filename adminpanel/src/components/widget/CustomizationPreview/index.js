import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import styles from './styles.module.css'

export default function CustomizationPreview() {
  const { t } = useTranslation()

  const { title, text } = useSelector(state => {
    const widgetId = state?.widget?.customization?.widgetId

    // TODO: Add component for each widget and refactor this component to
    // component wrapper
    const customizationElement = state?.widget?.customization?.element

    const elementList = Object.values(state?.widget?.elementMap)
    const result = ['title', 'text'].reduce((acc, name) => {
      if (name === customizationElement?.name) {
        return { ...acc, [name]: customizationElement }
      }
      const element = elementList.find(element => {
        return element.widgetId === widgetId && element.name === name
      })
      return { ...acc, [name]: element }
    }, {})

    return result
  })

  return (
    <div className={styles.wrapper}>
      <div style={title}>
        {t('widget.placeholder_text')}
      </div>
      <div style={text}>
        {t('widget.placeholder_text')}
      </div>
    </div>
  )
}