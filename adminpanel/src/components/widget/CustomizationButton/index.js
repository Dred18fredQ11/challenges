import { useDispatch } from 'react-redux'
import { openWidgetCustomizationAction } from 'actions/widget'
import styles from './styles.module.css'

/**
 * Component that opens widget customization modal component with specified
 * widget id
 * @param {number} widgetId
 */
export default function CustomizationButton({ widgetId }) {
  const dispatch = useDispatch()
  
  const handleOpen = () => {
    dispatch(openWidgetCustomizationAction({ widgetId }))
  }

  return (
    <button
      className={styles.button}
      onClick={handleOpen}
    >
    </button>
  )
}
