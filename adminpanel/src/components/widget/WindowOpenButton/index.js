import styles from './styles.module.css'

export default function WindowOpenButton({ url }) {
  const onClick = () => {
    window.open(
      url,
      'WidgetWindow',
      'resizable=yes,scrollbars=yes,status=yes',
    )
  }

  return (
    <button className={styles.button} onClick={onClick}></button>
  )
}