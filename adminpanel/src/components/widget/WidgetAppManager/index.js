import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import CopyButton from 'components/layout/CopyButton'
import WindowOpenButton from 'components/widget/WindowOpenButton'
import { getPathURL } from 'services/utils'
import styles from './styles.module.css'

/**
 * Component for launching widget app
 * @param {{ widgetAppId: number }} props
 */
export default function WidgetAppManager({ widgetAppId }) {
  const { t } = useTranslation()

  // Redux Store Selectors
  const widgetApp = useSelector(state => {
    return state?.widget?.widgetAppMap[widgetAppId]
  })

  // Utils
  const url = getPathURL(`widget?token=${widgetApp?.token}`)

  return (
    <div className={styles.wrapper}>
      <div className={styles.field}>
        <h2>{t(`widget.widgetApp.${widgetApp?.name}`)}:</h2>
        <WindowOpenButton url={url} />
      </div>
      <div className={styles.field}>
        <div className={styles.url}>{url}</div>
        <CopyButton text={url}/>
      </div>
    </div>
  )
}
