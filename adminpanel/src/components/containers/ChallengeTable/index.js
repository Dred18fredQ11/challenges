import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { sliceText } from 'utils'
import Button from 'components/atoms/Button'
import { Modal } from 'react-bootstrap'
// import ConfirmRemove from 'components/challenge/ConfirmRemove'

// Actions
import {
  // requestWidgetShowAction,
  // requestWidgetHideAction,
} from 'actions/widget'

// Services
import { 
  fetchChallengeCount,
  fetchChallengeList,
  updateChallengeStatus,
  hideChallenge,
 } from 'services/api/challenge'

// import {
//   fetchChallengeCount,
//   fetchChallengeList,
//   requestChangeStatus,
// } from 'services/api/challenge'

import styles from './styles.module.css'

export default function ChallengeTable() {
  return (
    <div></div>
  )

  // const [isShow, setIsShow] = useState(false)
  // const [challengeId, setChalengeId] = useState()
  // const onDelete = id => () => {
  //   setChalengeId(id)
  //   setIsShow(true)
  // }
  // const onHide = () => {
  //   setIsShow(false)
  //   setChalengeId(null)
  // }

  // const { t } = useTranslation()
  
  // const [challengeList, setChallengeList] = useState([])
  // const [page, setPage] = useState(0)
  // const [fetchedPage, setFetchedPage] = useState(0)
  // const perPage = 10
  // const [pageCount, setPageCount] = useState(1)

  // const widget = useSelector(store => store?.widget)
  // const dispatch = useDispatch()

  // const changeStatus = ({ challengeid, status }) => () => {
  //   requestChangeStatus({ challengeid, status })
  //     .then(newChallenge => {
  //       const newList = challengeList.map(challenge => {
  //         return challenge?.id === newChallenge?.id ? newChallenge : challenge
  //       })
  //       setChallengeList(newList)
  //     })
  //     .then(() => {
  //       if (status !== 'approved') return
  //       showOnWidget(challengeid)()
  //     })
  //     .catch(err => console.log(err))
  // }

  // const showOnWidget = id => () => {
  //   // dispatch(requestWidgetShowAction(id)).catch(err => console.log(err))
  // }

  // const hideOnWidget = () => () => {
  //   // dispatch(requestWidgetHideAction()).catch(err => console.log(err))
  // }

  // useEffect(() => {
  //   Promise.all([
  //     fetchChallengeCount(),
  //     fetchChallengeList({ limit: perPage, offset: fetchedPage * perPage }),
  //   ]).then(([count, list]) => {
  //       setPageCount(Math.ceil((count !== 0 ? count : 1) / perPage))
  //       setChallengeList(list)
  //       setPage(fetchedPage)
  //     })
  //     .catch(err => console.log(err))
  // }, [fetchedPage])

  // const forceUpdate = () => {
  //   Promise.all([
  //     fetchChallengeCount(),
  //     fetchChallengeList({ limit: perPage, offset: fetchedPage * perPage }),
  //   ]).then(([count, list]) => {
  //       setPageCount(Math.ceil((count !== 0 ? count : 1) / perPage))
  //       setChallengeList(list)
  //       setPage(fetchedPage)
  //     })
  //     .catch(err => console.log(err))
  // }

  // const StatusButton = ({ id: challengeid, status, isRight }) => {
  //   switch (status) {
  //     case 'pending': {
  //       if (isRight){
  //         return (
  //           <Button red
  //             onClick={changeStatus({ challengeid, status: 'declined' })}>
  //             {t('challenges.decline')}
  //           </Button>
  //         )
  //       }
  //       return (
  //         <Button
  //           onClick={changeStatus({ challengeid, status: 'approved' })}>
  //           {t('challenges.accept')}
  //         </Button>
  //       )
  //     }
  //     case 'approved': {
  //       if (isRight) {
  //         return (
  //           <Button red
  //             onClick={changeStatus({ challengeid, status: 'failed' })}>
  //             {t('challenges.failed')}
  //           </Button>
  //         )
  //       }
  //       return (
  //         <Button onClick={changeStatus({ challengeid, status: 'done' })}>
  //           {t('challenges.done')}
  //         </Button>
  //       )
  //     }
  //     default: return ''
  //   }
  // }

  // const PageButton = ({ index, children }) => {
  //   if (index === page) {
  //     return (
  //       <div className={`${styles.button} ${styles.active}`}>
  //         {children ?? index + 1} 
  //       </div>
  //     )
  //   }
  //   return (
  //     <button
  //       className={styles.button}
  //       onClick={() => setFetchedPage(index)}>
  //       {children ?? index + 1}
  //     </button>
  //   )
  // }

  // const Pagination = () => {
  //   if (pageCount <= 5) {
  //     return (
  //       <div className={styles.foot}>
  //         {Array(pageCount).fill().map((_, index) => (
  //           <PageButton index={index} key={index} />
  //         ))}
  //       </div>
  //     )
  //   }

  //   const pages = [page - 1, page, page + 1].filter(x => {
  //     return x > 0 && x < pageCount - 1
  //   })
  //   return (
  //     <div className={styles.foot}>
  //       <PageButton index={0} disabled={page !== 0}>
  //         1
  //       </PageButton>
  //       {page > 1 && <div className={styles.btnEmpty}>...</div>}
  //       {pages.map(index => (
  //         <PageButton index={index} key={index} />
  //       ))}
  //       {page < pageCount - 2 && <div className={styles.btnEmpty}>...</div>}
  //       <PageButton index={pageCount - 1}>
  //         {pageCount}
  //       </PageButton>
  //     </div>
  //   )
  // }

  // return (
  //   <div className={styles.container}>
      
  //     <div className={styles.head}>
  //       <div className={styles.item}>
  //         {t('challenges.nickname')}
  //       </div>
  //       <div className={styles.item}>
  //         {t('challenges.description')}
  //       </div>
  //       <div className={styles.item}>
  //         {t('challenges.sum')}
  //       </div>
  //       <div className={styles.item}>
  //         {t('challenges.status')}
  //       </div>
  //     </div>

  //     {challengeList.length === 0 && (
  //       <div className={styles.empty}>{t('common.empty_field')}</div>
  //     )}

  //     {challengeList.map(challenge => (
  //       <div className={styles.row} key={challenge.id}>
  //         <div className={styles.item}>
  //           {sliceText(challenge.ownerName, 20)}
  //         </div>
  //         <div className={styles.item}>
  //           {challenge.description}
  //         </div>
  //         <div className={styles.item}>
  //           {challenge.price}$
  //         </div>
  //         <div className={styles.item}>
  //           {t(`status.${challenge.status}`)}
  //         </div>
  //         <div className={styles.item}>
  //           <StatusButton {...challenge} />
  //         </div>
  //         <div className={styles.item}>
  //           <StatusButton {...challenge} isRight />
  //         </div>
  //         <div className={styles.item}>
  //           <Button
  //             className={styles.view}
  //             onClick={widget.challenge_id !== challenge.id
  //               ? showOnWidget(challenge.id)
  //               : hideOnWidget()}
  //             red={widget.challenge_id === challenge.id}
  //             empty={widget.challenge_id !== challenge.id}
  //             title={t('challenges.show_in_obs')}/>
  //         </div>
  //         <div className={styles.item}>
  //           <Button
  //             onClick={onDelete(challenge.id)}
  //             className={styles.delete}
  //             empty />
  //         </div>
  //       </div>
  //     ))}

  //     {pageCount > 1 && <Pagination />}

  //     <Modal show={isShow} onHide={onHide} animation={false}>
  //       <Modal.Body className={styles.modal}>
  //         {challengeId && (
  //           <ConfirmRemove
  //             challengeId={challengeId}
  //             onHide={onHide}
  //             forceUpdate={forceUpdate} />
  //         )}
  //       </Modal.Body>
  //     </Modal>
  //   </div>
  // )
}
