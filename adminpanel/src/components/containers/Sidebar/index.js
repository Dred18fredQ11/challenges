import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import classnames from 'classnames'
import styles from './styles.module.css'

export default function Sidebar() {
  const { t } = useTranslation()
  const { isOpen } = useSelector(store => store?.modal?.sidebar)
  const dispatch = useDispatch()

  const sidebarClass = classnames(styles.sidebar, isOpen && styles.active)
  
  const toggle = e => {
    e.preventDefault()
    dispatch({ type: 'SIDEBAR_CLOSE' })
  }

  return (
    <aside className={sidebarClass}>
      <div className={styles.panel}>
        <div className={styles.wrapper}>
          <button className={styles.close} onClick={toggle}></button>
        </div>
        <nav className={styles.nav}>
          <ul className={styles.list}>
            <li className={styles.item}>
              <Link className={styles.link} to="/">
                <i className="tim-icons icon-chart-pie-36"></i>
                <p>{t('sidebar.dashboard')}</p>
              </Link>
            </li>
            <li className={styles.item}>
              <Link className={styles.link} to="/settings">
                <i className="tim-icons icon-settings-gear-63"></i>
                <p>{t('sidebar.settings')}</p>
              </Link>
            </li>
            <li className={styles.item}>
              <Link className={styles.link} to="/widget">
                <i className="tim-icons icon-bell-55"></i>
                <p>{t('sidebar.widgets')}</p>
              </Link>
            </li>
            {/* <li className={styles.item}>
              <Link className={styles.link} to="/not-implemented">
                <i className="tim-icons icon-coins"></i>
                <p>{t('sidebar.payouts')}</p>
              </Link>
            </li> */}
            <li className={styles.item}>
              <a
                className={styles.link}
                target="__blank"
                href="https://discord.gg/KyjunKMWJ9"
              >
                <i className="tim-icons icon-alert-circle-exc"></i>
                <p>{t('sidebar.help')}</p>
              </a>
            </li>
            <li className={`${styles.item} ${styles.important}`}>
              <Link className={styles.link} to="/tournament">
                <i className="tim-icons icon-trophy"></i>
                <p>{t('sidebar.tournament')}</p>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </aside>
  )
}
