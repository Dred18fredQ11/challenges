import { useState } from 'react'
import { Form, Row, Col, Button } from 'react-bootstrap'
import ModalWrapper from 'components/atoms/ModalWrapper'


export default function WidgetTextModal({ element: q, isShow, setIsShow }) {
  const [isLoading, setIsLoading] = useState(true)
  const [element, setElement] = useState({})


  const close = () => setIsShow(false)



  const onSubmit = e => {
    e.preventDefault()
  }
  return (
    <ModalWrapper show={isShow} close={close}>
      <Form onSubmit={onSubmit}>
        <Form.Group as={Row} className="mb-3">
          <Form.Label>Font-Family</Form.Label>
          <Col sm="10">
            <Form.Control 
              type="text"
              defaultValue={element.font_family} />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3">
          <Form.Label>Color</Form.Label>
          <Col sm="10">
            <Form.Control 
              type="text"
              defaultValue={element.color} />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3">
          <Form.Label>Font-Size</Form.Label>
          <Col sm="10">
            <Form.Control 
              type="number"
              defaultValue={element.font_size} />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3">
          <Form.Label>Font-Style</Form.Label>
          <Col sm="10">
            <Form.Control 
              type="text"
              defaultValue={element.font_style} />
          </Col>
        </Form.Group>

        <Form.Group as={Row}>
          <Form.Label>Text Shadow</Form.Label>
          <Col sm="10">
            <Form.Control 
              type="text"
              defaultValue={element.text_shadow} />
          </Col>
        </Form.Group>

        <Form.Group as={Row}>
          <Button variant="primary" type="submit">Submit</Button>
          {/* <Button variant="primary">Close</Button> */}
        </Form.Group>
      </Form>
    </ModalWrapper>
  )
}
