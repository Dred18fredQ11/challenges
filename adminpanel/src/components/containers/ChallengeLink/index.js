import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import CopyButton from 'components/layout/CopyButton'
import styles from './styles.module.css'

export default function WidgetLink() {
  const { t } = useTranslation()
  const [isCopied, setIsCopied] = useState(false)
  const user = useSelector(state => state.auth.user)
  
  const link = `${window.location.protocol}//${window.location.host}/challenge/`
    + user.login
  
  useEffect(() => {
    const timeout = setTimeout(() => setIsCopied(false), 5000)
    return () => clearTimeout(timeout)
  }, [isCopied])

  return (
    <div className={styles.content}>
      <div className={styles.link}>{link}</div>
      <CopyButton
        text={link}
        onCopy={() => setIsCopied(true)}
        active={isCopied} />
      {isCopied && (
        <div className={styles.message}>{t('common.copied')}</div>
      )}
    </div>
  )
}
