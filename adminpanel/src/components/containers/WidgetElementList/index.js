import { useState, useEffect } from 'react'
import { Button } from 'react-bootstrap'

import WidgetTextModal from 'components/containers/WidgetTextModal'
import { fetchWidgetElementList } from 'services/api/widget'

export default function WidgetElementList() {
  const [isLoading, setIsLoading] = useState(true)
  const [titleElement, setTitleElement] = useState()
  const [textElement, setTextElement] = useState()
  const [isShow, setIsShow] = useState(false)

  useEffect(() => {
    fetchWidgetElementList()
      .then(data => {
        setTitleElement(data?.title)
        setTextElement(data?.text)
      })
      .catch(err => console.log(err))
      .finally(() => setIsLoading(false))
  }, [])

  if (isLoading) {
    return <div>Loading...</div>
  }

  return (
    <div>
      <Button onClick={() => setIsShow(true)}>SHOW</Button>
      <WidgetTextModal
        element={titleElement}
        isShow={isShow}
        setIsShow={setIsShow} />
      {/* <p>{titleElement.font_family}</p>
      <p>{titleElement.color}</p>
      <p>{titleElement.font_size}</p>
      <p>{titleElement.font_style}</p>
      <p>{titleElement.text_shadow}</p> */}
    </div>
  )
}
