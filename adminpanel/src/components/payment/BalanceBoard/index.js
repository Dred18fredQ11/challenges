import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import classnames from 'classnames'
import styles from './styles.module.css'

export default function BalanceBoard({ className }) {
  const { t } = useTranslation()
  const balance = useSelector(state => state?.auth?.user?.balance)
  const wrapperClass = classnames(styles.wrapper, className)
  return (
    <div className={wrapperClass}>
      {t('header.balance')}: {balance?.toFixed(2) ?? '0.00'} RUB
    </div>
  )
}
