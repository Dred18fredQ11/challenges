import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { Form, InputGroup, Button } from 'react-bootstrap'
import { setMinPaymentSumAction } from 'actions/auth'
import styles from './styles.module.css'

export default function MinPaymentSum() {
  const { t } = useTranslation()
  const dispatch = useDispatch()

  // Redux Store
  const user = useSelector(state => state?.auth?.user)

  // Local State
  const [currency, setCurrency] = useState('RUB')
  const [sum, setSum] = useState(user?.minRubSum?.toFixed(2) ?? '1.00')
  const [isLoading, setIsLoading] = useState(false)
  const [isInvalidSum, setIsInvalidSum] = useState(false)

  // Action handlers
  const handleCurrencyChange = e => {
    const value = e?.target?.value
    if (!['RUB', 'UAH', 'USD'].includes(value)) return
    const currencyDict = {
      RUB: 'minRubSum',
      USD: 'minUsdSum',
      UAH: 'minUahSum',
    }
    setCurrency(value)
    setSum(user[currencyDict[value]]?.toFixed(2))
  }

  const handleSumChange = e => {
    if (e.target.value?.length > 6) return
    setSum(e.target.value.length > 0 ? e?.target?.value : '0.00')
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setIsInvalidSum(false)
    if (!sum || isNaN(parseFloat(sum)) || parseFloat(sum) < 50) {
      return setIsInvalidSum(true)
    }
    setIsLoading(true)
    try {
      await dispatch(setMinPaymentSumAction({ sum, currency }))
    } catch (err) {
      console.log(err)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Label className="mb-3">
          <h2 className="mb-0">{t('settings.minimum_sum')}:</h2>
        </Form.Label>
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <Form.Control
              as="select"
              value={currency}
              onChange={handleCurrencyChange}
              disabled={isLoading}
            >
              <option value="RUB">RUB</option>
              {/* <option value="UAH">UAH</option> */}
              {/* <option value="USD">USD</option> */}
            </Form.Control>
          </InputGroup.Prepend>
          <Form.Control
            type="number"
            className={styles.input}
            value={sum}
            onChange={handleSumChange}
            disabled={isLoading}
            isInvalid={isInvalidSum}
          />
          <Form.Control.Feedback type="invalid" tooltip>
            {t('settings.wrong_sum')}
          </Form.Control.Feedback>
        </InputGroup>
      </Form.Group>
      <Button type="submit">{t('settings.submit')}</Button>
    </Form>
  )
}
