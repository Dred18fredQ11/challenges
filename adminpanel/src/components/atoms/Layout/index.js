import classnames from 'classnames'

export default function Layout({ className, children }) {
  const layoutClass = classnames(className)
  return (
    <main className={layoutClass}>{children}</main>
  )
}