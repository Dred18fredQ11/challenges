import classnames from 'classnames'
import styles from './styles.module.css'

export default function Block({ children, className }) {
  const blockClass = classnames(styles.block, className)
  return <div className={blockClass}>{children}</div>
}