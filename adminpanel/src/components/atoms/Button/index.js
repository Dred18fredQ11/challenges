import classnames from 'classnames'
import styles from './styles.module.css'

export default function Button({
  children,
  className,
  onClick,
  red,
  empty,
  disabled,
  title,
}) {
  const buttonClass = classnames(
    styles.button,
    (!red && !empty) && styles.purple,
    red && styles.red,
    empty && styles.empty,
    className,
  )
  const componentOnClick = onClick ?? (() => {})
  return (
    <button
      className={buttonClass}
      onClick={componentOnClick}
      disabled={disabled}
      title={title}>
      { children }
    </button>
  )
}