import { Modal } from 'react-bootstrap'
import styles from './styles.module.css'

export default function ModalWrapper({ show, close, children, title }) {
  return (
    <Modal show={show} onHide={close} className={styles.wrapper}>
      {title && <Modal.Title>{title}</Modal.Title>}
      <Modal.Body className={styles.body}>
        {children}
      </Modal.Body>
    </Modal>
  )
}
