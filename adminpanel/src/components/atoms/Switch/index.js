import classnames from 'classnames'
import styles from './styles.module.css'

export default function Switch({ className }) {
  const switchClass = classnames(styles.switch, className)
  return (
    <label className={switchClass}>
      <input type="checkbox" />
      <span className={styles.slider}></span>
    </label>
  )
}
