import { BrowserRouter, Route, Switch } from 'react-router-dom'

export default function ExternalRedirect({ url }) {
  return (
    <BrowserRouter basename="/adminpanel">
      <Switch>
        <Route 
          path="/" 
          render={() => {
            window.location = url
            return <div></div>
          }} />
      </Switch>
    </BrowserRouter>
  )
}