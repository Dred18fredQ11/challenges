import Loader from 'react-loader-spinner'
import styles from './styles.module.css'

export default function LoaderPlaceholder() {
  return (
    <div className={styles.wrapper}>
      <Loader
        type="ThreeDots"
        color="#27293d"
        height={100}
        width={100}
      />
    </div>
  )
}
