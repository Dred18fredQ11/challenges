import { Tooltip, OverlayTrigger } from 'react-bootstrap'
import styles from './styles.module.css'

const renderTooltip = text => props => (
  <Tooltip {...props}>
    {text}
  </Tooltip>
)

/**
 * Component that shows tooltip
 * @param {{ text: string }} props
 */
export default function TooltipWrapper({ text }) {
  return (
    <OverlayTrigger
      placement="top"
      delay={{ show: 250, hide: 400 }}
      overlay={renderTooltip(text)}
    >
      <span className={styles.button} onClick={e => e.preventDefault}
      ></span>
    </OverlayTrigger>
  )
}


