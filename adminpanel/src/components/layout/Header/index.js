import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Container } from 'react-bootstrap'
import { useTranslation } from 'react-i18next'
import BalanceBoard from 'components/payment/BalanceBoard'
import { requestLogoutAction } from 'actions/auth'
import classnames from 'classnames'
import styles from './styles.module.css'

export default function Header() {
  const { t, i18n } = useTranslation()
  const { language } = i18n
  const changeLanguage = (lng) => i18n.changeLanguage(lng)
  const user = useSelector(store => store?.auth?.user)
  const dispatch = useDispatch()

  // Dropdown
  const [isOpen, setIsOpen] = useState(false)
  const menuClass = classnames(styles.menu, isOpen && styles.active)
  const navClass = classnames(styles.nav, isOpen && styles.active)
  const toggleMenu = e => {
    e.preventDefault()
    setIsOpen(!isOpen)
  }

  // Logout
  const logout = e => {
    e.preventDefault()
    dispatch(requestLogoutAction())
  }

  // Sidebar
  const toggleSidebar = e => {
    e.preventDefault()
    dispatch({ type: 'SIDEBAR_OPEN' })
  }

  return (
    <header className={styles.header}>
      <Container fluid>
        <div className={styles.wrapper}>
          <button className={styles.hamburger} onClick={toggleSidebar}></button>
          <div className={styles.dropdown}>
            <BalanceBoard className={styles.balanceBoard} />
            <button className={menuClass} onClick={toggleMenu}>
              <img
                className={styles.avatar}
                src={user?.profileImageUrl}
                alt="User" />
            </button>

            <nav className={navClass}>
              <ul className={styles.list}>
                <li className={styles.item}>
                  <a
                    className={styles.link}
                    href="/">
                    {t('header.main')}
                  </a>
                </li>
                <li className={styles.item}>
                  <a
                    className={styles.link}
                    target="__blank"
                    href={`https://www.twitch.tv/${user?.login}`}>
                    {t('header.twitch')}
                  </a>
                </li>
                <li className={styles.item}>
                  <button className={styles.link} onClick={logout}>
                    {t('header.logout')}
                  </button>
                </li>
                <li className={styles.item}>
                  { language === 'ru' ? (
                    <button className={styles.langbutton} type="button" onClick={() => changeLanguage('en')}>
                      en
                    </button>
                  ) : (
                    <button className={styles.langbutton} type="button" onClick={() => changeLanguage('ru')}>
                      ru
                    </button>
                  )}
                </li>
              </ul>
            </nav>

          </div>
        </div>
      </Container>
    </header>
  )
}
