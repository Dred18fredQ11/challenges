import { useState, useEffect } from 'react'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import classnames from 'classnames'
import styles from './styles.module.css'

export default function CopyButton({ text }) {
  const [isCopied, setIsCopied] = useState(false)

  useEffect(() => {
    const timeout = setTimeout(() => setIsCopied(false), 5000)
    return () => clearTimeout(timeout)
  }, [isCopied])

  const onCopy = () => {
    setIsCopied(true)
  }

  const buttonClass = classnames(
    styles.button,
    isCopied && styles.active
  )

  return (
    <div className={styles.wrapper}>
      <CopyToClipboard text={text} onCopy={onCopy}>
        <button className={buttonClass}></button>
      </CopyToClipboard>
    </div>
  )
}
