import classnames from 'classnames'
import styles from './styles.module.css'

export default function CloseButton({ className, onClick, variant = 'black' }) {
  const buttonClass = classnames(styles.button, styles[variant], className)
  const handleClick = onClick ?? (() => {})
  return (
    <button className={buttonClass} onClick={handleClick}></button>
  )
}
