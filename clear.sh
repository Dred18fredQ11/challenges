#!/bin/bash

# Clear modules and build folders

(
  cd adminpanel;
  rm -r node_modules;
  rm -r build;
)

(
  cd widget;
  rm -r node_modules;
  rm -r build;
)

(
  cd client;
  rm -r .next;
)

(
  rm -r node_modules;
)
