import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import WithAuth from 'components/user/WithAuth';
import Layout from 'components/layout/Layout';
import LoaderPlaceholder from 'components/layout/LoaderPlaceholder';
import ChallengeContainer from 'components/challenge/ChallengeContainer';

function DashboardPage() {
  return (
    <Layout
      title="Dropchallenge - Dashboard"
      header="dashboard"
      footer="none"
      sidebar="dashboard"
    >
      <ChallengeContainer />
    </Layout>
  );
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['translation', 'footer'])),
  },
});

export default WithAuth(DashboardPage);
