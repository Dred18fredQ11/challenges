import Link from 'next/link'
import Image from 'next/image'
import { useRef } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Container, Row, Col } from 'react-bootstrap'
import Layout from 'components/layout/Layout'
import SignInButton from 'components/user/SignInButton'
import styles from 'styles/pages/home.module.css'

/**
 * Home page component.
 */
export default function Home() {
  const { t } = useTranslation('translation')
  
  // Redux Store
  const isUserFetching = useSelector(state => {
    const status = state.user.fetchingUserStatus;
    return status === 'loading';
  });
  const user = useSelector(state => state.user.user)

  // State
  const scrollTargetRef = useRef(null)

  // Event handlers
  const handleScroll = e => {
    e.preventDefault()
    window.scrollTo({
      top: scrollTargetRef?.current?.offsetTop ?? 1000,
      behavior: 'smooth',
    })
  }

  return (
    <Layout>
      <section className={styles.banner}>
        <Container>
          <h1 className={styles.bannerTitle}>
            {t('home.banner.title')}
          </h1>
          <h2 className={styles.bannerSubtitle}>
            {t('home.banner.subtitle')}
          </h2>
          {(!user && !isUserFetching) && (
            <div className="d-flex justify-content-center mt-5">
              <SignInButton variant="large" />
            </div>
          )}
          <button className={styles.bannerScroll} onClick={handleScroll}>
            <span></span>
            Scroll
          </button>
        </Container>
      </section>

      <section
        className={styles.challenges}
        ref={scrollTargetRef}
      >
        <Container>
          <h2 className="d-flex justify-content-center mb-5">
            {t('home.challenges.title')}
          </h2>
          <Row>
            <Col md={6} className="d-flex">
              <div className={styles.challengesItem}>
                <div>
                  <time dateTime="2021-05-12 11:44">11:44 12/05/21</time>
                  <h6>{t('home.challenges.from')} zxcsmiley {t('home.challenges.sum')} 100 RUB</h6>
                  <h5>{t('home.challenges.to')}: <span>znafix</span></h5>
                  <p>Тогда челлендж по сложнее. Убей с ножа одного противника)</p>
                </div>
              </div>
            </Col>

            <Col md={6} className="d-flex">
              <div className={styles.challengesItem}>
                <div>
                  <time dateTime="2021-05-12 11:44">11:44 12/05/21</time>
                  <h6>{t('home.challenges.from')} Groove {t('home.challenges.sum')} 100 RUB</h6>
                  <h5>{t('home.challenges.to')}: <span>Omni_Knight_</span></h5>
                  <p>Привет! Сыграй на гуле с первым айтемом баттл фьюри!</p>
                </div>
              </div>
            </Col>

            <Col md={6} className="d-flex">
              <div className={styles.challengesItem}>
                <div>
                  <time dateTime="2021-05-12 11:44">11:44 12/05/21</time>
                  <h6>{t('home.challenges.from')} copirka curseda {t('home.challenges.sum')} 100 RUB</h6>
                  <h5>{t('home.challenges.to')}: <span>zxcsmiley</span></h5>
                  <p>Купи после блинка и еула дагон и добей 5 врагов</p>
                </div>
              </div>
            </Col>

            <Col md={6} className="d-flex">
              <div className={styles.challengesItem}>
                <div>
                  <time dateTime="2021-05-12 11:44">11:44 12/05/21</time>
                  <h6>{t('home.challenges.from')} zxcsmiley {t('home.challenges.sum')} 100 RUB</h6>
                  <h5>{t('home.challenges.to')}: <span>Omni_Knight_</span></h5>
                  <p>Сыграй на физ сфе</p>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>

      <section className={styles.features}>
        <Container>
          <Row xs={1} md={2}>
            <Col className={styles.featuresItem}>
              <Image src="/assets/obsproject.png" width={220} height={220} />
              <p>
                {t('feature1')}
              </p>
            </Col>
            <Col className={styles.featuresItem}>
              <Image src="/assets/debit-card.png" width={220} height={220} />
              <p>
                {t('feature2')}
              </p>
            </Col>
          </Row>
        </Container>
      </section>

      <section className={styles.dashboardFeatures}>
        <Container>
          <Row xs={1} md={2} className={styles.dashboardFeaturesRow}>
            <Col className={styles.dashboardFeaturesItem}>
              <h3>{t('dashboard_feature1_title')}</h3>
              <p>
                {t('dashboard_feature1_text')}
              </p>
              <Link href="/not-implemented">
                {t('dashboard_feature1_link')}
              </Link>
            </Col>
            <Col></Col>
          </Row>
          <Row xs={1} md={2} className={styles.dashboardFeaturesRow}>
            <Col>
            </Col>
            <Col className={styles.dashboardFeaturesItem}>
              <h3>{t('dashboard_feature2_title')}</h3>
              <p>
                {t('dashboard_feature2_text')}
              </p>
              <Link href="/not-implemented">
                {t('dashboard_feature2_link')}
              </Link>
            </Col>
          </Row>
        </Container>
      </section>
      
      <section className={styles.tariffList}>
        <Container className={styles.tariffListContainer}>
          <h2 className={styles.tariffListTitle}>Тарифы на прием донатов</h2>
          <Row xs={1} md={2} className={styles.tariffListRow}>
            <Col className={styles.tariffItem}>
              <h3>Комиссия для донатов с карты</h3>
              <h4>3,5%</h4>
              <p>с любых карт</p>
            </Col>
            <Col className={styles.tariffItem}>
              <h3>Моментальный вывод на карту</h3>
              <h4>Бесплатно</h4>
              <p>
                или 30₽ комиссии, если сумма менее 10 000₽
              </p>
            </Col>
          </Row>
          <Row>
            <Link href="/not-implemented" passHref>
              <a className={styles.tariffListLink}>
                Подробнее о тарифах
              </a>
            </Link>
          </Row>
        </Container>
      </section>
  
      <section className={styles.additionalFeatures}>
        <Container>
          <Row xs={1} md={2}>
            <Col></Col>
            <Col className={styles.additionalFeaturesItem}>
              <h3>{t('additional_feature1_title')}</h3>
              <p>
                {t('additional_feature1_text1')}
              </p>
              <p>
                {t('additional_feature1_text2')}
              </p>
            </Col>
          </Row>
        </Container>
      </section>

      <section 
        className={[
          styles.additionalFeatures,
          styles.additionalFeaturesDark,
        ].join(' ')}>
        <Container>
          <Row xs={1} md={2}>
            <Col className={[
              styles.additionalFeaturesItem,
              styles.additionalFeaturesItemDark,
            ].join(' ')}>
              <h3>
                {t('additional_feature2_title')}
              </h3>
              <p>
                {t('additional_feature2_text1')}
              </p>
            </Col>
            <Col></Col>
          </Row>
        </Container>
      </section>

      <section className={[
        styles.additionalFeatures,
        styles.additionalFeaturesBlack,
      ].join(' ')}>
        <Container>
          <Row xs={1} md={2}>
            <Col></Col>
            <Col className={[
              styles.additionalFeaturesItem,
              styles.additionalFeaturesItemBlack,
            ].join(' ')}>
              <h3>{t('additional_feature3_title')}</h3>
              <p>
                {t('additional_feature3_text')}
              </p>
              <Link href="/not-implemented">
                {t('additional_feature3_link')}
              </Link>
            </Col>
          </Row>
        </Container>
      </section>

      {(!user && !isUserFetching) && (
        <section className={[
          styles.additionalFeatures,
          styles.additionalFeaturesGradient,
        ].join(' ')}>
          <Container>
            <Row xs={1} md={2}>
              <Col className={[
                styles.additionalFeaturesItem,
                styles.additionalFeaturesItemGradient,
              ].join(' ')}>
                <h3>
                  {t('additional_feature4_title')}
                </h3>
                <Link href="/api/auth/twitch">
                  {t('additional_feature4_link')}
                </Link>
              </Col>
              <Col></Col>
            </Row>
          </Container>
        </section>
      )}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(
      locale,
      ['translation', 'common', 'header', 'home', 'footer'],
    ),
  },
})
