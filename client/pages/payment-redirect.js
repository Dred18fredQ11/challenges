import { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { NotificationManager } from 'react-notifications';
import Layout from 'components/layout/Layout';
import LoaderScreen from 'components/layout/LoaderPlaceholder';
// import NoBroadcasterScreen from 'components/containers/NoBroadcasterScreen';
// import { fetchPaymentTarget } from 'services/api/payment';
import styles from 'styles/pages/payment-redirect.module.css';

export default function PaymentSuccess() {
  const { t } = useTranslation('translation');
  const { us_orderId, orderId } = useRouter().query;

  // Local component state
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [broadcaster, setBroadcaster] = useState();

  // Fetch challenge performer (payment target)
  useEffect(() => {
    if (!us_orderId && !orderId) return;
    const fetchData = async () => {
      try {
        // setBroadcaster(await fetchPaymentTarget({ orderId, us_orderId }));
      } catch (err) {
        console.log(err);
        // setIsError(true);
        // NotificationManager.error('Failed to fetch challenge target');
      } finally {
        // setIsLoading(false);
      }
    };

    fetchData();
  }, [orderId, us_orderId]);

  if (isLoading) {
    return <LoaderScreen />;
  }

  // if (isError) {
  //   return <NoBroadcasterScreen />
  // }

  return (
    <Layout
      title={`Dropchallenge - Success`}
      header="tiny"
      footer="none"
      className={styles.page}
    >
      <Container>
        <p className="mb-5">{t('success.message')}</p>
        <Row className="justify-content-between">
          {/* <Col sm={4}>
            <Link href={`../challenge/${broadcaster?.login}`}>
              <Button variant="primary" className="w-100">
                {t('success.return')}
              </Button>
            </Link>
          </Col> */}
          <Col sm={4}>
            <Link href="../">
              <Button variant="primary" className="w-100">
                {t('success.home')}
              </Button>
            </Link>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['translation'])),
  },
});
