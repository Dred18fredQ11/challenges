import { useSelector } from 'react-redux';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Row, Col } from 'react-bootstrap';
import WithAuth from 'components/user/WithAuth';
import Layout from 'components/layout/Layout';
import WidgetAppManager from 'components/widget/WidgetAppManager';
import WidgetManager from 'components/widget/WidgetManager';
import CustomizationModal from 'components/widget/CustomizationModal';
import { createSelector } from 'reselect';
import dashboardStyles from 'styles/pages/dashboard.module.css';
import styles from 'styles/pages/dashboard/widgets.module.css';

// Selectors
const selectWidgetAppId = createSelector(
  state => state.widget.widgetAppMap,
  (_, name) => name,
  (widgetAppMap, name) =>
    Object.values(widgetAppMap).find(widgetApp => widgetApp.name === name)?.id,
);

const selectWidgetIdList = createSelector(
  state => state.widget.widgetMap,
  (_, widgetAppId) => widgetAppId,
  (widgetMap, widgetAppId) =>
    Object.values(widgetMap)
      .filter(widget => widget.widgetAppId === widgetAppId)
      .map(widget => widget.id),
);

function PageDashboardWidgets() {
  // Redux Selector Hooks
  const mainWidgetAppId = useSelector(state => {
    return selectWidgetAppId(state, 'main');
  });

  const notificationWidgetAppId = useSelector(state => {
    return selectWidgetAppId(state, 'notification');
  });

  const mainWidgetIdList = useSelector(state => {
    return selectWidgetIdList(state, mainWidgetAppId);
  });

  const notificationWidgetIdList = useSelector(state => {
    return selectWidgetIdList(state, notificationWidgetAppId);
  });

  return (
    <Layout
      title="Dropchallenge - Dashboard Widgets"
      header="dashboard"
      footer="none"
      sidebar="dashboard"
      className={dashboardStyles.main}
    >
      <WidgetAppManager
        className={styles.field}
        widgetAppId={mainWidgetAppId}
      />

      <Row className={styles.field}>
        {mainWidgetIdList.map(widgetId => (
          <Col key={widgetId} sm={12} md={12} lg={6}>
            <WidgetManager widgetId={widgetId} />
          </Col>
        ))}
      </Row>

      <div className={styles.field}>
        <WidgetAppManager  widgetAppId={notificationWidgetAppId} />
      </div>

      <Row className={styles.field}>
        {notificationWidgetIdList.map(widgetId => (
          <Col key={widgetId} sm={12} md={12} lg={6}>
            <WidgetManager widgetId={widgetId} />
          </Col>
        ))}
      </Row>

      <CustomizationModal />
    </Layout>
  );
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['translation', 'footer'])),
  },
});

export default WithAuth(PageDashboardWidgets);
