import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Layout from 'components/layout/Layout';
import DonationManagerBlock from 'components/payment/DonationManagerBlock';
import dashboardStyles from 'styles/pages/dashboard.module.css';

function PageDashboardWidgets() {
  return (
    <Layout
      title="Dropchallenge - Dashboard Settings"
      header="dashboard"
      footer="none"
      sidebar="dashboard"
      className={dashboardStyles.main}
    >
      <DonationManagerBlock />
    </Layout>
  );
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['translation', 'footer'])),
  },
});

export default PageDashboardWidgets;
