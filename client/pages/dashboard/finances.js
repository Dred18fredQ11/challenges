import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import Layout from 'components/layout/Layout'

function PageDashboardWidgets() {
  return (
    <Layout
      title="Dropchallenge - Dashboard Finances"
      header="dashboard"
      footer="none"
      sidebar="dashboard"
    >
    </Layout>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(
      locale,
      ['translation', 'footer'],
    ),
  },
})

export default PageDashboardWidgets
