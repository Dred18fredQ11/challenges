import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Container } from 'react-bootstrap';
import Layout from 'components/layout/Layout';
import LoaderPlaceholder from 'components/layout/LoaderPlaceholder';
import AssignChallengeFrom from 'components/broadcaster/AssignChallengeForm';
import { fetchBroadcaster } from 'services/api/broadcaster.services';
import styles from 'styles/pages/challenge.module.css';

function ChallengePage() {
  const { t } = useTranslation('translation');
  const { broadcasterLogin } = useRouter().query;

  // State Hooks
  const [isLoading, setIsLoading] = useState(true);
  const [broadcaster, setBroadcaster] = useState(null);

  // Effect Hooks
  useEffect(() => {
    const fetchData = async () => {
      try {
        const broadcaster = await await fetchBroadcaster({
          login: broadcasterLogin,
        });
        setBroadcaster(broadcaster);
      } catch (err) {
        console.log(err);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
  }, []);

  if (isLoading) {
    return (
      <Layout
        className={styles.page}
        title={`Dropchallenge - Loading...`}
        header="tiny"
        footer="none"
      >
        <LoaderPlaceholder />
      </Layout>
    );
  }

  if (!broadcaster) {
    return (
      <Layout
        className={styles.page}
        title={`Dropchallenge - Not Found}`}
        header="tiny"
        footer="none"
      >
        <div className={styles.broadcasterNotFound}>
          {broadcasterLogin} - {t('broadcaster.broadcasterNotFound')}
        </div>
      </Layout>
    );
  }

  return (
    <Layout
      className={styles.page}
      title={`Dropchallenge - ${broadcaster.displayName}`}
      header="tiny"
      footer="none"
    >
      <Container>
        <div className={styles.broadcaster}>
          <h2>{t('broadcaster.target')}:</h2>
          <div>
            <a
              className={styles.nickname}
              target="_blank"
              rel="noopener noreferrer"
              href={`https://www.twitch.tv/${broadcaster?.login}`}
            >
              {broadcaster?.displayName}
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={`https://www.twitch.tv/${broadcaster?.login}`}
            >
              <img
                className={styles.avatar}
                src={broadcaster?.profileImageUrl}
                alt="Streamer"
              />
            </a>
          </div>
        </div>
        <AssignChallengeFrom broadcaster={broadcaster} />
      </Container>
    </Layout>
  );
}

export async function getServerSideProps(ctx) {
  return {
    props: {
      ...(await serverSideTranslations(ctx.locale, ['translation'])),
    },
  };
}

export default ChallengePage;
