import { useEffect } from 'react';
import { Provider } from 'react-redux';
import { useStore } from '../store';
import { appWithTranslation } from 'next-i18next';
import { NotificationContainer } from 'react-notifications';
import { fetchUserAction } from 'actions/user.actions';
import { fetchWidgetInfrastractureAction } from 'actions/widget.actions';

// Styles CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';
import 'styles/globals/variables.css';
import 'styles/globals/globals.css';

function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);

  // Effect Hooks
  useEffect(() => {
    if (!localStorage.getItem('jwt')) return;
    store.dispatch(fetchUserAction());
    store.dispatch(fetchWidgetInfrastractureAction());
  }, []);

  return (
    <Provider store={store}>
      <Component {...pageProps} />
      <NotificationContainer />
    </Provider>
  );
}

export default appWithTranslation(App);
