import { useRouter } from 'next/router'
import { useEffect } from 'react'
import Layout from 'components/layout/Layout'
import LoaderPlaceholder from 'components/layout/LoaderPlaceholder';

export default function AuthPage() {
  const router = useRouter()

  useEffect(() => {
    const query = window.location.search.substring(1)

    const { token } = query
      .split('&')
      .map(param => param.split('='))
      .reduce((acc, [key, value]) => {
        const decodedKey = decodeURIComponent(key)
        return { ...acc, [decodedKey]: decodeURIComponent(value)}
      }, {})

    if (!token) return router.push('/')
    
    localStorage.setItem('jwt', token)

    router.push('/dashboard')
  }, [])

  return (
    <Layout
      title="Dropchallenge - Auth"
      header="none"
      footer="none"
      banners="none"
    >
      <LoaderPlaceholder />
    </Layout>
  )
}
