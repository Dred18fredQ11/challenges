import {
  USER_DATA_FETCHING,
  USER_DATA_FETCHED,
  USER_DATA_FAILED,
  USER_LOGOUT,
} from 'constants/actionTypes';

const initialState = {
  user: null,
  fetchingUserStatus: 'idle',
};

function userReducer(state = initialState, { type, payload }) {
  switch (type) {
    case USER_DATA_FETCHING: {
      return { ...state, fetchingUserStatus: 'loading' };
    }
    case USER_DATA_FETCHED: {
      const { user } = payload;
      return { ...state, user, fetchingUserStatus: 'succeeded' };
    }
    case USER_DATA_FAILED: {
      // TODO: ADD ERROR HANDLING
      localStorage.removeItem('jwt');
      return { ...state, fetchingUserStatus: 'failed' };
    }
    case USER_LOGOUT: {
      localStorage.removeItem('jwt');
      return { ...state, user: null, fetchingUserStatus: 'idle' };
    }
    default: {
      return state;
    }
  }
}

export default userReducer;
