import { combineReducers } from 'redux';
import user from './user.reducer';
import challenge from './challenge.reducer';
import widget from 'reducer/widget.reducer';

export default combineReducers({
  user,
  challenge,
  widget,
});
