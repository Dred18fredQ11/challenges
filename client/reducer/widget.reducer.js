import {
  WIDGET_APP_LIST_FETCHING,
  WIDGET_APP_LIST_FETCHED,
  WIDGET_APP_LIST_FAILED,
  WIDGET_LIST_FETCHING,
  WIDGET_LIST_FETCHED,
  WIDGET_LIST_FAILED,
  WIDGET_ITEM_FETCHED,
  WIDGET_ELEMENT_LIST_FETCHING,
  WIDGET_ELEMENT_LIST_FETCHED,
  WIDGET_ELEMENT_LIST_FAILED,
  WIDGET_CUSTOMIZATION_OPEN,
  WIDGET_CUSTOMIZATION_CLOSE,
  WIDGET_CUSTOMIZATION_SELECTED,
  WIDGET_CUSTOMIZATION_CHANGED,
  USER_LOGOUT,
} from 'constants/actionTypes';

const initialState = {
  widgetAppMap: {},
  fetchingAppListStatus: 'idle',
  widgetMap: {},
  fetchingWidgetListStatus: 'idle',
  elementMap: {},
  fetchingElementListStatus: 'idle',
  customization: {
    isOpen: false,
    widgetId: null,
    element: null,
  },
};

function widgetReducer(state = initialState, { type, payload }) {
  switch (type) {
    case WIDGET_APP_LIST_FETCHING: {
      return { ...state, fetchingAppListStatus: 'loading' };
    }
    case WIDGET_APP_LIST_FETCHED: {
      const { widgetAppList } = payload;
      const widgetAppMap = widgetAppList.reduce((acc, widgetApp) => {
        return { ...acc, [widgetApp.id]: widgetApp };
      }, {});
      return {
        ...state,
        widgetAppMap,
        fetchingAppListStatus: 'succeeded',
      };
    }
    case WIDGET_APP_LIST_FAILED: {
      // TODO: ADD ERROR HANDLING
      return { ...state, fetchingAppListStatus: 'failed' };
    }
    case WIDGET_LIST_FETCHING: {
      return { ...state, fetchingWidgetListStatus: 'loading' };
    }
    case WIDGET_LIST_FETCHED: {
      const { widgetList } = payload;
      const widgetMap = widgetList.reduce((acc, widget) => {
        return { ...acc, [widget.id]: widget };
      }, {});
      return {
        ...state,
        widgetMap,
        fetchingWidgetListStatus: 'succeeded',
      };
    }
    case WIDGET_LIST_FAILED: {
      // TODO: ADD ERROR HANDLING
      return { ...state, fetchingWidgetListStatus: 'failed' };
    }
    case WIDGET_ITEM_FETCHED: {
      const { widget } = payload;
      const widgetMap = { ...state.widgetMap, [widget.id]: widget };
      return { ...state, widgetMap };
    }
    case WIDGET_ELEMENT_LIST_FETCHING: {
      return { ...state, fetchingElementListStatus: 'loading' };
    }
    case WIDGET_ELEMENT_LIST_FETCHED: {
      const { elementList } = payload;
      const elementMap = elementList.reduce((acc, element) => {
        return { ...acc, [element.id]: element };
      }, {});
      return { ...state, elementMap, fetchingElementListStatus: 'succeeded' };
    }
    case WIDGET_ELEMENT_LIST_FAILED: {
      // TODO: ADD ERROR HANDLING
      return { ...state, fetchingElementListStatus: 'failed' };
    }
    case WIDGET_CUSTOMIZATION_OPEN: {
      const { widgetId } = payload;
      const customization = { ...state.customization, widgetId, isOpen: true };
      return { ...state, customization };
    }
    case WIDGET_CUSTOMIZATION_CLOSE: {
      const customization = {
        ...state.customization,
        isOpen: false,
      };
      return { ...state, customization };
    }
    case WIDGET_CUSTOMIZATION_SELECTED: {
      const { elementId } = payload;
      const element = elementId ? { ...state.elementMap[elementId] } : null;
      const customization = {
        ...state.customization,
        element,
      };
      return { ...state, customization };
    }
    case WIDGET_CUSTOMIZATION_CHANGED: {
      const { elementProps } = payload;
      const customization = {
        ...state.customization,
        element: { ...state.customization.element, ...elementProps },
      };
      return { ...state, customization };
    }
    case USER_LOGOUT: {
      return { ...initialState };
    }
    default: {
      return state;
    }
  }
}

export default widgetReducer;
