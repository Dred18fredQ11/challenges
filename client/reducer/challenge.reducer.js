import {
  CHALLENGE_LIST_FETCHING,
  CHALLENGE_LIST_FETCHED,
  CHALLENGE_LIST_FAILED,
  CHALLENGE_ITEM_FETCHING,
  CHALLENGE_ITEM_FETCHED,
  CHALLENGE_ITEM_FAILED,
  CHALLENGE_FILTER,
  USER_LOGOUT,
} from 'constants/actionTypes';

const initialState = {
  count: 0,
  challengeMap: {},
  fetchingStatus: 'idle',
  filteredStatuses: {},
};

function challengeReducer(state = initialState, { type, payload }) {
  switch (type) {
    case CHALLENGE_LIST_FETCHING: {
      return { ...state, fetchingStatus: 'loading' };
    }
    case CHALLENGE_LIST_FETCHED: {
      const { challengeList } = payload;
      const challengeMap = challengeList.reduce((acc, challenge) => {
        return { ...acc, [challenge.id]: { ...challenge, isFetching: false } };
      }, {});
      return { ...state, challengeMap, fetchingStatus: 'succeeded' };
    }
    case CHALLENGE_LIST_FAILED: {
      return { ...state, fetchingStatus: 'failed' };
    }
    case CHALLENGE_ITEM_FETCHING: {
      const { challengeId } = payload;
      const challenge = {
        ...state.challengeMap[challengeId],
        isFetching: true,
      };
      const challengeMap = {
        ...state.challengeMap,
        [challengeId]: challenge,
      };
      return { ...state, challengeMap };
    }
    case CHALLENGE_ITEM_FETCHED: {
      const { challenge } = payload;
      const challengeMap = {
        ...state.challengeMap,
        [challenge.id]: challenge,
      };
      return { ...state, challengeMap };
    }
    case CHALLENGE_ITEM_FAILED: {
      // TODO: ADD ERROR HANDLING
      return state;
    }
    case CHALLENGE_FILTER: {
      const { filteredStatuses } = payload;
      localStorage.setItem(
        'filteredStatuses',
        JSON.stringify(filteredStatuses),
      );
      return {
        ...state,
        filteredStatuses: { ...state.filteredStatuses, ...filteredStatuses },
      };
    }
    case USER_LOGOUT: {
      return { ...initialState };
    }
    default: {
      return state;
    }
  }
}

export default challengeReducer;
