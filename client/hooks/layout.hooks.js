import { useEffect, MutableRefObject } from 'react';

/**
 * Hook that calls callback function after click outside of ref component.
 * @param {MutableRefObject<any>} ref ref to component.
 * @param {Function} callback function that will be executed on outside click.
 */
export function useOutsideClick(ref, callback) {
  useEffect(() => {
    const handleClickOutside = event => {
      if (!ref.current || ref.current.contains(event.target)) return;
      callback();
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref]);
}
