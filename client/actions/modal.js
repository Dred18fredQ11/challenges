export function widgetCustomizationOpen({ widgetId }) {
  return dispatch => {
    dispatch({
      type: 'MODAL_WIDGET_CUSTOMIZATION_OPEN',
      payload: { widgetId },
    })
  }
}

export function widgetCustomizationClose() {
  return dispatch => {
    dispatch({
      type: 'MODAL_WIDGET_CUSTOMIZATION_CLOSE',
    })
  }
}
