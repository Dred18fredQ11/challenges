import {
  WIDGET_CUSTOMIZATION_OPEN,
  WIDGET_CUSTOMIZATION_CLOSE,
  WIDGET_CUSTOMIZATION_ELEMENT,
  WIDGET_SET_APP_LIST,
  WIDGET_SET_WIDGET,
  WIDGET_SET_WIDGET_LIST,
  WIDGET_SET_ELEMENT_LIST,
  WIDGET_SET_ELEMENT,
} from 'constants/actionTypes'
import {
  fetchWidgetAppList,
  fetchWidgetList,
  fetchWidgetElement,
  fetchWidgetElementList,
  updateWidgetElement,
  showWidget,
  hideWidget,
} from 'services/api/widget'

/**
 * Redux action to fetch widget app list
 */
export function fetchWidgetAppListAction() {
  return async dispatch => {
    const payload = await fetchWidgetAppList()
    dispatch({ type: WIDGET_SET_APP_LIST, payload })
    return payload
  }
}

/**
 * Redux action to fetch widget list
 */
export function fetchWidgetListAction() {
  return async dispatch => {
    const payload = await fetchWidgetList()
    dispatch({ type: WIDGET_SET_WIDGET_LIST, payload })
    return payload
  }
}

/**
 * Redux action to fetch widget element list
 */
export function fetchWidgetElementListAction() {
  return async dispatch => {
    const payload = await fetchWidgetElementList()
    dispatch({ type: WIDGET_SET_ELEMENT_LIST, payload })
    return payload
  }
}

/**
 * Redux action to fetch all data about widget
 */
export function fetchWidgetInfrastractureAction() {
  return dispatch => {
    Promise.all([
      dispatch(fetchWidgetAppListAction()),
      dispatch(fetchWidgetListAction()),
      dispatch(fetchWidgetElementListAction()),
    ])
  }
}

// ---

// TODO: Order functions
export function showWidgetAction({ widgetAppId, widgetId, challengeId }) {
  return async dispatch => {
    const widget = await showWidget({ widgetAppId, widgetId, challengeId })
    const payload = { widget }
    dispatch({ type: WIDGET_SET_WIDGET, payload })
    return payload
  }
}

export function hideWidgetAction({ widgetAppId, widgetId }) {
  return async dispatch => {
    const widget = await hideWidget({ widgetAppId, widgetId })
    const payload = { widget }
    dispatch({ type: WIDGET_SET_WIDGET, payload })
    return payload
  }
}

export function openWidgetCustomizationAction({ widgetId }) {
  return dispatch => {
    dispatch({ type: WIDGET_CUSTOMIZATION_OPEN, payload: { widgetId } })
  }
}

export function closeWidgetCustomizationAction() {
  return dispatch => {
    dispatch({ type: WIDGET_CUSTOMIZATION_CLOSE })
  }
}

export function setCustomizationElementAction({ element }) {
  return dispatch => {
    dispatch({ type: WIDGET_CUSTOMIZATION_ELEMENT, payload: { element } })
  }
}

export function fetchWidgetElementAction({ widgetAppId, widgetId, elementId }) {
  return async dispatch => {
    const element = await fetchWidgetElement({
      widgetAppId,
      widgetId,
      elementId,
    })
    const payload = { element }
    dispatch({ type: WIDGET_SET_ELEMENT, payload })
    return payload
  }
}

/**
 * Makes request for updating element fields and fetching new element sate.
 * Redux thunk action.
 * @param {{ elementId: number, fields: object }} values
 */
export function updateWidgetElementAction({
  elementId,
  fields,
}) {
  return async dispatch => {
    const element = await updateWidgetElement({
      elementId,
      fields,
    })
    const payload = { element }
    dispatch({ type: WIDGET_SET_ELEMENT, payload })
    return payload
  }
}
