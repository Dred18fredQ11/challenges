import {
  WIDGET_APP_LIST_FETCHING,
  WIDGET_APP_LIST_FETCHED,
  WIDGET_APP_LIST_FAILED,
  WIDGET_LIST_FETCHING,
  WIDGET_LIST_FETCHED,
  WIDGET_LIST_FAILED,
  WIDGET_ITEM_FETCHED,
  WIDGET_ELEMENT_LIST_FETCHING,
  WIDGET_ELEMENT_LIST_FETCHED,
  WIDGET_ELEMENT_LIST_FAILED,
  WIDGET_ELEMENT_ITEM_FETCHED,
  WIDGET_CUSTOMIZATION_OPEN,
  WIDGET_CUSTOMIZATION_CLOSE,
  WIDGET_CUSTOMIZATION_SELECTED,
  WIDGET_CUSTOMIZATION_CHANGED,
} from 'constants/actionTypes';
import {
  fetchWidgetAppList,
  fetchWidgetList,
  fetchWidgetElementList,
  showChallengeOnWidget,
  hideChallengeOnWidget,
  updateWidgetElement,
} from 'services/api/widget.services';

/**
 * Thunk action for fetching list of widget apps.
 */
export function fetchWidgetAppListAction() {
  return async dispatch => {
    dispatch({ type: WIDGET_APP_LIST_FETCHING });
    try {
      const widgetAppList = await fetchWidgetAppList();
      dispatch({ type: WIDGET_APP_LIST_FETCHED, payload: { widgetAppList } });
    } catch (err) {
      // TODO: ADD ERROR HANDLING
      console.log(err);
      dispatch({ type: WIDGET_APP_LIST_FAILED });
    }
  };
}

/**
 * Thunk action for fetching list of widgets.
 */
export function fetchWidgetListAction() {
  return async dispatch => {
    dispatch({ type: WIDGET_LIST_FETCHING });
    try {
      const widgetList = await fetchWidgetList();
      dispatch({ type: WIDGET_LIST_FETCHED, payload: { widgetList } });
    } catch (err) {
      // TODO: ADD ERROR HANDLING
      console.log(err);
      dispatch({ type: WIDGET_LIST_FAILED });
    }
  };
}

/**
 * Thunk action for fetching list of widget elements.
 */
export function fetchWidgetElementListAction() {
  return async dispatch => {
    dispatch({ type: WIDGET_ELEMENT_LIST_FETCHING });
    try {
      const elementList = await fetchWidgetElementList();
      dispatch({ type: WIDGET_ELEMENT_LIST_FETCHED, payload: { elementList } });
    } catch (err) {
      // TODO: ADD ERROR HANDLING
      console.log(err);
      dispatch({ type: WIDGET_ELEMENT_LIST_FAILED });
    }
  };
}

/**
 * Redux action to fetch all data about widget
 */
export function fetchWidgetInfrastractureAction() {
  return dispatch => {
    Promise.all([
      dispatch(fetchWidgetAppListAction()),
      dispatch(fetchWidgetListAction()),
      dispatch(fetchWidgetElementListAction()),
    ]);
  };
}

/**
 * Thunk action to show challenge on widget.
 * @param {{ widgetId: number; challengeId: number; }} values
 */
export function showChallengeOnWidgetAction({ widgetId, challengeId }) {
  return async dispatch => {
    const widget = await showChallengeOnWidget({ widgetId, challengeId });
    dispatch({ type: WIDGET_ITEM_FETCHED, payload: { widget } });
  };
}

/**
 * Thunk action to hide challenge on widget.
 * @param {{ widgetId: number; }} values
 */
export function hideChallengeOnWidgetAction({ widgetId }) {
  return async dispatch => {
    const widget = await hideChallengeOnWidget({ widgetId });
    dispatch({ type: WIDGET_ITEM_FETCHED, payload: { widget } });
  };
}

/**
 * Action creator to open widget customization modal.
 * @param {{ widgetId: number; }} values
 */
export function openWidgetCustomizationAction({ widgetId }) {
  return { type: WIDGET_CUSTOMIZATION_OPEN, payload: { widgetId } };
}

/**
 * Action creator to close widget customization modal.
 */
export function closeWidgetCustomizationAction() {
  return { type: WIDGET_CUSTOMIZATION_CLOSE };
}

/**
 * Action creator. Select element for customization.
 * @param {{ elementId: number; }} values
 */
export function selectWidgetCustomizationAction({ elementId }) {
  return { type: WIDGET_CUSTOMIZATION_SELECTED, payload: { elementId } };
}

/**
 * Action creator. Set customization element.
 * @param {{ elementProps: Object; }} values
 */
export function setCustomizationElementAction({ elementProps }) {
  return { type: WIDGET_CUSTOMIZATION_CHANGED, payload: { elementProps } };
}

/**
 * Thunk action. Updates widget's element.
 * @param {{ elementId: number; elementProps: Object; }} values
 */
export function updateWidgetElementAction({ elementId, elementProps }) {
  return async dispatch => {
    const element = await updateWidgetElement({ elementId, elementProps });
    dispatch({ type: WIDGET_ELEMENT_ITEM_FETCHED, payload: { element } });
  };
}
