import {
  CHALLENGE_LIST_FETCHING,
  CHALLENGE_LIST_FETCHED,
  CHALLENGE_LIST_FAILED,
  CHALLENGE_ITEM_FETCHING,
  CHALLENGE_ITEM_FETCHED,
  CHALLENGE_ITEM_FAILED,
  CHALLENGE_FILTER,
} from 'constants/actionTypes';
import {
  fetchChallengeList,
  updateChallengeStatus,
} from 'services/api/challenge.services';

/**
 * Fetchign the list of challenges.
 * @param {{ limit?: number; offset?: number; }} values
 */
export function fetchChallengeListAction({ limit, offset }) {
  return async dispatch => {
    dispatch({ type: CHALLENGE_LIST_FETCHING });
    try {
      const challengeList = await fetchChallengeList({ limit, offset });
      dispatch({ type: CHALLENGE_LIST_FETCHED, payload: { challengeList } });
    } catch (err) {
      console.log(err);
      dispatch({ type: CHALLENGE_LIST_FAILED });
    }
  };
}

/**
 * Updates status of challenge and fetches new status.
 * @param {{ challengeId: number; newStatus: string }} values
 */
export function updateChallengeStatusAction({ challengeId, newStatus }) {
  return async dispatch => {
    dispatch({ type: CHALLENGE_ITEM_FETCHING, payload: { challengeId } });
    try {
      const challenge = await updateChallengeStatus({ challengeId, newStatus });
      dispatch({ type: CHALLENGE_ITEM_FETCHED, payload: { challenge } });
    } catch (err) {
      console.log(err);
      dispatch({ type: CHALLENGE_ITEM_FAILED });
    }
  };
}

/**
 * 
 */
export function filterChallengeListAction({ filteredStatuses }) {
  return async dispatch => {
    dispatch({ type: CHALLENGE_FILTER, payload: { filteredStatuses } });
  };
}
