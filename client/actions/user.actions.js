import {
  USER_DATA_FETCHING,
  USER_DATA_FETCHED,
  USER_DATA_FAILED,
  USER_LOGOUT,
} from 'constants/actionTypes';
import { fetchUser } from 'services/api/user.services';

/**
 * Thunk action for fetching user's data.
 */
export function fetchUserAction() {
  return async dispatch => {
    dispatch({ type: USER_DATA_FETCHING });
    try {
      const user = await fetchUser();
      dispatch({ type: USER_DATA_FETCHED, payload: { user } });
    } catch (err) {
      dispatch({ type: USER_DATA_FAILED });
    }
  };
}

/**
 * Thunk action for logout.
 */
export function logoutAction() {
  return async dispatch => {
    dispatch({ type: USER_LOGOUT });
  }
}
