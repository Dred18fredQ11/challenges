import axios from 'axios'
import { appendHeaderJWT, appendHeaderNoCache } from 'services/utils'

/**
 * Fetch user data.
 */
export async function fetchUser() {
  const options = appendHeaderNoCache(appendHeaderJWT())
  const res = await axios.post(`/api/auth?_=${Date.now()}`, {}, options)
  return res?.data?.data
}
