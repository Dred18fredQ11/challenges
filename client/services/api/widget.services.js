import axios from 'axios';
import {
  appendHeaderJWT,
  appendHeaderNoCache,
  appendHeaderJSON,
} from 'services/utils';

/**
 * Fetches list of widget apps.
 */
export async function fetchWidgetAppList() {
  const config = appendHeaderNoCache(appendHeaderJWT());
  const url = `/api/widget/appList?_=${Date.now()}`;
  const res = await axios.get(url, config);
  return res.data.data;
}

/**
 * Fetches list of widgets.
 */
export async function fetchWidgetList() {
  const config = appendHeaderNoCache(appendHeaderJWT());
  const res = await axios.get(`/api/widget/widgetList?_=${Date.now()}`, config);
  return res.data.data;
}

/**
 * Fetches list of widget's elements.
 */
export async function fetchWidgetElementList() {
  const config = appendHeaderNoCache(appendHeaderJWT());
  const res = await axios.get(`/api/widget/elementList`, config);
  return res.data.data;
}

/**
 * Show challenge on widget.
 * @param {{ widgetId: number; challengeId: number; }} values
 */
export async function showChallengeOnWidget({ widgetId, challengeId }) {
  const config = appendHeaderJSON(appendHeaderJWT());
  const body = { challengeId };
  const url = `/api/widget/widget/${widgetId}/show`;
  const res = await axios.put(url, body, config);
  return res.data.data;
}

/**
 * Hide challenge on widget.
 * @param {{ widgetId: number; }} values
 */
export async function hideChallengeOnWidget({ widgetId }) {
  const url = `/api/widget/widget/${widgetId}/hide`;
  const res = await axios.put(url, {}, appendHeaderJWT());
  return res.data.data;
}

/**
 * Update widget's element props.
 * @param {{ elementId: number; elementProps: Object }} values
 */
export async function updateWidgetElement({ elementId, elementProps }) {
  const config = appendHeaderJSON(appendHeaderJWT());
  const body = { elementProps };
  const res = await axios.put(`/api/widget/element/${elementId}`, body, config);
  return res.data.data;
}
