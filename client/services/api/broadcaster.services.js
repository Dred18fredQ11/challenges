import axios from 'axios';

export async function fetchBroadcaster({ login }) {
  const res = await axios.get(`/api/broadcaster/${login}`);
  return res.data.data;
}
