import axios from 'axios';
import {
  appendHeaderJSON,
  appendHeaderJWT,
  appendHeaderNoCache,
} from 'services/utils';
import { appendJWT } from 'services/utils';

/**
 * Fetches list of challenges.
 * @param {{ limit?: number; offset?: number; }} values
 */
export async function fetchChallengeList({ limit, offset }) {
  const config = appendHeaderJWT(
    appendHeaderNoCache({ params: { limit, offset } }),
  );
  const url = `/api/challenge/challengeList?_=${Date.now()}`;
  const res = await axios.get(url, config);
  return res.data.data;
}

/**
 * Updates status of the challenge.
 * @param {{ challengeId: number; newStatus: string; }} values
 */
export async function updateChallengeStatus({ challengeId, newStatus }) {
  const config = appendHeaderJSON(appendHeaderJWT());
  const body = { newStatus };
  const url = `/api/challenge/challenge/${challengeId}/updateStatus`;
  const res = await axios.put(url, body, config);
  return res.data.data;
}

/**
 * Hide challenge.
 * @param {{ challengeId: number }} values
 */
export async function hideChallenge({ challengeId }) {
  const config = appendHeaderJWT();
  const url = `/api/challenge/challenge/${challengeId}/hide`;
  const res = await axios.put(url, {}, config);
  return res.data.data;
}





////////////////////////////////////////////////////////////////////////////////

export async function fetchChallengeCount() {
  const config = { headers: { 'Cache-Control': 'no-cache' } };
  const url = `/api/challenge/count?_=${Date.now()}`;
  const res = await axios.get(url, appendJWT(config));
  return res?.data?.data;
}

// export async function fetchChallengeList({ limit, offset }) {
//   const config = {
//     headers: { 'Cache-Control': 'no-cache' },
//     params: { limit, offset },
//   }
//   const url = `/api/challenge/challengeList?_=${Date.now()}`
//   const res = await axios.get(url, appendJWT(config))
//   return res?.data?.data
// }


export async function speachChallenge({ challengeId }) {
  const url = `/challenge/${challengeId}/speach`;
  const res = await axios.post(url, {}, appendJWT());
  return res?.data?.data;
}

/**
 * Makes challenge assigment request. Starts payment chain.
 * @param {object} values
 * @param {options} options
 */
export async function assignChallenge(
  { nickname, sum, currency, description, broadcasterId },
  { isTest = true } = {},
) {
  const config = { headers: { 'Content-Type': 'application/json' } };
  const body = { nickname, sum, currency, description, broadcasterId };
  const url = !isTest ? '/api/challenge' : '/api/challenge/test';
  const res = await axios.post(url, body, config);
  return res?.data?.data;
}
