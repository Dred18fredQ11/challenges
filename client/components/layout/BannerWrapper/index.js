import BannerTournament from 'components/layout/BannerTournament'
import styles from './styles.module.css'

/**
 * Wrapper component for Banners components
 */
export default function BannerWrapper() {
  return (
    <div className={styles.wrapper}>
      <BannerTournament />
    </div>
  )
}
