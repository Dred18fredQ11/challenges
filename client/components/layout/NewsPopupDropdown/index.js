import { useRef, useState } from 'react'
import Image from 'next/image'
import { useTranslation } from 'next-i18next'
import { CSSTransition } from 'react-transition-group'
import { useOutsideClick } from 'hooks/layout.hooks'
import classnames from 'classnames'
import styles from './styles.module.css'

/**
 * Dropdown popup component that contains project news.
 * @param {{ className?: string }} props
 */
function NewsPopupDropdown({ className }) {
  const { t } = useTranslation('translation')

  // State
  const [isOpen, setIsOpen] = useState(false)
  const wrapperRef = useRef(null)

  // Event Handlers
  const handleToggleClick = () => setIsOpen(!isOpen)
  useOutsideClick(wrapperRef, () => setIsOpen(false))

  // Classes Css
  const wrapperClass = classnames(styles.wrapper, className)
  const toggleClass = classnames(styles.toggle, isOpen && styles.active)

  return (
    <div className={wrapperClass} ref={wrapperRef}>
      <button className={toggleClass} onClick={handleToggleClick}>
        <Image
          src="/assets/bell.svg"
          alt="Project News"
          width={30}
          height={30}
        />
      </button>
      <CSSTransition
        in={isOpen}
        timeout={300}
        classNames={{
          enter: styles.dropdownEnter,
          enterActive: styles.dropdownEnterActive,
          enterDone: styles.dropdownEnterDone,
          exit: styles.dropdownExit,
        }}
      >
        <div className={styles.dropdown}>
          <h5>{t('news.title')}:</h5>
          {/* <div className={styles.dropdownList}>
            <div className={styles.dropdownItem}>
              <h6>Заголовок 1</h6>
              <p>
                Пример текста
              </p>
            </div>
          </div> */}
          <div className={styles.dropdownEmpty}>{t('news.empty')}</div>
        </div>
      </CSSTransition>
    </div>
  )
}

export default NewsPopupDropdown
