import Image from 'next/image';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Button with close icon.
 * @param {{ className?: string; onClick?: Function; }} props
 */
function CloseButton({ className, onClick }) {
  // Classes CSS
  const buttonClass = classnames(styles.button, className);

  return (
    <button className={buttonClass} onClick={onClick}>
      <Image src="/assets/close.svg" width={20} height={20} alt="Close Modal" />
    </button>
  );
}

export default CloseButton;
