import Select from 'react-select';
import classnames from 'classnames';
import thisStyles from './styles.module.css';

/**
 * React-select with custom styles.
 * @param {{
 *  instanceId: string;
 *  options: { value: any, label: string }[];
 *  onChange: Function;
 *  value: { value: any, label: string };
 *  disabled?: boolean;
 *  className?: string;
 *  styles?: {
 *    [key]: () => {
 *      [key]: string;
 *    };
 *  };
 * }} props
 */
function CustomSelect({
  instanceId,
  options,
  onChange,
  value,
  disabled,
  className,
  styles,
}) {
  // Classes CSS
  const wrapperClass = classnames(thisStyles.wrapper, className);

  return (
    <Select
      instanceId={instanceId}
      className={wrapperClass}
      styles={{
        container: base => ({
          ...base,
          background: '#1C1C1E',
          borderRadius: '8px',
          ...((styles?.container && styles.container({})) ?? {}),
        }),
        valueContainer: base => ({
          ...base,
          background: '#1C1C1E',
          padding: '0 15px',
          ...((styles?.valueContainer && styles.valueContainer({})) ??
            {}),
        }),
        control: base => ({
          ...base,
          border: 'none',
          background: '#1C1C1E',
          ...((styles?.control && styles.control({})) ?? {}),
        }),
        singleValue: base => ({
          ...base,
          color: '#F2F2F7',
          ...((styles?.singleValue && styles.singleValue({})) ?? {}),
        }),
        menu: base => ({
          ...base,
          background: '#1C1C1E',
        }),
        option: (base, config) => ({
          ...base,
          background: '#1C1C1E',
          ...((styles?.option && styles.option({}, config)) ?? {}),
          ':hover': {
            background: '#3A3A3C',
          },
        }),
      }}
      options={options}
      onChange={onChange}
      value={value}
      isDisabled={disabled}
    />
  );
}

export default CustomSelect;
