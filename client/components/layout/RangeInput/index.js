import classnames from 'classnames';
import styles from './styles.module.css';

function RangeInput({ value, onChange, className }) {
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <input type="range" value={value} onChange={onChange} />
      <input type="number" value={value} onChange={onChange} />
    </div>
  );
}

export default RangeInput;
