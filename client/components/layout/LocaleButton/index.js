import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'
import classnames from 'classnames'
import styles from './styles.module.css'

/**
 * Button component for switching local.e
 * @param {{ className?: string }} props
 */
function LocaleButton({ className }) {
  const router = useRouter()
  const buttonClass = classnames(styles.button, className)

  return (
    <Link
      href={router.asPath}
      locale={router.locale === 'en' ? 'ru' : 'en'}
    >
      <button className={buttonClass}>
        {router.locale === 'ru' ? (
          <Image src="/assets/lang_ru.svg" width={30} height={22} />
        ) : (
          <Image src="/assets/lang_en.svg" width={30} height={22} />
        )}
      </button>
    </Link>
  )
}

export default LocaleButton
