import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Block with burger icon.
 * @param {{
 *  className: string;
 *  isOpen: boolean;
 * }} props
 */
function BurgerBlock({ className, isOpen = false }) {
  const wrapperClass = classnames(styles.wrapper, className);
  const iconClass = classnames(styles.icon, isOpen && styles.open);
  return (
    <div className={wrapperClass}>
      <i className={iconClass}>
        <span></span>
      </i>
    </div>
  );
}

export default BurgerBlock;
