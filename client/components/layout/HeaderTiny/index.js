import { Container } from 'react-bootstrap'
import LocaleButton from 'components/layout/LocaleButton'
import styles from './styles.module.css'

export default function HeaderTiny() {
  return (
    <header className={styles.header}>
      <Container className="d-flex justify-content-end" fluid>
        <LocaleButton />
      </Container>
    </header>
  )
}
