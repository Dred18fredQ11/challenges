import Loader from 'react-loader-spinner'
import classnames from 'classnames'
import styles from './styles.module.css'

/**
 * Component that displays on free space loadeder spinner.
 * Default values: `width=100px`, `height=100px`, `color=#ffffff`.
 * @param {{
 *  className?: string;
 *  color?: string;
 *  width?: number;
 *  height?: number;
 * }} props
 */
function LoaderPlaceholder({
  className,
  color = "#ffffff",
  width = 100,
  height = 100,
}) {
  const wrapperClasss = classnames(styles.wrapper, className)

  return (
    <div className={wrapperClasss}>
      <Loader
        type="ThreeDots"
        color={color}
        width={width}
        height={height}
      />
    </div>
  )
}

export default LoaderPlaceholder
