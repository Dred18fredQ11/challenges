import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Color text input and color input box;
 * @param {{ value: string; onChange: Funciton; className: string }} props
 */
function ColorInput({ value, onChange, className }) {
  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className)

  return (
    <div className={wrapperClass}>
      <input type="text" value={value} onChange={onChange} />
      <input type="color" value={value} onChange={onChange} />
    </div>
  );
}

export default ColorInput;
