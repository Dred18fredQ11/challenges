import Image from 'next/image';
import Link from 'next/link';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Block with the brand icon and its name.
 * @param {{ className?: string; width?: number; height?: number }} props
 */
function BrandBlock({ className, width = 50, height = 42 }) {
  return (
    <Link href="/" passHref>
      <a className={classnames(styles.wrapper, className)}>
        <Image src="/assets/logo.ico" width={width} height={height} />
        <h2 className={styles.title}>Dropchallenge</h2>
      </a>
    </Link>
  );
}

export default BrandBlock;
