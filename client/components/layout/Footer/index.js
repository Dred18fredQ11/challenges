import Image from 'next/image'
import Link from 'next/link'
import { useSelector } from 'react-redux'
import { Container, Row, Col } from 'react-bootstrap'
import { useTranslation } from 'next-i18next'

import styles from './styles.module.css'

export default function Footer() {
  const user = useSelector(store => store?.auth?.user)
  const { t } = useTranslation('footer')

  return (
    <footer className={styles.footer}>
      <Container>
        <Row xs={1} sm={2} md={4}>
          <Col className={styles.info}>
            <Link href="/">
              <a>
                <Image src="/assets/logo.ico" width={100} height={100} />
              </a>
            </Link>
            <p>
              {t('text')}
            </p>
          </Col>
          <Col className={styles.nav}>
            <h5>{t('home')}</h5>
            <ul>
              <li>
                <Link href="/">
                  {t('home')}
                </Link>
              </li>
              {/* <li>
                <Link href="/rules">{t('rules')}</Link>
              </li> */}
              {/* <li>
                <Link href="/not-implemented">Тарифы</Link>
              </li> */}
              {/* <li>
                <Link href="/not-implemented">{t('about-us')}</Link>
              </li> */}
              {/* <li>
                <Link href="/not-implemented">Статус сервиса</Link>
              </li> */}
              {!user && (
                <li>
                  <a href="/api/auth/twitch">{t('signin')}</a>
                </li>
              )}
            </ul>
          </Col>
          <Col className={styles.nav}>
            {/* <a href="//freekassa.ru/">
              <img src="//www.free-kassa.ru/img/fk_btn/18.png" title="Приём оплаты на сайте картами"/>
            </a> */}
            {/* <h5>Фичи</h5>
            <ul>
              <li>
                <Link href="/not-implemented">Страница стримера</Link>
              </li>
              <li>
                <Link href="/not-implemented">Личный кабинет стримера</Link>
              </li>
              <li>
                <Link href="/not-implemented">Панели для Twitch и логотипы</Link>
              </li>
            </ul> */}
          </Col>
          <Col className={styles.nav}>
            {/* <h5>ТОП донатеров</h5>
            <ul>
              <li>
                <Link href="/not-implemented">Уведомления о донатах</Link>
              </li>
              <li>
                <Link href="/not-implemented">Полоска цели на стриме</Link>
              </li>
              <li>
                <Link href="/not-implemented">ТОП донатеров</Link>
              </li>
            </ul> */}
          </Col>
        </Row>
      </Container>
    </footer>
  )
}
