import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import PageChallengesIcon from 'components/icons/PageChallengesIcon';
import PageStatisticsIcon from 'components/icons/PageStatisticsIcon';
import PageWidgetIcon from 'components/icons/PageWidgetsIcon';
import PageSettingsIcon from 'components/icons/PageSettingsIcon';
import PageFinancesIcon from 'components/icons/PageFinancesIcon';
import PageTournamentIcon from 'components/icons/PageTournamentIcon';
import PageHelpIcon from 'components/icons/PageHelpIcon';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Utils component for items.
 * @param {{
 *  IconComponent: JSX.Element;
 *  href: string
 *  children?: JSX.Element;
 *  className?: string;
 *  isExternal?: boolean
 * }} props
 */
function SidebarLink({
  IconComponent,
  href,
  children,
  className,
  isExternal = false,
}) {
  const { asPath } = useRouter();
  const isActive = asPath === href;
  const [isHover, setIsHover] = useState(false);
  const linkClass = classnames(
    styles.link,
    (isActive || isHover) && styles.active,
    className,
  );

  if (isExternal) {
    return (
      <a
        className={linkClass}
        onMouseEnter={() => setIsHover(true)}
        onMouseLeave={() => setIsHover(false)}
        target="__blank"
        href={href}
      >
        <IconComponent
          className={styles.icon}
          fill={isHover || isActive ? '#F2F2F7' : '#8E8E93'}
        />
        {children && <p>{children}</p>}
      </a>
    );
  }

  return (
    <Link href={href} passHref={true}>
      <a
        className={linkClass}
        onMouseEnter={() => setIsHover(true)}
        onMouseLeave={() => setIsHover(false)}
      >
        <IconComponent
          className={styles.icon}
          fill={isHover || isActive ? '#F2F2F7' : '#8E8E93'}
        />
        {children && <p>{children}</p>}
      </a>
    </Link>
  );
}

/**
 * Aside floating component with navigation.
 */
function DashboardSidebar() {
  const { t } = useTranslation('translation');

  return (
    <aside className={styles.wrapper}>
      <nav className={styles.nav}>
        <SidebarLink IconComponent={PageChallengesIcon} href="/dashboard">
          {t('sidebar.pageChallenges')}
        </SidebarLink>

        {/* <SidebarLink
          IconComponent={PageStatisticsIcon}
          href="/dashboard/statistics"
        >
          {t('sidebar.pageStatistics')}
        </SidebarLink> */}

        <SidebarLink IconComponent={PageWidgetIcon} href="/dashboard/widgets">
          {t('sidebar.pageWidgets')}
        </SidebarLink>

        <SidebarLink
          IconComponent={PageSettingsIcon}
          href="/dashboard/settings"
        >
          {t('sidebar.pageSettings')}
        </SidebarLink>

        <SidebarLink
          IconComponent={PageFinancesIcon}
          href="/dashboard/finances"
        >
          {t('sidebar.pageFinances')}
        </SidebarLink>

        {/* <SidebarLink
          IconComponent={PageTournamentIcon}
          href="/dashboard/tournament"
        >
          {t('sidebar.pageTournament')}
        </SidebarLink> */}

        <SidebarLink
          IconComponent={PageHelpIcon}
          href="https://discord.gg/KyjunKMWJ9"
          isExternal={true}
        >
          {t('sidebar.pageHelp')}
        </SidebarLink>
      </nav>
    </aside>
  );
}

export default DashboardSidebar;
