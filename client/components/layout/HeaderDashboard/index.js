import { useSelector } from 'react-redux';
import { Container } from 'react-bootstrap';
import HeaderDropdown from 'components/layout/HeaderDropdown';
import SignInButton from 'components/user/SignInButton';
import LocaleButton from 'components/layout/LocaleButton';
import ContactWrapper from 'components/layout/ContactWrapper';
import BrandBlock from 'components/layout/BrandBlock';
import NewsPopupDropdown from 'components/layout/NewsPopupDropdown';
import BalanceBlock from 'components/payment/BalanceBlock';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Header component for dashboard pages.
 */
function HeaderDashboard() {
  // Redux Store Selectors
  const isAuth = useSelector(state => {
    const status = state.user.fetchingUserStatus;
    return status === 'loading' || status === 'succeeded';
  });

  // Classes CSS
  const containerClass = classnames(
    'd-flex',
    'justify-content-between',
    'align-items-center',
  );

  return (
    <header className={styles.header}>
      <Container fluid className={containerClass}>
        <BrandBlock />
        <div className="d-flex align-items-center">
          <ContactWrapper className={styles.margin} />
          <LocaleButton className={styles.margin} />
          <NewsPopupDropdown className={styles.margin} />
          {isAuth && <BalanceBlock className={styles.margin} />}
          {isAuth ? <HeaderDropdown /> : <SignInButton />}
        </div>
      </Container>
    </header>
  );
}

export default HeaderDashboard;
