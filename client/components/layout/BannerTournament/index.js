import Image from 'next/image'
import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'next-i18next'
import { Container } from 'react-bootstrap'
import SignInButton from 'components/user/SignInButton'
import styles from './styles.module.css'

export default function TournamentBanner() {
  const { t } = useTranslation('translation')

  // Redux Store
  const user =  useSelector(state => state.user.user)

  // State
  const [isClosed, setIsClosed] = useState(false)

  // Fetch data from LocalStorage
  useEffect(() => {
    const localStorageItem = localStorage.getItem('tournamentBanner')
    if (!localStorageItem) return

    const { isClosed, timestamp } = JSON.parse(localStorageItem)
    if (typeof isClosed !== 'boolean' || typeof timestamp !== 'number') {
      return
    }

    const period = 24 * 60 * 60 // 24 hours
    const isPeriod = (Date.now() - new Date(timestamp)) > period
    if (isClosed && !isPeriod) {
      setIsClosed(true)
    }
  }, [])

  // Don't render if previously closed
  if (isClosed) {
    return ''
  }

  // Event handlers
  const handleAction = () => {
    localStorage.setItem('tournamentBanner', JSON.stringify({
      isClosed: true,
      timestamp: Date.now(),
    }))
    setIsClosed(true)
  }

  const handleClose = () => {
    handleAction()
    setIsClosed(true)
  }

  const handleClick = e => {
    e.preventDefault()
    handleAction()
    setIsClosed(true)
    window.location.href = e?.target?.href ?? '/api/auth/twitch'
  }

  /**
   * Select redirect button component between sign in and redirect
   */
  const RedirectButton = () => {
    if (user) {
      return (
        <a
          className={styles.link}
          href="/adminpanel/tournament"
          onClick={handleClick}
        >
          {t('banner.goToDashboard')}
        </a>
      )
    }
    return (
      <SignInButton
        className={styles.button}
        variant="link"
        onClick={handleClick}
      />
    )
  }

  return (
    <div className={styles.wrapper}>
      <Container className="d-flex justify-content-between align-items-center">
        <p>
          {t('banner.tournament')} <RedirectButton />.
        </p>
        <button className={styles.close} onClick={handleClose}>
          <Image src="/close.svg" width={15} height={15} />
        </button>
      </Container>
    </div>
  )
}