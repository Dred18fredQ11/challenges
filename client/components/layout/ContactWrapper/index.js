import Image from 'next/image'
import classnames from 'classnames'
import styles from './styles.module.css'

export default function ContactWrapper({ className }) {
  const wrapperClass = classnames('d-flex', 'align-items-center', className)
  return (
    <div className={wrapperClass}>
      <a
        className={styles.link}
        target="__blank"
        href="https://discord.gg/KyjunKMWJ9"
      >
        <Image src="/discord-logo.svg" width={30} height={30} />
      </a>
    </div>
  )
}