import Link from 'next/link';
import { useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import { CSSTransition } from 'react-transition-group';

import SignOutButton from 'components/user/SignOutButton';
import UserProfileBlock from 'components/user/UserProfileBlock';
import BurgerBlock from 'components/layout/BurgerBlock';

import { useOutsideClick } from 'hooks/layout.hooks';
import classnames from 'classnames';

import styles from './styles.module.css';

/**
 * Dropdown element with navigation on user pages.
 */
function HeaderDropdown() {
  const { t } = useTranslation('translation');

  // Redux Store Selector
  const isUserFetching = useSelector(state => {
    return state.user.fetchingUserStatus === 'loading';
  });

  // State
  const [isOpen, setIsOpen] = useState(false);
  const wrapperRef = useRef(null);

  // Event handlers
  const handleOpen = () => !isUserFetching && setIsOpen(!isOpen);
  useOutsideClick(wrapperRef, () => setIsOpen(false));

  // Classes CSS
  const toggleClass = classnames(styles.toggle, isOpen && styles.active);

  return (
    <div className={styles.wrapper} ref={wrapperRef}>
      <button className={toggleClass} onClick={handleOpen}>
        <UserProfileBlock className={styles.profile} />
        <BurgerBlock className={styles.burger} isOpen={isOpen} />
      </button>

      <CSSTransition
        in={isOpen}
        timeout={300}
        classNames={{
          enter: styles.dropdownEnter,
          enterActive: styles.dropdownEnterActive,
          enterDone: styles.dropdownEnterDone,
          exit: styles.dropdownExit,
        }}
      >
        <nav className={styles.dropdown}>
          <ul>
            <li>
              <Link href="/dashboard">{t('page.dashboard')}</Link>
            </li>
            <li>
              <SignOutButton />
            </li>
          </ul>
        </nav>
      </CSSTransition>
    </div>
  );
}

export default HeaderDropdown;
