import Head from 'next/head';
import Header from 'components/layout/Header';
import HeaderTiny from 'components/layout/HeaderTiny';
import HeaderDashboard from 'components/layout/HeaderDashboard';
import Footer from 'components/layout/Footer';
import BannerWrapper from 'components/layout/BannerWrapper';
import DashboardSideber from 'components/layout/DashboardSidebar';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Wrapper component for page content.
 * @param {{
 *  children?: JSX.Element;
 *  className?: string;
 *  title?: string;
 *  header?: 'normal' | 'dashboard' | 'tiny' | 'none';
 *  footer?: 'normal' | 'none';
 *  banners?: 'all' | 'none';
 *  sidebar?: 'dashboard' | 'none';
 * }} props
 */
export default function Layout({
  children,
  className,
  title,
  header = 'normal',
  footer = 'normal',
  banners = 'all',
  sidebar = 'none',
}) {
  const mainClass = classnames(
    styles.main,
    sidebar === 'dashboard' && styles.mainDashboard,
    className,
  );

  return (
    <>
      <Head>
        <title>{title ?? 'Dropchallenge'}</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Lobster&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@300;400;600&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Comic+Neue:wght@400;700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@300;400;700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap"
          rel="stylesheet"
        />

        {/* Google Analytics */}
        {/* <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-LYJ83XVHJK"
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'G-LYJ83XVHJK');
            `
          }}
        /> */}
        {/* <script src="//code.jivosite.com/widget/0UHTX3E7z4" async /> */}
      </Head>

      {header === 'normal' && <Header />}
      {header === 'tiny' && <HeaderTiny />}
      {header === 'dashboard' && <HeaderDashboard />}

      {sidebar === 'dashboard' && <DashboardSideber />}

      <main className={mainClass}>{children}</main>

      {footer === 'normal' && <Footer />}

      {banners === 'all' && <BannerWrapper />}
    </>
  );
}
