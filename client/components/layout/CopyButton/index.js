import Image from 'next/image';
import { useState, useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Button to copy specified text in user's clipboard.
 * @param {{ text: string; className?: string; }} props
 */
function CopyButton({ text, className }) {
  const { t } = useTranslation('translation');

  // State Hooks
  const [isCopied, setIsCopied] = useState(false);

  // Effect Hooks
  useEffect(() => {
    const timeout = setTimeout(() => setIsCopied(false), 5000);
    return () => clearTimeout(timeout);
  }, [isCopied]);

  // Event Handlers
  const handleCopy = () => setIsCopied(true);

  // Classes CSS
  const buttonClass = classnames(
    styles.button,
    isCopied && styles.buttonCopied,
    className,
  );

  return (
    <CopyToClipboard text={text} onCopy={handleCopy}>
      <button className={buttonClass}>
        <span className={styles.text}>
          {!isCopied
            ? t('layout.copyButton.copy')
            : t('layout.copyButton.copied')}
        </span>
        <span className={styles.copyIcon}>
          <Image src="/assets/copy.svg" width={25} height={25} />
        </span>
        <span className={styles.copiedIcon}>
          <Image src="/assets/ok.svg" width={25} height={25} />
        </span>
      </button>
    </CopyToClipboard>
  );
}

export default CopyButton;
