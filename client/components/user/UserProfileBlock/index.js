import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import LoaderPlaceholder from 'components/layout/LoaderPlaceholder';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Block user's information.
 * @param {{ className?: string }} props
 */
function UserProfileBlock({ className }) {
  const { t } = useTranslation('translation');

  // Selector Hooks
  const user = useSelector(state => state.user.user);

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  if (!user) {
    return (
      <div className={wrapperClass}>
        <div className={styles.nickname}>{t('utils.isLoading')}</div>
        <LoaderPlaceholder className={styles.avatar} width={25} height={25} />
      </div>
    );
  }

  return (
    <div className={wrapperClass}>
      <div className={styles.nickname}>{user?.displayName}</div>
      <img className={styles.avatar} src={user?.profileImageUrl} alt="User" />
    </div>
  );
}

export default UserProfileBlock;
