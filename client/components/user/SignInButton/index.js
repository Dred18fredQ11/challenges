import { useTranslation } from 'next-i18next';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Button for user's authorization.
 * 
 * There are such variants: `normal` - in header, `large` - home page, `link` -
 * different banners.
 * 
 * It is neccessary to process link redirect for setting `onClick` event
 * handler.
 * 
 * @param {{
 *  className?: string;
 *  variant?: 'normal' | 'large' | 'link';
 *  onClick?: Function;
 * }} props
 */
function SignInButton({ className, variant = 'normal', onClick = () => {} }) {
  const { t } = useTranslation('translation');

  // Classes CSS
  const wrapperClass = classnames(
    styles.wrapper,
    variant === 'large' && styles.variantLarge,
    variant === 'link' && styles.variantLink,
    className,
  );

  // Content HTML
  const textContent = (() => {
    switch (variant) {
      case 'normal':
        return t('user.SignInButton.normal');
      case 'large':
        return t('user.SignInButton.large');
      case 'link':
        return t('user.SignInButton.link');
      default:
        return t('utils.signIn');
    }
  })();

  return (
    <a className={wrapperClass} href="/api/auth/twitch" onClick={onClick}>
      <span className={styles.content}>{textContent}</span>
      <span className={styles.contentHover}>{textContent}</span>
    </a>
  );
}

export default SignInButton;
