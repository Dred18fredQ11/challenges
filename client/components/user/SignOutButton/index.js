import { useTranslation } from 'next-i18next';
import { useDispatch } from 'react-redux';
import { logoutAction } from 'actions/user.actions';
import classnames from 'classnames';

/**
 * Component that performs logout action.
 * @param {{ className?: string }} props
 */
function SignOutButton({ className }) {
  const { t } = useTranslation('translation');
  const dispatch = useDispatch();

  // Event Handlers
  const handleClick = async () => {
    dispatch(logoutAction());
  };

  // Classes CSS
  const buttonClass = classnames(className);

  return (
    <button className={buttonClass} onClick={handleClick}>
      {t('utils.signOut')}
    </button>
  );
}

export default SignOutButton;
