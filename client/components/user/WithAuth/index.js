import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import Layout from 'components/layout/Layout';
import LoaderPlaceholder from 'components/layout/LoaderPlaceholder';

/**
 * HOC to check if user is authentificated. If not - redirects to the home page.
 * @param {JSX.Element} Component
 */
function WithAuth(Component) {
  function Auth(props) {
    const router = useRouter();

    // State Hooks
    const [isLoading, setIsLoading] = useState(true);

    // Selector Hooks
    const isAuth = useSelector(state => {
      const fetchingStatus = state.user.fetchingUserStatus;
      return fetchingStatus === 'succeeded';
    });

    // Effect Hooks
    useEffect(() => {
      const token = localStorage.getItem('jwt')
      if (!token && !isAuth) return router.push('/');
      setIsLoading(false)
    }, [isAuth]);

    if (isLoading) {
      return (
        <Layout
          title="Dropchallenge - Loading"
          header="none"
          footer="none"
          sidebar="none"
        >
          <LoaderPlaceholder />
        </Layout>
      );
    }

    return <Component {...props} />;
  }

  return Auth;
}

export default WithAuth;
