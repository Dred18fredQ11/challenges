import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import CopyButton from 'components/layout/CopyButton';
import { getPathURL } from 'services/utils';
import classnames from 'classnames';
import styles from './styles.module.css';

function DonationManagerBlock({ className }) {
  // State Hooks
  const [url, setUrl] = useState('');
  
  // Selector Hooks
  const user = useSelector(state => state.user.user);
  
  // Effect Hooks
  useEffect(() => {
    if (user) setUrl(getPathURL(`challenge/${user.login}`));
  }, [user])
  
  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <h2 className={styles.title}>Страница заказа Челленджей:</h2>
      <div className={styles.field}>
        <div className={styles.linkContainer}>{url}</div>
        <CopyButton text={url} />
      </div>
    </div>
  );
}

export default DonationManagerBlock;
