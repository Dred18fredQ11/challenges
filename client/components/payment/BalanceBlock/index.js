import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Block that displays information about balance status.
 * @param {{ className?: string }} props
 */
function BalanceBlock({ className }) {
  const { t } = useTranslation('translation');

  // Selector Hooks
  const isLoading = useSelector(state => {
    return state.user.fetchingStatus === 'loading';
  });

  const balance = useSelector(state => {
    const balance = state.user.user?.balance ?? 0;
    return balance.toFixed(2);
  });

  // Class CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <span>{t('payment.balance')}:</span> {!isLoading ? balance : '...'} RUB
    </div>
  );
}

export default BalanceBlock;
