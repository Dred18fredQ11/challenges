import { useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import CloseButton from 'components/layout/CloseButton';
import CustomizationPreview from 'components/widget/CustomizationPreview';
import CustomizationSelector from 'components/widget/CustomizationSelector';
import CustomizationTextForm from 'components/widget/CustomizationTextForm';
import { closeWidgetCustomizationAction } from 'actions/widget.actions';
import { useOutsideClick } from 'hooks/layout.hooks';
import styles from './styles.module.css';

/**
 * Modal for customization element.
 */
function CustomizationModal() {
  const dispatch = useDispatch();

  // State Hooks
  const dialogRef = useRef();

  // Selector Hooks
  const isOpen = useSelector(state => state.widget.customization.isOpen);
  const elementType = useSelector(state => {
    return state.widget.customization.element?.type;
  });

  // Effect Hooks
  useEffect(() => {
    if (isOpen) document.body.style.overflow = 'hidden';
    return () => (document.body.style.overflow = 'unset');
  }, [isOpen]);

  // Event Handlers
  const handleClose = () => dispatch(closeWidgetCustomizationAction());
  useOutsideClick(dialogRef, () => dispatch(closeWidgetCustomizationAction()));

  return (
    <CSSTransition
      in={isOpen}
      timeout={500}
      classNames={{
        enter: styles.modalEnter,
        enterActive: styles.modalEnterActive,
        enterDone: styles.modalEnterDone,
        exit: styles.modalExit,
      }}
    >
      <div className={styles.modal}>
        <div className={styles.dialog} ref={dialogRef}>
          <CloseButton className={styles.close} onClick={handleClose} />
          <div className={styles.settings}>
            <CustomizationSelector className={styles.selector} />
            {elementType === 'text' && (
              <CustomizationTextForm className={styles.form} />
            )}
          </div>
          <CustomizationPreview className={styles.preview} />
        </div>
      </div>
    </CSSTransition>
  );
}

export default CustomizationModal;
