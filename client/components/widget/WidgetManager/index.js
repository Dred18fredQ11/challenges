import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import CustomizeButton from 'components/widget/CustomizationButton';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Component for manage widget preferences
 * @param {{ widgetId: number; className?: srtring; }} props
 */
function WidgetManager({ widgetId, className }) {
  const { t } = useTranslation();

  const widget = useSelector(state => state?.widget?.widgetMap[widgetId]);

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <h2 className={styles.title}>Оповещение о новом челлендже</h2>
      <p className={styles.description}>
        Описание функционала виджета. Описание функционала виджета
      </p>
      <CustomizeButton className={styles.button} widgetId={widgetId} />
    </div>
  );
}

export default WidgetManager;
