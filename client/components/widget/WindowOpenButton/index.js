import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Button to open specified url link in window.
 * @param {{ url: string; className?: string; }} props
 */
function WindowOpenButton({ url, className }) {
  const { t } = useTranslation('translation');

  // Event Handlers
  const handleClick = () => {
    window.open(url, 'WidgetWindow', 'resizable=yes,scrollbars=yes,status=yes');
  };

  // Classes CSS
  const buttonClass = classnames(styles.button, className);

  return (
    <button className={buttonClass} onClick={handleClick}>
      <span className={styles.text}>{t('widget.play')}</span>
      <Image src="/assets/play.svg" width={20} height={22.5} />
    </button>
  )
}

export default WindowOpenButton;
