import { useSelector, useDispatch } from 'react-redux';
import CustomSelect from 'components/layout/CustomSelect';
import ColorInput from 'components/layout/ColorInput';
import RangeInput from 'components/layout/RangeInput';
import {
  setCustomizationElementAction,
  updateWidgetElementAction,
  closeWidgetCustomizationAction,
} from 'actions/widget.actions';
import classnames from 'classnames';
import styles from './styles.module.css';

function selectTextElementCSSFields({ element }) {
  const { fontFamily, color, fontSize, fontWeight, fontStyle } = element;
  return { fontFamily, color, fontSize, fontWeight, fontStyle };
}

function CustomizationTextForm({ className }) {
  const dispatch = useDispatch();

  // Selector Hooks
  const element = useSelector(state => state.widget.customization.element);

  // Utils
  const fontFamilyOptions = [
    { value: 'Roboto', label: 'Roboto' },
    { value: 'Open Sans', label: 'Open Sans' },
    { value: 'Lobster', label: 'Lobster' },
    { value: 'Ubuntu', label: 'Ubuntu' },
    { value: 'Roboto Mono', label: 'Roboto Mono' },
    { value: 'Source Sans Pro', label: 'Source Sans Pro' },
    { value: 'Comic Neue', label: 'Comic Neue' },
    { value: 'Poppins', label: 'Poppins' },
    { value: 'Fira Sans', label: 'Fira Sans' },
    { value: 'Montserrat', label: 'Montserrat' },
  ];
  const fontFamilyStyles = {
    option: (styles, { data: { value } }) => {
      return { ...styles, fontFamily: value };
    },
  };
  const fontFamilyValue = fontFamilyOptions.find(option => {
    return option.value === element.fontFamily;
  });

  // Event handlers
  const handleSubmit = async e => {
    e.preventDefault();
    dispatch(
      updateWidgetElementAction({
        elementId: element.id,
        elementProps: selectTextElementCSSFields({ element }),
      }),
    );
  };

  const handleChange = valuesMap => {
    const elementProps = { ...element, ...valuesMap };
    dispatch(setCustomizationElementAction({ elementProps }));
  };

  // TODO: Write data validation
  const handleChangeFontFamily = ({ value }) => {
    handleChange({ fontFamily: value });
  };

  const handleColorChange = e => handleChange({ color: e.target.value });

  const handleSizeChange = e => {
    return handleChange({ fontSize: e.target.value });
  };

  const handleClose = () => dispatch(closeWidgetCustomizationAction());

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <form className={wrapperClass} onSubmit={handleSubmit}>
      <h2>Оглавление Виджета Настройки:</h2>
      <fieldset>
        <label>Шрифт:</label>
        <CustomSelect
          instanceId="fontFamily"
          styles={fontFamilyStyles}
          options={fontFamilyOptions}
          onChange={handleChangeFontFamily}
          value={fontFamilyValue}
        />
      </fieldset>
      <fieldset>
        <label>Цвет:</label>
        <ColorInput value={element.color} onChange={handleColorChange} />
      </fieldset>
      <fieldset>
        <label>Размер шрифта:</label>
        <RangeInput value={element.fontSize} onChange={handleSizeChange} />
      </fieldset>
      <fieldset className={styles.buttonWrapper}>
        <button type="submit" className={styles.accept}>
          Принять
        </button>
        <button className={styles.decline} onClick={handleClose}>
          Отмена
        </button>
      </fieldset>
    </form>
  );
}

export default CustomizationTextForm;
