import Image from 'next/image';
import { useState, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import { useOutsideClick } from 'hooks/layout.hooks';
import { openWidgetCustomizationAction } from 'actions/widget.actions';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Component that opens widget customization modal component with specified
 * widget id.
 * @param {{ widgetId?: number; className?: string; }} values
 */
function CustomizationButton({ widgetId, className }) {
  const dispatch = useDispatch();

  // State Hooks
  const [isOpen, setIsOpen] = useState(false);
  const wrapperRef = useRef();

  // Event Handlers
  const handleOpen = () => setIsOpen(!isOpen);
  const handleCustomizationOpen = () => {
    dispatch(openWidgetCustomizationAction({ widgetId }));
  };

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);
  useOutsideClick(wrapperRef, () => setIsOpen(false));

  return (
    <div className={wrapperClass} ref={wrapperRef}>
      <button className={styles.button} onClick={handleOpen}>
        <Image
          src="/assets/widget-settings.svg"
          width={30}
          height={30}
          alt="Widget Customization"
        />
      </button>
      <CSSTransition
        in={isOpen}
        timeout={300}
        classNames={{
          enter: styles.dropdownEnter,
          enterActive: styles.dropdownEnterActive,
          enterDone: styles.dropdownEnterDone,
          exit: styles.dropdownExit,
        }}
      >
        <div className={styles.dropdown}>
          <button onClick={handleCustomizationOpen}>Настройки</button>
          {/* <button>Тестовое Сообщение</button> */}
        </div>
      </CSSTransition>
    </div>
  );
}

export default CustomizationButton;
