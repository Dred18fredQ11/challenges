import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { selectWidgetCustomizationAction } from 'actions/widget.actions';
import { createSelector } from 'reselect';
import CustomSelect from 'components/layout/CustomSelect';
import classnames from 'classnames';
import styles from './styles.module.css';

// Selectors
const selectElementList = createSelector(
  state => state.widget.customization.widgetId,
  state => state.widget.elementMap,
  (widgetId, elementMap) => {
    return Object.values(elementMap).filter(element => {
      return element.widgetId === widgetId;
    });
  },
);

/**
 * Component that contains list of widget elements and allows select one to
 * customization.
 */
export default function CustomizationSelector({ className }) {
  const dispatch = useDispatch();
  const { t } = useTranslation('translation');

  // Selector Hooks
  const elementList = useSelector(selectElementList);
  const selectedElement = useSelector(state => {
    return state.widget.customization.element;
  });

  // Effect Hooks
  useEffect(() => {
    if (!selectedElement && elementList.length > 0) {
      dispatch(
        selectWidgetCustomizationAction({ elementId: elementList[0].id }),
      );
    }
  }, [selectedElement, elementList]);

  // Utils
  const options = elementList.map(element => ({
    value: element.id,
    label: t(`widget.element.${element.name}`),
  }));
  const selectedElementOption = {
    value: selectedElement?.id ?? null,
    label: t(`widget.element.${selectedElement?.name}`),
  };

  // Event handlers
  const handleSelect = ({ value }) => {
    const elementId = value !== 'null' ? parseInt(value) : null;
    dispatch(selectWidgetCustomizationAction({ elementId }));
  };

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <h2 className={styles.title}>
        {t('widget.CustomizationSelector.title')}:
      </h2>
      <CustomSelect
        instanceId="customizationSelect"
        options={options}
        onChange={handleSelect}
        value={selectedElementOption}
      />
    </div>
  );
}
