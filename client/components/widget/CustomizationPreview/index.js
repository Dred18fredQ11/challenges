import { useSelector } from 'react-redux';
// import { useTranslation } from 'react-i18next'
import { createSelector } from 'reselect';
import classnames from 'classnames';
import styles from './styles.module.css';

// Utils
function parseStyles(styles) {
  if (!styles) return {}
  return { ...styles, fontSize: `${styles.fontSize}px` };
}

// Selectors
const selectElement = createSelector(
  state => state.widget.customization.widgetId,
  state => state.widget.elementMap,
  state => state.widget.customization?.element,
  (_, name) => name,
  (widgetId, elementMap, element, name) => {
    if (name === element?.name) {
      return element;
    }
    return Object.values(elementMap).find(element => {
      return element.widgetId === widgetId && element.name === name;
    });
  },
);

/**
 * 
 * @param {*} props
 */
function CustomizationPreview({ className }) {
  // Selector Hooks
  const title = useSelector(state => selectElement(state, 'title'));
  const text = useSelector(state => selectElement(state, 'text'));

  // Utils
  

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <div style={parseStyles(title) || {}}>aasdas asdsadsad asdsad</div>
      <div style={parseStyles(text) || {}}>asdsad</div>
    </div>
  );
}

export default CustomizationPreview;
