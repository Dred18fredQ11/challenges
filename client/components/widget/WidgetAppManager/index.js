import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import LoaderPlaceholder from 'components/layout/LoaderPlaceholder';
import CopyButton from 'components/layout/CopyButton';
import WindowOpenButton from 'components/widget/WindowOpenButton';

import { getPathURL } from 'services/utils';

import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Component for launching widget app.
 * @param {{ widgetAppId: number; className?: string }} props
 */
function WidgetAppManager({ widgetAppId, className }) {
  const { t } = useTranslation('translation');

  // State Hooks
  const [url, setUrl] = useState(null);

  // Selector Hooks
  const isWidgetFetching = useSelector(state => {
    const status = state.widget.fetchingAppListStatus;
    return status === 'idle' || status === 'loading' || status === 'failed';
  });
  const widgetApp = useSelector(state => {
    return state.widget.widgetAppMap[widgetAppId];
  });

  // Effect Hooks
  useEffect(() => {
    if (isWidgetFetching) return;
    setUrl(getPathURL(`widget?token=${widgetApp.token}`));
  }, [isWidgetFetching]);

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  if (isWidgetFetching) {
    return (
      <div className={wrapperClass}>
        <LoaderPlaceholder />
      </div>
    );
  }

  return (
    <div className={wrapperClass}>
      <h2 className={styles.title}>
        {t(`widget.widgetAppManager.title.${widgetApp.name}`)}:
      </h2>
      <h3 className={styles.subtitle}>
        {t('widget.widgetAppManager.subtitle')}:
      </h3>
      <div className={styles.field}>
        <div className={styles.linkWrapper}>{url}</div>
        <CopyButton text={url} />
        <WindowOpenButton url={url} />
      </div>
    </div>
  );
}

export default WidgetAppManager;
