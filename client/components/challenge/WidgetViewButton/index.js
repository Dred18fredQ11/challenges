import Image from 'next/image';
import { useSelector, useDispatch } from 'react-redux';
import { createSelector } from 'reselect';
import {
  showChallengeOnWidgetAction,
  hideChallengeOnWidgetAction,
} from 'actions/widget.actions';
import classnames from 'classnames';
import styles from './styles.module.css';

// Selectors
const selectWidget = createSelector(
  state => state.widget.widgetMap,
  widgetMap =>
    Object.values(widgetMap).find(widget => {
      return widget.name === 'challengeBanner';
    }),
);

/**
 * Component used to show component on widget `challengeBanner`
 * @param {{ challengeId: number; className?: string; }} props
 */
function WidgetViewButton({ challengeId, className }) {
  const dispatch = useDispatch();

  // Redux Selector Hooks
  const isLoading = useSelector(state => {
    const status = state.widget.fetchingWidgetListStatus;
    return status === 'idle' || status === 'loading';
  });
  const widget = useSelector(selectWidget);

  // Utils
  const isActive = !isLoading && (widget.challengeId === challengeId);
  const handleClickAction = (() => {
    if (isLoading) {
      return () => {};
    }
    if (!isActive) {
      return showChallengeOnWidgetAction({ widgetId: widget.id, challengeId });
    }
    return hideChallengeOnWidgetAction({ widgetId: widget.id });
  })()

  // Event Listeners
  const handleClick = async () => dispatch(handleClickAction);

  // Classes CSS
  const buttonClass = classnames(
    styles.button,
    isActive && styles.buttonActive,
    className,
  );

  return (
    <button
      className={buttonClass}
      onClick={handleClick}
    >
      <Image
        src="/assets/view.svg"
        width={30}
        height={30}
        alt="View Widget"
      />
    </button>
  );
}

export default WidgetViewButton;
