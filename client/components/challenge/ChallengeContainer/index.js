import { useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { createSelector } from 'reselect';
import classnames from 'classnames';

import LoaderPlaceholder from 'components/layout/LoaderPlaceholder';
import ChallengeItem from 'components/challenge/ChallengeItem';
import FilterSelection from 'components/challenge/FilterSelection';
import { fetchChallengeListAction } from 'actions/challenge.actions';
import styles from './styles.module.css';

// Utils Selectors
const selectChallengeList = createSelector(
  state => state.challenge.challengeMap,
  state => state.challenge.filteredStatuses,
  (challengeMap, filteredStatuses) => {
    const filteredStatusesList = Object.entries(filteredStatuses)
      .filter(tuple => tuple[1])
      .map(tuple => tuple[0]);

    const challengeList = Object.values(challengeMap)
      .filter(challenge => filteredStatusesList.includes(challenge.status))
      .sort((a, b) => Date(a.createdAt) - Date(b.createdAt));

    return challengeList;
  },
);

/**
 * Component that contains list of challenges.
 * @param { className?: string; } values
 */
function ChallengeContainer({ className }) {
  const { t } = useTranslation('translation');
  const dispatch = useDispatch();

  // Redux Selector Hooks
  const isLoading = useSelector(state => {
    const status = state.challenge.fetchingStatus;
    return status === 'idle' || status === 'loading';
  });
  const challengeList = useSelector(selectChallengeList);

  // Effect Hooks
  useEffect(() => {
    dispatch(fetchChallengeListAction({}));
  }, [dispatch]);

  // Classes CSS
  const wrapperClass = classnames(
    styles.wrapper,
    isLoading && styles.wrapperLoading,
    className,
  );

  if (isLoading) {
    return (
      <div className={wrapperClass}>
        <LoaderPlaceholder />
      </div>
    );
  }

  return (
    <div className={wrapperClass}>
      <FilterSelection className={styles.filter} />
      {challengeList.length === 0 && (
        <div className={styles.empty}>
          <p>{t('challenge.utils.empty')}</p>
        </div>
      )}
      {challengeList.map(challenge => (
        <ChallengeItem
          key={challenge.id}
          challengeId={challenge.id}
          className={styles.challengeItem}
        />
      ))}
    </div>
  );
}

export default ChallengeContainer;
