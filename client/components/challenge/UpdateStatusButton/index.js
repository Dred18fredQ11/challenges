import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import { updateChallengeStatusAction } from 'actions/challenge.actions';
import styles from './styles.module.css';

/**
 * Button component for updating status of challenge.
 * @param {{
 *  challengeId: number;
 *  newStatus: 'approved' | 'declined' | 'done' | 'failed';
 *  variant: 'green' | 'red';
 *  className?: string;
 * }} props
 */
function UpdateStatusButton({
  challengeId,
  newStatus,
  variant = 'green',
  className,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation('translation');

  // Redux Selector Hooks
  const isFetching = useSelector(state => {
    return state.challenge.challengeMap[challengeId].isFetching;
  });

  // Event Handlers
  const handleClick = async () => {
    dispatch(updateChallengeStatusAction({ challengeId, newStatus }));
  };

  // Classess CSS
  const buttonClass = classnames(
    styles.button,
    variant === 'green' && styles.buttonGreen,
    variant === 'red' && styles.buttonRed,
    className,
  );

  return (
    <button className={buttonClass} onClick={handleClick} disabled={isFetching}>
      {t(`challenge.newStatus.${newStatus}`)}
    </button>
  );
}

export default UpdateStatusButton;
