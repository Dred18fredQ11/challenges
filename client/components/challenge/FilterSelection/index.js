import { useState, useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { CSSTransition } from 'react-transition-group';
import { useOutsideClick } from 'hooks/layout.hooks';
import { filterChallengeListAction } from 'actions/challenge.actions';
import { createSelector } from 'reselect';
import classnames from 'classnames';
import styles from './styles.module.css';

// Selectors
const selectFilteredStatuses = createSelector(
  state => state.challenge.filteredStatuses,
  filteredStatuses =>
    Object.entries(filteredStatuses)
      .filter(tuple => tuple[1])
      .map(tuple => tuple[0]),
);

/**
 * Controls loaded statuses of challenge.
 * @param {{ className?: string }} props
 */
export default function ChallengeFilterSelection({ className }) {
  const dispatch = useDispatch();
  const { t } = useTranslation('translation');

  // Redux Seletors Hooks
  const filteredStatusesList = useSelector(selectFilteredStatuses);

  // State Hooks
  const [isOpen, setIsOpen] = useState(false);
  const wrapperRef = useRef(null);

  // Effect Hooks
  useEffect(() => {
    const storageData = localStorage.getItem('filteredStatuses');
    const storageValue = storageData ? JSON.parse(storageData) : {};
    const initValue = {
      pending: true,
      declined: true,
      approved: true,
      failed: true,
      done: true,
    };
    const filteredStatuses = { ...initValue, ...storageValue };
    dispatch(filterChallengeListAction({ filteredStatuses }));
  }, []);

  // Utils
  const statusList = ['pending', 'declined', 'approved', 'failed', 'done'];
  const getCheckboxClass = status =>
    classnames(
      styles.checkbox,
      filteredStatusesList.includes(status) && styles.checked,
    );

  // Action handlers
  const handleOpen = () => setIsOpen(!isOpen);
  const handleCheck = status => () => {
    const allow = !filteredStatusesList.includes(status);
    const filteredStatuses = { [status]: allow };
    dispatch(filterChallengeListAction({ filteredStatuses }));
  };
  useOutsideClick(wrapperRef, () => setIsOpen(false));

  // CSS Classes
  const wrapperClass = classnames(styles.wrapper, className);
  const buttonClass = classnames(styles.button, isOpen && styles.isOpen);
  const dropdownClass = classnames(styles.dropdown, isOpen && styles.isOpen);

  return (
    <div className={wrapperClass} ref={wrapperRef}>
      <button className={buttonClass} onClick={handleOpen}>
        {t(`challenge.utils.filter`)}
      </button>
      <CSSTransition
        in={isOpen}
        timeout={300}
        classNames={{
          enter: styles.dropdownEnter,
          enterActive: styles.dropdownEnterActive,
          enterDone: styles.dropdownEnterDone,
          exit: styles.dropdownExit,
        }}
      >
        <div className={dropdownClass}>
          {statusList.map(status => (
            <button
              key={status}
              className={getCheckboxClass(status)}
              onClick={handleCheck(status)}
            >
              {t(`challenge.status.${status}`)}
            </button>
          ))}
        </div>
      </CSSTransition>
    </div>
  );
}
