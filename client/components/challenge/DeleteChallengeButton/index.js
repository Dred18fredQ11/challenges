import Image from 'next/image';
import { useDispatch } from 'react-redux';
import { fetchChallengeListAction } from 'actions/challenge.actions';
import { hideChallenge } from 'services/api/challenge.services';
import styles from './styles.module.css';

function DeleteChallengeButton({ challengeId }) {
  const dispatch = useDispatch();
  const handleClick = async () => {
    await hideChallenge({ challengeId });
    dispatch(fetchChallengeListAction({}));
  };

  return (
    <button className={styles.button} onClick={handleClick}>
      <Image src="/assets/delete.svg" width={30} height={30} />
    </button>
  );
}

export default DeleteChallengeButton;
