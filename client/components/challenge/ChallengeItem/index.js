import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import UpdateStatusButton from 'components/challenge/UpdateStatusButton';
import WidgetViewButton from 'components/challenge/WidgetViewButton';
import DeleteChallengeButton from 'components/challenge/DeleteChallengeButton';
import { formatDate } from 'services/utils';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Component wrapper for challenge fields.
 * @param {{ challengeId: number; className?: string; }} props
 */
function ChallengeItem({ challengeId, className }) {
  const { t } = useTranslation('translation');

  // Redux Store Selectors
  const challenge = useSelector(state => {
    return state.challenge.challengeMap[challengeId];
  });

  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <div className={styles.header}>
        <div>
          {t('challenge.item.from')}: <b>{challenge.creatorName}</b>{' '}
          {t('challenge.item.amount')} {challenge.price ?? 0}{' '}
          {challenge.currency ?? 'RUB'} {formatDate(challenge.createdAt)}{' '}
          {t('challenge.item.status')}:{' '}
          {t(`challenge.status.${challenge.status}`)}
        </div>
        <div className={styles.controls}>
          {challenge.status === 'pending' && <>
            <UpdateStatusButton
              challengeId={challengeId}
              newStatus="approved"
              variant="green"
              />
            <UpdateStatusButton
              challengeId={challengeId}
              newStatus="declined"
              variant="red"
            />
          </>}
          {challenge.status === 'approved' && <>
            <UpdateStatusButton
              challengeId={challengeId}
              newStatus="done"
              variant="green"
              />
            <UpdateStatusButton
              challengeId={challengeId}
              newStatus="failed"
              variant="red"
            />
          </>}
          <WidgetViewButton challengeId={challengeId} />
          <DeleteChallengeButton challengeId={challengeId} />
        </div>
      </div>
      <p className={styles.body}>{challenge.description}</p>
    </div>
  );
}

export default ChallengeItem;
