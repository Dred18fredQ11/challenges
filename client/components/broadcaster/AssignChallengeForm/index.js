import { useState } from 'react';
import { useTranslation } from 'next-i18next';
import { NotificationManager } from 'react-notifications';
import CustomSelect from 'components/layout/CustomSelect';

import InputField from 'components/common/InputField';

import { assignChallenge } from 'services/api/challenge.services';
import classnames from 'classnames';
import styles from './styles.module.css';

const currencyDict = {
  RUB: 'minRubSum',
  USD: 'minUsdSum',
  UAH: 'minUahSum',
};

/**
 *
 * @param {{
 *  broadcaster: {
 *    id: number;
 *    minRubSum: number;
 *    minUsdSum: number;
 *    minUahSum: number;
 *    [key]: any;
 *  };
 *  className?: string;
 * }} props
 */
function AssignChallengeForm({ broadcaster, className }) {
  const { t } = useTranslation('translation');

  // State Hooks
  const [isLoading, setIsLoading] = useState(false);

  // Nickname Field
  const [nickname, setNickname] = useState('');
  const [nicknameMessage, setNicknameMessage] = useState('');
  const handleNicknameChange = e => setNickname(e.target.value);
  const validateNickname = () => {
    if (nickname.length < 6 || nickname.length > 16) {
      setNicknameMessage(t('broadcaster.AssignChallengeForm.nickname.error'));
      return false;
    }
    return true;
  };

  // Currency Field
  const [currencyOption, setCurrencyOption] = useState({
    value: 'RUB',
    label: 'RUB',
  });
  const currencyOptionList = [
    { value: 'RUB', label: 'RUB' },
    { value: 'UAH', label: 'UAH' },
    { value: 'USD', label: 'USD' },
  ];
  const handleCurrencyChange = ({ value }) => {
    if (!['RUB', 'UAH', 'USD'].includes(value)) {
      return NotificationManager.error('Неверный тип валюты');
    }
    return setCurrencyOption(
      currencyOptionList.find(option => option.value === value),
    );
  };
  const validateCurrency = () => {
    if (!['RUB', 'UAH', 'USD'].includes(currencyOption.value)) {
      NotificationManager.error('Неверный тип валюты');
      return false;
    }
    return true;
  };

  // Amount Field
  const minAmount = broadcaster[currencyDict[currencyOption.value]];
  const [amount, setAmount] = useState(minAmount ?? '1.00');
  const [amountMessage, setAmountMessage] = useState('');
  const handleAmountChange = e => {
    const value = e.target.value;
    if (value?.length > 6) return;
    setAmount(value?.length > 0 ? value : '0.00');
  };
  const validateAmount = () => {
    if (!amount || isNaN(parseFloat(amount))) {
      setAmountMessage(t('broadcaster.AssignChallengeForm.amount.incorrect'));
      return false;
    } else if (parseFloat(amount) < minAmount) {
      setAmountMessage(t('broadcaster.AssignChallengeForm.amount.lessMin'));
      return false;
    }
    return true;
  };

  // Description Field
  const [description, setDescription] = useState('');
  const handleDescriptionChange = e => setDescription(e.target.value);

  // Utils Functions
  const clearMessages = () => {
    setNicknameMessage('');
    setAmountMessage('');
  };
  const validateFields = () => {
    return [validateNickname(), validateCurrency(), validateAmount()].every(
      validation => validation,
    );
  };

  // Event Handlers
  const handleSubmit = e => {
    e.preventDefault();
    setIsLoading(true);
    clearMessages();
    if (!validateFields()) {
      NotificationManager.error('Неверно заполненные поля');
      return setIsLoading(false);
    }
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);
    // assignChallenge()
    // Do stuff
  };

  // Classes CSS
  const formClass = classnames(styles.form, className);
  const currencyStyles = {
    control: base => ({
      ...base,
      height: '60px',
    }),
    singleValue: base => ({
      ...base,
      ...(isLoading ? { color: '#3A3A3C' } : {}),
    }),
  };

  const descriptionClass = classnames(
    styles.textarea,
    isLoading && styles.inputDisabled,
  );
  const submitClass = classnames(
    styles.submit,
    isLoading && styles.submitDisabled,
  );

  return (
    <form className={formClass} onSubmit={handleSubmit}>
      <InputField
        className={styles.field}
        label={`${t('broadcaster.AssignChallengeForm.nickname.label')}:`}
        id="nickname"
        name="nickname"
        value={nickname}
        onChange={handleNicknameChange}
        disabled={isLoading}
      />
      <fieldset className={styles.fieldRow}>
        <InputField
          className={styles.amountInput}
          label={`${t('broadcaster.AssignChallengeForm.amount.label')}:`}
          id="amount"
          name="amount"
          value={amount}
          onChange={handleAmountChange}
          disabled={isLoading}
        />
        <CustomSelect
          className={styles.currencySelect}
          styles={currencyStyles}
          instanceId="currency"
          name="currency"
          options={currencyOptionList}
          value={currencyOption}
          onChange={handleCurrencyChange}
          disabled={isLoading}
        />
      </fieldset>




      {/* <fieldset className={styles.fieldset}>
        <label className={styles.label}>
          {t('broadcaster.AssignChallengeForm.amount.label')}
        </label>
        <div className={styles.fieldRow}>
          <CustomSelect
            className={styles.currency}
            styles={currencyStyles}
            instanceId="currency"
            name="currency"
            options={currencyOptionList}
            value={currencyOption}
            onChange={handleCurrencyChange}
            disabled={isLoading}
          />
          <input
            className={amountClass}
            id="amount"
            name="amount"
            type="number"
            value={amount}
            onChange={handleAmountChange}
            disabled={isLoading}
          />
        </div>
        {amountMessage && (
          <span className={styles.errorMessage}>{amountMessage}</span>
        )}
      </fieldset> */}
      <fieldset className={styles.fieldset}>
        <label className={styles.label}>
          {t('broadcaster.AssignChallengeForm.description.label')}
        </label>
        <textarea
          className={descriptionClass}
          id="description"
          name="description"
          value={description}
          onChange={handleDescriptionChange}
          disabled={isLoading}
        />
      </fieldset>
      <button className={submitClass} disabled={isLoading}>
        {t('broadcaster.AssignChallengeForm.submit')}
      </button>
    </form>
  );
}

export default AssignChallengeForm;
