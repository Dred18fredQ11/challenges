import { useState } from 'react';
import Image from 'next/image';
import TailspinLoader from 'components/animations/TailspinLoader';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Styled input field with label.
 * If `errorMessage` properthy specified - component will change styles.
 * @param {{
 *  label: string;
 *  id: string;
 *  name: string;
 *  value: string;
 *  onChange: Function;
 *  errorMessage?: string;
 *  disabled?: boolean;
 *  type?: 'text' | 'number';
 *  className?: string;
 * }} props
 */
function InputField({
  label,
  id,
  name,
  value,
  onChange,
  errorMessage = '',
  disabled = false,
  type = 'text',
  className,
}) {
  // State Hooks
  const [isFocus, setIsFocus] = useState(false);

  // Event Handlers
  const handleFocus = () => setIsFocus(true);
  const handleBlur = () => setIsFocus(false);

  // Classes CSS
  const fieldsetClass = classnames(
    styles.fieldset,
    (isFocus || value) && styles.fieldsetFocused,
    (errorMessage && !disabled) && styles.fieldsetError,
    disabled && styles.fieldsetDisabled,
    className,
  );

  return (
    <fieldset className={fieldsetClass}>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>
      <input
        id={id}
        name={name}
        className={styles.input}
        type={type}
        value={value}
        onChange={onChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
        disabled={disabled}
      />
      {disabled && <TailspinLoader className={styles.loader} />}
      {(errorMessage && !disabled) && (
        <>
          <span className={styles.errorIcon}>
            <Image
              src="/assets/inputErrorIcon.svg"
              width={25}
              height={25}
              alt="Input Error"
            />
          </span>
          <span className={styles.errorMessage}>{errorMessage}</span>
        </>
      )}
    </fieldset>
  );
}

export default InputField;
