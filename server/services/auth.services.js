const axios = require('axios')
const jsonwebtoken = require('jsonwebtoken')
const { User } = require('../data')
const { TwitchAPIRequestError } = require('../utils/errors')
const config = require('../config')

/**
 * Returns URL for user redirect for authorization in Twitch.
 * @returns {URL} URL instance.
 */
function formTwitchAuthUrl() {
  const url = new URL('https://id.twitch.tv/oauth2/authorize')

  const params = {
    client_id: config.twitch.clientId,
    redirect_uri: config.twitch.callbackUrl,
    response_type: 'code',
    scope: 'user_read',
  }

  Object.entries(params).forEach(([key, value]) => {
    url.searchParams.append(key, value)
  })

  return url
}

/**
 * Returns URL for accessing Token from Twitch API.
 * @param {{ code: string }} values
 * @returns {URL} URL instance.
 */
function formTwitchTokenAccessUrl({ code }) {
  const url = new URL('https://id.twitch.tv/oauth2/token')

  const params = {
    client_id: config.twitch.clientId,
    client_secret: config.twitch.secret,
    code,
    grant_type: 'authorization_code',
    redirect_uri: config.twitch.callbackUrl,
  }

  Object.entries(params).forEach(([key, value]) => {
    url.searchParams.append(key, value)
  })

  return url
}

/**
 * Makes response to Twitch API and returns fetched token.
 * @param {{ code: string }} values
 * @returns {Promise<object>} Twitch API response data
 * @throws {TwitchAPIRequestError} when request to Twtich API returns an error.
 */
async function requestTwitchToken({ code }) {
  const url = formTwitchTokenAccessUrl({ code }).href
  try {
    const twitchResponse = await axios.post(url)
    return twitchResponse.data
  } catch (err) {
    console.log(err)
    throw new TwitchAPIRequestError('Twitch error when requestes token')
  }
}

/**
 * Makes response to Twitch API and returns fetched user data.
 * @param {{ token: string }} values
 * @returns {Promise<object>} object with user data.
 * @throws {TwitchAPIRequestError} when request to Twtich API returns an error.
 */
async function requestTwitchUserData({ token }) {
  const url = `https://api.twitch.tv/helix/users?timestamp=${Date.now()}`

  const options = {
    headers: {
      'Client-ID': config.twitch.clientId,
      Authorization: `Bearer ${token}`,
    },
  }

  try {
    const twitchResponse = await axios.get(url, options)
    return twitchResponse.data
  } catch (err) {
    console.log(err)
    throw new TwitchAPIRequestError('Twitch error when requested data')
  }
}

/**
 * Handles Twitch API auth callback.
 * @param {{ code: string }} values
 * @returns {Promise<{ user: User, isCreated: boolean}>} Twitch callback code.
 * @throws {TwitchAPIRequestError} when request to Twtich API returns an error.
 */
async function handleTwitchCallback({ code }) {
  const { access_token: token } = await requestTwitchToken({ code })

  const twitchUserResponse = await requestTwitchUserData({ token })

  const twitchUserData = twitchUserResponse.data[0]

  const [user, isCreated] = await User.findOrCreate({
    where: {
      twitchId: twitchUserData.id,
      login: twitchUserData.login,
    },
    defaults: {
      twitchId: twitchUserData.id,
      email: twitchUserData.email,
      login: twitchUserData.login,
      displayName: twitchUserData.display_name,
      profileImageUrl: twitchUserData.profile_image_url,
    },
  })

  return { user, isCreated }
}

/**
 * Returns JWT token for authorization.
 * @param {{ user: User }} values
 * @returns {string} JWT token.
 */
function signAuthJWT({ user }) {
  const { id, login, email } = user

  return jsonwebtoken.sign(
    {
      id,
      login,
      email,
    },
    config.secret,
    { expiresIn: '6h' },
  )
}

module.exports = {
  formTwitchAuthUrl,
  formTwitchTokenAccessUrl,
  requestTwitchToken,
  requestTwitchUserData,
  handleTwitchCallback,
  signAuthJWT,
}
