const { Challenge, UnitpayPayment } = require('../data')
const { NotFoundError } = require('../utils/errors')
const { requestSynthesizeSpeach } = require('../libs/polly')

/**
 * Find challenge instance.
 * @param {{ challengeId: number, userId?: number }} values
 * @returns {Promise<Challenge>}
 * @throws {NotFoundError} when challenge instance not found.
 */
async function findChallenge({ challengeId, userId }) {
  const whereUserOptions = userId ? { userId } : {}

  const challenge = await Challenge.findByPk(challengeId, {
    where: { ...whereUserOptions },
    include: {
      model: UnitpayPayment,
      as: 'Payment',
    }
  })

  if (!challenge) {
    throw new NotFoundError(
      `Challenge not found challengeId=${challengeId}, userId=${userId}`,
    )
  }

  return challenge
}

/**
 * Find challenge list for user.
 * @param {{ userId: number, offset?: number, limit?: number }} values
 * @returns {Promise<Challenge[]>} list of challenge instances.
 */
function findChallengeList({ userId, offset, limit }) {
  return Challenge.findAll({
    where: {
      performerId: userId,
      isHidden: 0,
    },
    include: {
      model: UnitpayPayment,
      as: 'Payment',
    },
    limit: limit ? parseInt(limit) : null,
    offset: offset ? parseInt(offset) : null,
    order: [['createdAt', 'DESC']],
  })
}

/**
 * Find count of challenges for user.
 * @param {{ userId: number }} values
 * @returns {Promise<number>} count of unhidden challenges.
 */
function findChallengeCount({ userId }) {
  return Challenge.count({ where: { performerId: userId, isHidden: 0 } })
}

/**
 * Assigns new challenge for user.
 * @param {{ userId: number nickname: string, description: srting }} values
 * @param {{ isTest: boolean }} options
 * @returns {Promise<Challenge>}
 */
function assignChallenge(
  { userId, nickname, description },
  { isTest = false } = {},
) {
  const hidden = isTest ? { isHidden: 0 } : {}
  return Challenge.create({
    performerId: userId,
    description,
    creatorName: nickname,
    isTest,
    ...hidden,
  })
}

/**
 * Update challenge status.
 * @param {{ challenge: Challenge, newStatus: string }} values
 * @returns {Promise<Challenge>}
 */
function updateChallengeStatus({ challenge, newStatus }) {
  return challenge.updateStatus({ newStatus })
}

/**
 * View challenge for performer
 * @param {{ challenge: Challenge }} values
 * @returns {Promise<Challenge>}
 */
function viewChallenge({ challenge }) {
  return challenge.view()
}

/**
 * Hide challenge for performer
 * @param {{ challenge: Challenge }} values
 * @returns {Promise<Challenge>}
 */
function hideChallenge({ challenge }) {
  return challenge.hide()
}

/**
 * Returns url to mp3 with speach
 * @param {{ challenge: Challenge }} values
 * @returns {Promise<string>} url to the speach mp3 file
 * @throws {TypeError}
 * @throws {Error}
 */
async function getChallengeSpeach({ challenge }) {
  // TODO: Add speach caching
  const { description: text } = challenge
  const url = await requestSynthesizeSpeach({ text })
  return url
}

/**
 * Forms JSON object from challenge instance. Ignores private fields.
 * @param {{ challenge: Challenge }} values
 * @returns {object} serialized challenge instance.
 */
function serializeChallenge({ challenge }) {
  return { 
    id: challenge.id,
    creatorName: challenge?.creatorName,
    description: challenge?.description,
    performerId: challenge?.performerId,
    status: challenge?.status,
    sum: challenge?.Payment?.sum,
    currency: challenge?.Payment?.currency,
    createdAt: challenge.createdAt,
  }
}

/**
 * Forms JSON objects from list of challenge instances.
 * @param {{ challengeList: Challenge[] }} values
 * @returns {object} serialized list of challenge instances.
 */
function serializeChallengeList({ challengeList }) {
  return challengeList.map(challenge => serializeChallenge({ challenge }))
}

async function viewChallenge({ challengeId }) {
  const challenge = await findChallenge({ challengeId });
  await challenge.view();
}

module.exports = {
  findChallenge,
  findChallengeList,
  findChallengeCount,

  assignChallenge,
  updateChallengeStatus,
  viewChallenge,
  hideChallenge,

  getChallengeSpeach,

  serializeChallenge,
  serializeChallengeList,
}
