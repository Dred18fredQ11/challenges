const { User } = require('../data')
const { NotFoundError } = require('../utils/errors')

/**
 * Find user object with specified `userId` or `login`
 * @param {{ userId: number, login?: string } | { login: string}} values
 * @returns {Promise<User>} user object
 * @throws {TypeError}
 * @throws {NotFoundError}
 */
async function findUser({ userId, login }) {
  let user

  if (userId) {
    const whereLoginOptions = login ? { login } : {}
    user = await User.findByPk(userId, { where: { ...whereLoginOptions } })
  } else if (login) {
    user = await User.findOne({ where: { login } })
  } else {
    throw new TypeError('There are no specified params to search user')
  }

  if (!user) {
    throw new NotFoundError(
      `User not found:` +
        (userId ? ` userId = ${userid}` : '') +
        (login ? ` login = ${login}` : ''),
    )
  }

  return user
}

/**
 * Forms from user instance data JSON object.
 * If options contains `isFull = true`, than will include balance data.
 * @param {{ user: User }} values
 * @param {{ isFull: boolean }=} options
 * @returns {object} JSON data of user instance.
 */
function serializeUser({ user }, { isFull = false } = {}) {
  const personalInfo = isFull
    ? {
        balance: user.balance,
        minRubSum: user.minRubSum,
        minUsdSum: user.minUsdSum,
        minUahSum: user.minUahSum,
        chatbotIsActive: user.chatbotIsActive,
        chatbotFrequency: user.chatbotFrequency,
      }
    : {}

  return {
    id: user.id,
    twitchId: user.twitchId,
    email: user.email,
    login: user.login,
    displayName: user.displayName,
    profileImageUrl: user.profileImageUrl,
    ...personalInfo,
  }
}

module.exports = {
  findUser,
  serializeUser,
}
