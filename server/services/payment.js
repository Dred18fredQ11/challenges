const axios = require('axios')
const CryptoJS = require('crypto-js')
const { nanoid } = require('nanoid')
const config = require('../config')
const { User, Challenge, UnitpayPayment } = require('../data')
const { NotFoundError } = require('../utils/errors')

/** 
 * Find payment instance.
 * @param {{ orderId: string }} values
 * @returns {Promise<FondyPayment>} payment instance
 * @throws {NotFoundError} when payment not found
 */
exports.findPayment = async function findPayment({ orderId }) {
  const payment = await FondyPayment.findOne({ where: { orderId }})
  if (!payment) {
    throw new NotFoundError('Payment not found')
  }
  return payment
}

exports.findPayment2 = async function findPayment2({ challengeId }) {
  const payment = await UnitpayPayment.findOne({ where: { challengeId } })
  if (!payment) {
    throw new NotFoundError(`Payment not found: ${challengeId}`)
  }
  return payment
}

// /**
//  * Find payment by `challengeId`.
//  * TODO: add another options to find payment
//  * @param {object} values
//  * @param {number} values.challengeId
//  */
// exports.findPayment = async function findPayment({ challengeId }) {
//   const payment = await UnitpayPayment.findOne({ where: { challengeId } })
//   if (!payment) {
//     throw new NotFoundError(`Payment not found: ${challengeId}`)
//   }
//   return payment
// }

/**
 * Find payment target instance.
 * @param {{ orderId: number }} values
 * @returns {Promise<User>} user instance
 * @throws {NotFoundError} when user instance not found
 */
exports.findPaymentTarget = async function findPaymentTarget({ orderId }) {
  const user = await User.findOne({
    include: {
      model: Challenge,
      as: 'Challenge',
      attributes: [],
      include: {
        model: UnitpayPayment,
        as: 'Payment',
        where: { orderId },
      },
    }
  })
  if (!user) {
    throw new NotFoundError(`Payment target not found ${orderId}`)
  }
  return user
}

/** Put payment profit on user's balance */
exports.putOnUserBalance = function putOnUserBalance({ user, payment }) {
  const sum = payment?.sum
  if (!sum) {
    throw new TypeError('Payment not finished')
  }
  return user.putBalance({ sum })
}

/**
 * Forms signature for Fondy Payment System.
 * @param {{
 *  amount: number;
 *  currency: string;
 *  orderId: string;
 *  orderDesc: string;
 *  responseUrl: string;
 *  serverCallbackUrl: string;
 * }} params
 * @returns {string} SHA1 crypted signature
 */
exports.formSignature = function formSignature(params) {
  const list = Object.entries(params)
    .sort((pair0, pair1) => {
      if (pair0[0] < pair1[0]) return -1
      if (pair0[0] > pair1[0]) return 1
      return 0
    })
    .map(([_, value]) => value)
  const message = [config.fondy.paymentKey, ...list].join('|')
  return CryptoJS.enc.Hex.stringify(CryptoJS.SHA1(message))
}

/**
 * 
 * @param {{
 *  amount: number;
 *  currency: string;
 * }} values 
 */
exports.initPayment = async function initPayment({ currency, amount }) {
  const orderId = nanoid()
  const orderDesc = 'Челлендж для стримера'
  const signature = exports.formSignature({
    amount,
    currency,
    merchantId: config.fondy.merchantId,
    orderId,
    orderDesc,
    response_url: 'https://dropchallenge.com/payment-success',
    server_callback_url: 'https://dropchallenge.com/api/payment/handler'
  })

  const payment = await FondyPayment.create({
    orderId,
    amount,
    currency,
  })

  try {
    const headers = { headers: { 'Content-Type': 'application/json' } }
    const body = { request: {
      'order_id': orderId,
      'order_desc': orderDesc,
      currency,
      amount,
      signature,
      response_url: 'https://dropchallenge.com/payment-success',
      server_callback_url: 'https://dropchallenge.com/api/payment/handler',
      'merchant_id': config.fondy.merchantId,
    }}
    const res = await axios.post(
      'https://pay.fondy.eu/api/checkout/url/',
      body,
      headers,
    )
    console.log('initPayment', res.data)
    return { payment, redirectUrl: res.data.response.checkout_url }
  } catch (err) {
    console.log('initPayment', err.data)
    throw new Error('INIT PAYMENT ERROR')
  }
}

/**
 * 
 * @param {{
 *  payment: FondyPayment;
 *  orderStatus: string;
 * }} values
 */
exports.updatePayment = function updatePayment({ payment, orderStatus}) {
  payment.status = orderStatus
  return payment.save()
}


























// /**
//  * Forms MD5 Hash for Free-Kassa
//  */
// exports.formMD5PaymentHash = function formMD5PaymentHash({ sum, orderId }) {
//   //"ID Вашего магазина:Сумма платежа:Секретное слово:Номер заказа",
//   const message = `${config.freeKassa.merchantId}:${sum}:`
//     + `${config.freeKassa.secret1}:${orderId}`
//   return MD5(message)
// }

// /**
//  * Forms URL for Free-Kassa and inits payment
//  */
// exports.initPayment = async function formPaymentUrl({
//   sum,
//   currency,
//   challengeId,
// }) {
//   const orderId = nanoid()
  
//   const paramsMap = {
//     m: config.freeKassa.merchantId,
//     oa: sum,
//     o: orderId,
//     s: exports.formMD5PaymentHash({ sum, orderId }),
//     i: currency,
//     lang: 'ru',
//     us_orderId: orderId,
//     type: 'json',
//   }

//   const params = Object.entries(paramsMap).map(([k, v]) => {
//     return `${encodeURIComponent(k)}=${encodeURIComponent(v)}`
//   }).join("&")

//   const url = `https://www.free-kassa.ru/merchant/cash.php?${params}`

//   const payment = await FreeKassaPayment.create({
//     sum,
//     orderId,
//     currency,
//     challengeId,
//   })

//   return { payment, url }
// }

// /**
//  * Handles Free Kassa payment response data 
//  */
// exports.handlePaymentNotification = async function handlePaymentNotification({
//   merchantId,
//   paidSum,
//   freeKassaId,
//   paidCurrency,
//   orderId,
// }) {
//   const payment = await exports.findPayment({ orderId })
//   await payment.setFreeKassaData({ freeKassaId, paidSum, paidCurrency })
//   return payment.setCompleteStatus()
// }

/**
 * Forms signature for payment.
 * @param {object} values
 * @param {string} values.account
 * @param {string} values.currency
 * @param {string} values.desc
 * @param {number} values.sum
 */
exports.formPaymentSignature = function formPaymentSignature({
  account,
  currency,
  desc,
  sum,
}) {
  const params = [account, currency, desc, sum, config.unitpay.secretKey]
  const message = params.join('{up}')
  const signature = CryptoJS.SHA256(message)
  return signature
}

/**
 * Forms url for payment request.
 * @param {object} values
 * @param {string} values.account
 * @param {string} values.currency
 * @param {string} values.desc
 * @param {number} values.sum
 * @param {string} values.clientIp
 * @param {object} options
 * @param {boolean} options.isTest
 */
exports.formPaymentRequestUrl = function formPaymentRequestUrl(
  { account, currency, desc, sum, clientIp },
  options = {},
) {
  const { isTest } = { isTest: false, ...options }

  const secretKey = isTest
    ? config.unitpay.apiSecretKeyTest
    : config.unitpay.secretKey

  const signature = exports.formPaymentSignature({
    account,
    currency,
    desc,
    sum,
  })

  const protocol = `${config.isSsl ? 'https' : 'http'}://`
  const resultUrl = `${protocol}${config.homeUrl}/payment-result`

  const url = new URL(
    'https://unitpay.money/api?' +
      'method=initPayment' +
      '&params[paymentType]=card' +
      `&params[account]=${account}` +
      `&params[sum]=${sum}` +
      `&params[currency]=${currency}` +
      `&params[projectId]=${config.unitpay.projectId}` +
      `&params[resultUrl]=${resultUrl}` +
      `&params[desc]=${desc}` +
      `&params[ip]=${clientIp}` +
      `&params[secretKey]=${secretKey}` +
      `&params[signature]=${signature}` +
      (isTest ? '&params[test]=1' : ''),
  ).href

  return url
}

exports.formPaymentRefundUrl = function formPaymentRefundUrl({
  sum,
  unitpayId,
}) {
  const url = new URL(
    'https://unitpay.money/api?' +
    'method=refundPayment' +
    `&params[paymentId]=${unitpayId}` +
    `&params[secretKey]=${config.unitpay.secretKey}`,
  ).href
  return url
}

exports.formPayWithdrawUrl = function formPayWithdrawUrl({
  sum,
  purse,
  login,
}) {
  const url = new URL('https://unitpay.money/api?'
    + 'method=massPayment'
    + `params[sum]=${sum}`
    + `params[purse]=${purse}`
    + `params[login]=${login}`
    + `&params[transactionId]=${transactionId}`
    + `&params[secretKey]=${config.unitpay.secretKey}`
    + `&params[paymentType]=card`
  ).href
  return url
}

/**
 * Init Unitpay payment instance in db.
 * Step 1 of payment chain.
 * @param {object} values
 * @param {string} values.account
 * @param {number} values.sum
 * @param {number} values.challengeId
 * @param {string} values.description
 * @param {object} options
 * @param {boolean} options.isTest
 */
exports.initUnitpayPayment = function initUnitpayPayment(
  { account, sum, currency, challengeId, description },
  options = {},
) {
  const { isTest } = options
  return UnitpayPayment.create({
    status: 0,
    account,
    description,
    sum,
    currency,
    dateCreate: new Date(),
    challengeId,
    isTest,
  })
}

/**
 * Make Unitpay payment request.
 * Step 2 of payment chain.
 * Inits handler request from Unitpay with `method=check`.
 * @param {object} values
 * @param {UnitpayPayment} values.payment
 * @param {string} values.clientIp
 * @param {object} options
 * @param {boolean} options.isTest
 */
exports.makeUnitpayPaymentRequest = async function makeUnitpayPaymentRequest(
  { payment, clientIp },
  options = {},
) {
  const { isTest } = { isTest: false, ...options }
  const { account, sum, currency, description: desc } = payment

  const url = exports.formPaymentRequestUrl(
    { account, sum, currency, desc, clientIp },
    { isTest },
  )
  const res = await axios.get(url)

  if (res?.data?.error) {
    const error = res?.data?.error
    const message = `code: ${error?.code} - ${error?.message}`
    throw new Error(`Ошибка платежа: ${message}`)
  }

  const result = res?.data?.result
  const unitpayId = result?.paymentId
  await payment.setUnitpayId({ unitpayId })
  return { payment, resData: result }
}

/**
 * Check Unitpay payment data. Step 3 of payment chain.
 * Occurs after request in step 2 but before response to that request.
 * @param {object} values - Unitpay request params
 */
exports.checkUnitpayPayment = async function checkUnitpayPayment({
  account,
  payerSum,
  profit,
  date,
  operator,
  paymentType,
  projectId,
  phone,
  payerCurrency,
  signature,
  orderSum,
  orderCurrency,
  unitpayId,
  test,
}) {
  const payment = await UnitpayPayment.findOne({ where: { account } })
  if (!payment) {
    throw new NotFoundError('Such payment not found at checking')
  }
  if (parseFloat(payment.sum) !== parseFloat(orderSum)) {
    throw new PaymentSumNotMatchError('Order sum and payment sum not match')
  }
  await payment.setPayer({ payerSum, payerCurrency })
  return payment.setCheckedStatus()
}

/**
 * Take Unitpay payment.
 * @param {object} values - Unitpay request params
 */
exports.paidUnitpayPayment = async function paidUnitpayPayment({
  account,
  payerSum,
  profit,
  date,
  operator,
  paymentType,
  projectId,
  phone,
  payerCurrency,
  signature,
  orderSum,
  orderCurrency,
  unitpayId,
  test,
}) {
  const payment = await UnitpayPayment.findOne({ where: { account } })
  if (!payment) {
    throw new NotFoundError('Such payment not found at pay')
  }
  if (parseFloat(payment.sum) !== parseFloat(orderSum)) {
    throw new PaymentSumNotMatchError('Order sum and payment sum not match')
  }
  await payment.setProfit({ profit })
  return payment.setCompleteStatus()
}

// /**
//  * Refund payment
//  * @param {object} values
//  * @param {number} values.sum
//  * @param {string} values.unitpayId
//  */
// exports.refundUnitpayPayment = async function refundUnitpayPayment({
//   sum,
//   unitpayId,
// }) {
//   const url = exports.formPaymentRefundUrl({ sum, unitpayId })
//   const res = await axios.get(url)

//   console.log('REFUND', res)

//   if (res?.data?.error) {
//     const error = res?.data?.error
//     const message = `code: ${error?.code} - ${error?.message}`
//     throw new Error(`Ошибка возрата платежа: ${message}`)
//   }

//   const payment = await UnitpayPayment.findOne({ where: { unitpayId }})
//   if (payment) {
//     payment.isRefund = 1
//     await payment.save()
//   }

//   return { payment, resData: res?.data?.result }
// }

// /**
//  * Find target of payment
//  * @param {object} values
//  * @param {string} values.account
//  */
// exports.findPaymentTarget = async function findPaymentTarget({ account }) {
//   const user = await User.findOne({
//     include: {
//       model: Challenge,
//       as: 'Challenge',
//       attributes: [],
//       include: {
//         model: UnitpayPayment,
//         as: 'Payment',
//         where: { account },
//       },
//     },
//   })
//   if (!user) {
//     throw new NotFoundError('User not found')
//   }
//   return user
// }

// /**
//  * Find payment by `challengeId`.
//  * TODO: add another options to find payment
//  * @param {object} values
//  * @param {number} values.challengeId
//  */
// exports.findPayment = async function findPayment({ challengeId }) {
//   const payment = await UnitpayPayment.findOne({ where: { challengeId } })
//   if (!payment) {
//     throw new NotFoundError(`Payment not found: ${challengeId}`)
//   }
//   return payment
// }

/** Put payment profit on user's balance */
exports.putOnUserBalance = function putOnUserBalance({ user, payment }) {
  const sum = payment?.profit
  if (!sum) {
    throw new TypeError('Payment not finished')
  }
  return user.putBalance({ sum })
}

/**
 * Withdraw user balance
 * @param {object} values
 * @param {User} values.user
 * @param {number} values.sum
 */
exports.withdrawUserBalance = async function withdrawUserBalance({ user, sum }) {
  const url = exports.formPaymentRefundUrl({ sum, unitpayId })
  const res = await axios.get(url)

  console.log('REFUND', res)

  if (res?.data?.error) {
    const error = res?.data?.error
    const message = `code: ${error?.code} - ${error?.message}`
    throw new Error(`Ошибка возрата платежа: ${message}`)
  }

  const payment = await UnitpayPayment.findOne({ where: { unitpayId }})
  if (payment) {
    payment.isRefund = 1
    await payment.save()
  }

  return { payment, resData: res?.data?.result }
}
