const {
  WidgetApp,
  Widget,
  WidgetChallenge,
  WidgetElement,
  WidgetElementText,
  WidgetElementSpeach,
} = require('../data')
const { NotFoundError } = require('../utils/errors')

/**
 * Find one widget app instance by its ID.
 * User ID can be specified for security.
 * @param {{ widgetAppId: number, userId?: number }
 *  | { token: string, userId?: number }
 * } values
 * @returns {Promise<WidgetApp>} widget app instance.
 * @throws {NotFoundError}
 */
async function findWidgetApp({ widgetAppId, token, userId }) {
  const whereUserId = userId ? { userId } : {}

  let widgetApp
  if (widgetAppId) {
    widgetApp = await WidgetApp.findByPk(widgetAppId, whereUserId)
  } else if (token) {
    widgetApp = await WidgetApp.findOne({ where: { ...whereUserId, token } })
  }

  if (!widgetApp) {
    throw new NotFoundError(
      'Widget App not found:' +
        ` widgetAppId = ${widgetAppId}` +
        (userId ? `, userId = ${userId}` : ''),
    )
  }

  return widgetApp
}

/**
 * Find token of widget app by user ID and widget app name.
 * @param {{ userId: number, name?: string }} values
 * @returns {Promise<string>} token of widget app.
 * @throws {NotFoundError}
 */
async function findWidgetAppToken({ userId, name = 'notification' }) {
  const widgetApp = await WidgetApp.findOne({
    where: { userId, name },
    attributes: ['token'],
  })

  if (!widgetApp) {
    throw new NotFoundError('WidgetApp not found')
  }

  return widgetApp.token
}

/**
 * Find widget app instances by one or multiple user IDs.
 * @param {{ userId: number | number[] }} values
 * @returns {Promise<WidgetApp[]>} list of widget app instances.
 */
function findWidgetAppList({ userId }) {
  return WidgetApp.findAll({ where: { userId } })
}

/**
 * Form widget app JSON response.
 * @param {{ widgetApp: WidgetApp }} values
 * @returns {{
 *  id: number,
 *  name: string,
 *  token: string,
 *  type: string,
 *  userId: number,
 * }} JSON response.
 */
function serializeWidgetApp({ widgetApp }) {
  return {
    id: widgetApp.id,
    name: widgetApp.name,
    token: widgetApp.token,
    type: widgetApp.type,
    userId: widgetApp.userId,
  }
}

/**
 * Form widget app list JSON response.
 * @param {{ widgetAppList: WidgetApp[] }} values
 * @returns {{
 *  id: number,
 *  name: string,
 *  token: string,
 *  type: string,
 *  userId: number,
 * }[]} JSON response.
 */
function serializeWidgetAppList({ widgetAppList }) {
  return widgetAppList.map(widgetApp => serializeWidgetApp({ widgetApp }))
}

/**
 * Find one widget instance by its ID.
 * Widget app ID and user ID can be specified for security.
 * @param {{ widgetId: number, widgetAppId?: number, userId?: number }} values
 * @returns {Promise<Widget>} widget instance.
 * @throws {NotFoundError}
 */
async function findWidget({ widgetId, widgetAppId, userId }) {
  const whereWidgetAppId = widgetAppId ? { widgetAppId } : {}
  const includeWhereUserId = userId
    ? [
        {
          model: WidgetApp,
          as: 'WidgetApp',
          attributes: [],
          where: { userId },
        },
      ]
    : []

  const widget = await Widget.findByPk(widgetId, {
    where: { ...whereWidgetAppId },
    include: [
      {
        model: WidgetChallenge,
        as: 'WidgetChallenge',
        attributes: ['id', 'challengeId'],
      },
      ...includeWhereUserId,
    ],
  })

  if (!widget) {
    throw new NotFoundError(
      'Widget not found:' +
        ` widgetId = ${widgetId}` +
        (widgetAppId ? `, wigetAppId = ${widgetAppId}` : '') +
        (userId ? `, userId = ${userId}` : ''),
    )
  }

  return widget
}

/**
 * Find widget instances by one or multiple widget app IDs.
 * User ID can be specified for security.
 * If specified only user ID find all widget instances that belongs to this
 * user IDs.
 * @param {{ widgetAppId: number | number[], userId?: number | number[] }
 *  | { userId: number | number[] }} values
 * @returns {Promise<Widget[]>} list of widget instances.
 */
async function findWidgetList({ widgetAppId, userId }) {
  if (!widgetAppId && !userId) {
    throw new TypeError('Wrong search params')
  }

  const whereWidgetAppId = widgetAppId ? { widgetAppId } : {}
  const includeWhereUserId = userId
    ? [
        {
          model: WidgetApp,
          as: 'WidgetApp',
          attributes: [],
          where: { userId },
        },
      ]
    : []

  const widgetList = await Widget.findAll({
    where: { ...whereWidgetAppId },
    include: [
      {
        model: WidgetChallenge,
        as: 'WidgetChallenge',
        attributes: ['id', 'challengeId'],
      },
      ...includeWhereUserId,
    ],
  })

  return widgetList
}

/**
 * Set the challenge on the widget.
 * @param {{ widget: Widget, challengeId: number }} values
 * @returns {Promise<Widget>} instance of widget with set challenge.
 */
async function setWidgetChallenge({ widget, challengeId }) {
  widget.WidgetChallenge.challengeId = challengeId
  await widget.WidgetChallenge.save()
  return widget
}

/**
 * Remove the challenge from the widget.
 * @param {{ widget: Widget }} values
 * @returns {Promise<Widget>} instance of widget without challenge.
 */
async function removeWidgetChallenge({ widget }) {
  widget.WidgetChallenge.challengeId = null
  await widget.WidgetChallenge.save()
  return widget
}

/**
 * Form widget json response.
 * @param {{ widget: Widget }} values
 * @returns {{
 *  id: number,
 *  name: string,
 *  displayName: string,
 *  type: string,
 *  widgetAppId: number,
 *  challengeId?: number,
 * }} JSON response.
 */
function serializeWidget({ widget }) {
  return {
    id: widget.id,
    name: widget.name,
    displayName: widget.displayName,
    type: widget.type,
    widgetAppId: widget.widgetAppId,
    challengeId: widget?.WidgetChallenge?.challengeId,
  }
}

/**
 * Form widget app list JSON response.
 * @param {{ widgetList: Widget[] }} values
 * @returns {{
 *  id: number,
 *  name: string,
 *  displayName: string,
 *  type: string,
 *  widgetAppId: number,
 *  challengeId: number,
 * }[]} JSON response.
 */
function serializeWidgetList({ widgetList }) {
  return widgetList.map(widget => serializeWidget({ widget }))
}

/**
 * Find one widget element instance by its ID.
 * Widget ID, widget app Id and user ID can be specified for security.
 * @param {{
 *  elementId: number,
 *  widgetId?: number,
 *  widgetAppId?: number,
 *  userId?: number,
 * }} values
 * @returns {Promise<WidgetElement>} widget element instance.
 * @throws {NotFoundError}
 */
async function findWidgetElement({ elementId, widgetId, widgetAppId, userId }) {
  const whereWidgetId = widgetId ? { widgetId } : {}
  const includeWhereUserId = userId
    ? [
        {
          model: WidgetApp,
          as: 'WidgetApp',
          attributes: [],
          where: { userId },
        },
      ]
    : []
  const includeWhereWidgetAppId =
    widgetAppId || userId
      ? [
          {
            model: Widget,
            as: 'Widget',
            attributes: [],
            where: widgetAppId ? { widgetAppId } : {},
            include: [...includeWhereUserId],
          },
        ]
      : []

  const element = await WidgetElement.findByPk(elementId, {
    where: { ...whereWidgetId },
    include: [
      {
        model: WidgetElementText,
        as: 'WidgetElementText',
        attributes: [
          'id',
          'fontFamily',
          'color',
          'fontSize',
          'fontWeight',
          'fontStyle',
          'textShadow',
        ],
      },
      {
        model: WidgetElementSpeach,
        as: 'WidgetElementSpeach',
        attributes: ['id', 'voiceId'],
      },
      ...includeWhereWidgetAppId,
    ],
  })

  if (!element) {
    throw new NotFoundError(
      'Widget element not found:' +
        ` elementId = ${elementId}` +
        (widgetId ? `, widgetId = ${widgetId}` : '') +
        (widgetAppId ? `, widgetApPId = ${widgetAppId}` : '') +
        (userId ? `, userId = ${userId}` : ''),
    )
  }

  return element
}

/**
 * Find widget element instances by one or multiple widget IDs.
 * Widget app ID and user ID can be specified for security.
 * If specified only user ID find all widget instances that belongs to this
 * user IDs.
 * @param {{
 *  widgetId: number | number[],
 *  widgetAppId?: number,
 *  userId?: number
 * } | {
 *  userId: number | number[],
 * }} values
 * @returns {Promise<WidgetElementList>} list of widget element instances.
 */
function findWidgetElementList({ widgetId, widgetAppId, userId }) {
  if (!widgetId && !userId) {
    throw new TypeError('Wrong search params')
  }

  const whereWidgetId = widgetId ? { widgetId } : {}
  const includeWhereUserId = userId
    ? [
        {
          model: WidgetApp,
          as: 'WidgetApp',
          attributes: [],
          where: { userId },
        },
      ]
    : []
  const includeWhereWidgetAppId =
    widgetAppId || userId
      ? [
          {
            model: Widget,
            as: 'Widget',
            attributes: [],
            where: widgetAppId ? { widgetAppId } : {},
            include: [...includeWhereUserId],
          },
        ]
      : []

  return WidgetElement.findAll({
    where: { ...whereWidgetId },
    include: [
      {
        model: WidgetElementText,
        as: 'WidgetElementText',
        attributes: [
          'id',
          'fontFamily',
          'color',
          'fontSize',
          'fontWeight',
          'fontStyle',
          'textShadow',
        ],
      },
      {
        model: WidgetElementSpeach,
        as: 'WidgetElementSpeach',
        attributes: ['id', 'voiceId'],
      },
      ...includeWhereWidgetAppId,
    ],
  })
}

/**
 * Update fields of widget element instance.
 * @param {{ element: WidgetElement; elementProps: Object; }} values
 * @returns {Promise<WidgetElement>} updated widget element instance.
 * @throws {TypeError} when type of element in unknown.
 */
async function updateWidgetElement({ element, elementProps }) {
  switch (element.type) {
    case 'text': {
      // TODO: Add fields validation
      Object.entries(elementProps).forEach(([key, value]) => {
        if (key === 'id' || key === 'widgetId') return;
        element.WidgetElementText[key] = value
      })
      await element.WidgetElementText.save()
      return element
    }
    case 'speach': {
      throw new Error('Not implemented update of speach element')
    }
    default: {
      throw new TypeError(`Wrong widget element type ${element.type}`)
    }
  }
}

/**
 * Form widget element JSON response.
 * @param {{ element: WidgetElement }} values
 * @returns JSON response.
 */
function serializeWidgetElement({ element }) {
  return {
    id: element.id,
    name: element.name,
    displayName: element.displayName,
    type: element.type,
    widgetId: element.widgetId,
    fontFamily: element?.WidgetElementText?.fontFamily,
    color: element?.WidgetElementText?.color,
    fontSize: element?.WidgetElementText?.fontSize,
    fontWeight: element?.WidgetElementText?.fontWeight,
    fontStyle: element?.WidgetElementText?.fontStyle,
    textShadow: element?.WidgetElementText?.textShadow,
    voiceId: element?.widgetElementSpeach?.voiceId,
  }
}

/**
 * Forms widget element list JSON response.
 * @param {{ elementList: WidgetElement[] }} values
 * @returns JSON response list
 */
function serializeWidgetElementList({ elementList }) {
  return elementList.map(element => serializeWidgetElement({ element }))
}

module.exports = {
  findWidgetApp,
  findWidgetAppToken,
  findWidgetAppList,
  serializeWidgetApp,
  serializeWidgetAppList,

  findWidget,
  findWidgetList,
  setWidgetChallenge,
  removeWidgetChallenge,
  serializeWidget,
  serializeWidgetList,

  findWidgetElement,
  findWidgetElementList,
  updateWidgetElement,
  serializeWidgetElement,
  serializeWidgetElementList,
}
