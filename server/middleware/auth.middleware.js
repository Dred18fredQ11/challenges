const jwt = require('jsonwebtoken')
const config = require('../config')
const { User } = require('../data')

module.exports = async (req, res, next) => {
  if (req.method === 'OPTIONS') {
    return next()
  }

  try {
    const token = req.headers.authorization.split(' ')[1]

    if (!token) {
      return res.status(401).json({ msg: 'No auth' })
    }

    const decoded = jwt.verify(token, config.secret)

    const id = decoded?.id
    
    if (!id) {
      return res.status(401).json({ msg: 'No auth' })
    }

    const isExists = await User.isExists({ id })
    if (!isExists) {
      return res.status(401).json({ msg: 'No auth' })
    }

    req.user = { ...decoded }
    next()
  } catch (e) {
    res.clearCookie('jwt')
    res.status(401).json({ message: 'No auth' })
  }
}
