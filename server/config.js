require('dotenv').config()

module.exports = {
  port: process.env.PORT,
  homeUrl: process.env.HOME_URL,
  isSsl: Boolean(parseInt(process.env.IS_SSL)),
  secret: process.env.SESSION_SECRET,
  db: {
    schemaName: process.env.DB_SCHEMA_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
  twitch: {
    clientId: process.env.TWITCH_CLIENT_ID,
    secret: process.env.TWITCH_SECRET,
    callbackUrl: process.env.TWITCH_CALLBACK_URL,
  },
  paypal: {
    clientId: process.env.PAYPAL_CLIENT_ID,
    secret: process.env.PAYPAL_SECRET,
  },
  fondy: {
    merchantId: process.env.FONDY_MERCHANT_ID,
    paymentKey: process.env.FONDY_PAYMENT_KEY,
    creditKey: process.env.FONDY_CREDIT_KEY,
  },
  unitpay: {
    publicKey: process.env.UNITPAY_PUBLIC_KEY,
    secretKey: process.env.UNITPAY_SECRET_KEY,
    apiSecretKey: process.env.UNITPAY_API_SECRET_KEY,
    apiSecretKeyTest: process.env.UNITPAY_API_SECRET_KEY_TEST,
    login: process.env.UNITPAY_LOGIN,
    projectId: process.env.UNITPAY_PROJECT_ID,
  },
  freeKassa: {
    merchantId: process.env.FREEKASSA_MERCHANT_ID,
    secret1: process.env.FREEKASSA_SECRET1,
    secret2: process.env.FREEKASSA_SECRET2,
  },
  aws: {
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  },
}
