const {
  findWidgetApp,
  findWidgetList,
  findWidgetElementList,
  serializeWidgetApp,
  serializeWidgetList,
  serializeWidgetElementList,
} = require('../services/widget.services')
const { findChallenge } = require('../services/challenge.services')

function widgetHandler(io, socket) {
  socket.on('widget:init', async token => {
    try {
      // Find all information about widget infrastructure
      const widgetApp = await findWidgetApp({ token })
      const widgetList = await findWidgetList({ widgetAppId: widgetApp.id })
      const widgetId = widgetList.map(widget => widget.id)
      const elementList = await findWidgetElementList({ widgetId })

      // If widget app is challenge banner than find displayed challenge
      const challengeId = widgetList?.find(widget => {
        return widget.type === 'challengeBanner'
      })?.WidgetChallenge?.challengeId
      const challenge = challengeId
        ? await findChallenge({ challengeId })
        : undefined
      
      // Send data to widget client
      socket.join(`widget:${token}`)
      io.to(`widget:${token}`).emit('widget:init', {
        widgetApp: serializeWidgetApp({ widgetApp }),
        widgetList: serializeWidgetList({ widgetList }),
        elementList: serializeWidgetElementList({ elementList }),
        challenge,
      })
    } catch(err) {
      // TODO: Add error handling on widget side
      console.log(err)
    }
  })
}

module.exports = widgetHandler
