const passport = require('passport')
const config = require('../config')
const { User } = require('../data')
const twitchStrategy = require('passport-twitch').Strategy


// const OAuth2Strategy = require('passport-oauth').OAuth2Strategy
// const request = require('request')


// passport.use(new twitchStrategy(
//   {
//     clientID: config.twitch.clientId,
//     clientSecret: config.twitch.secret,
//     callbackURL: config.twitch.callbackUrl,
//   },
//   async (accessToken, refreshToken, profile, done) => {
//     console.log('twitch', profile)
//     try {
//       const [user, created] = await User.findOrCreate({ 
//         where: {
//           twitchId: profile.id,
//         },
//         default: {
//           twitchId: profile.id,
//           email: profile.id,
//           login: profile.id,
//           displayName,
//           profileImageUrl,
//         },
//       })
//       done(null, { data: user.toJSON(), accessToken, refreshToken })
//     } catch(err) {
//       console.log(err)
//       res.status(400).json({ msg: 'err' })
//     }
//   }
// ))

// passport.serializeUser((user, done) => {
//   done(null, user)
// })

// passport.deserializeUser((user, done) => {
//   done(null, user)
// })




// OAuth2Strategy.prototype.userProfile = (token, done) => {
//   const options = {
//     url: 'https://api.twitch.tv/helix/users',
//     method: 'GET',
//     headers: {
//       'Client-ID': config.twitch.clientId,
//       'Accept': 'application/vnd.twitchtv.v5+json',
//       'Authorization': 'Bearer ' + token
//     }
//   }

//   request(options, (err, res, body) => {
//     if (res?.statusCode != 200) {
//       return done(JSON.parse(body))
//     }

//     done(null, JSON.parse(body))
//   })
// }

// passport.serializeUser((user, done) => {
//   done(null, user)
// })

// passport.deserializeUser((user, done) => {
//   done(null, user)
// })

// passport.use('twitch', new OAuth2Strategy({
//   authorizationURL: 'https://id.twitch.tv/oauth2/authorize',
//   tokenURL: 'https://id.twitch.tv/oauth2/token',
//   clientID: config.twitch.clientId,
//   clientSecret: config.twitch.secret,
//   callbackURL: config.twitch.callbackUrl,
// }, async (accessToken, refreshToken, profile, done) => {
//   profile.accessToken = accessToken
//   profile.refreshToken = refreshToken

//   const {
//     id: twitchId,
//     email,
//     login,
//     display_name: displayName,
//     profile_image_url: profileImageUrl,
//   } = profile.data[0]

//   try {
//     const [user, created] = await User.findOrCreate(
//       {
//         where: {
//           twitchId
//         },
//         defaults: {
//           twitchId,
//           email,
//           login,
//           displayName,
//           profileImageUrl,
//         },
//       }
//     )

//     done(null, { data: user.toJSON(), accessToken, refreshToken })
//   } catch(e) {
//   }
// }))

module.exports = passport
