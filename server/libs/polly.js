const config = require('../config')
const { Polly } = require('@aws-sdk/client-polly')
const { getSynthesizeSpeechUrl } = require('@aws-sdk/polly-request-presigner')

const client = new Polly({
  region: config.aws.region,
  credentials: {
    accessKeyId: config.aws.accessKeyId,
    secretAccessKey: config.aws.secretAccessKey,
  },
})

/**
 * Form s3Params file object
 * @param {{ text: string }} values
 * @returns {object} s3Params
 * @throws {TypeError}
 */
function formS3Params({ text }) {
  if (typeof text !== 'string') {
    throw new TypeError('Wrong type of text variable')
  }

  return {
    OutputFormat: 'mp3',
    OutputS3BucketName: 'bucket-dc-audio',
    Text: text,
    TextType: 'text',
    VoiceId: 'Maxim',
    SampleRate: '22050',
  }
}

/**
 * Requests for synthesize of speach to AWS Polly
 * @param {{ text: string }} values
 * @returns {Promise<string>} url to synthesized speech 
 * @throws {TypeError}
 * @throws {Error}
 */
async function requestSynthesizeSpeach({ text }) {
  // TODO: Add response error checking
  if (typeof text !== 'string') {
    throw new TypeError('Wrong type of text variable')
  }

  const params = formS3Params({ text })
  const url = await getSynthesizeSpeechUrl({ client, params })
  return url
}

module.exports = { requestSynthesizeSpeach }
