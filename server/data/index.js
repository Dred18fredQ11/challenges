const { Sequelize } = require('sequelize')
const config = require('../config')

const sequelize = new Sequelize(
  config.db.schemaName,
  config.db.user,
  config.db.password,
  {
    dialect: 'mysql',
    logging: true,
  },
)

const models = {
  User: require('./models/user.model').init(sequelize),
  Challenge: require('./models/challenge.model').init(sequelize),
  WidgetApp: require('./models/widgetApp.model').init(sequelize),
  Widget: require('./models/widget.model').init(sequelize),
  WidgetChallenge: require('./models/widgetChallenge.model').init(sequelize),
  WidgetElement: require('./models/widgetElement.model').init(sequelize),
  WidgetElementText: require('./models/widgetElementText.model').init(
    sequelize,
  ),
  WidgetElementSpeach: require('./models/widgetElementSpeach.model').init(
    sequelize,
  ),
  // FondyPayment: require('./models/fondyPayment.model').init(sequelize),
  // FreeKassaPayment: require('./models/freeKassaPayment.model').init(sequelize),
  UnitpayPayment: require('./models/unitpayPayment.model').init(sequelize),
}

// Associate models
Object.values(models)
  .filter(model => typeof model.associate === 'function')
  .forEach(model => model.associate(models))

// Init Hooks
Object.values(models)
  .filter(model => typeof model.initHooks === 'function')
  .forEach(model => model.initHooks(models))

module.exports = {
  ...models,
  sequelize,
}
