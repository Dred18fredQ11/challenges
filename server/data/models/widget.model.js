const { Model, DataTypes } = require('sequelize')

class Widget extends Model {
  static init(sequelize) {
    return super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        displayName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        type: {
          type: DataTypes.ENUM('challengeBanner', 'challengeNotification'),
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Widget',
        tableName: 'Widget',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.WidgetApp, {
      foreignKey: {
        name: 'widgetAppId',
        allowNull: false,
      },
      as: 'WidgetApp',
    })

    this.hasMany(models.WidgetElement, {
      foreignKey: {
        name: 'widgetId',
        allowNull: false,
      },
      as: 'WidgetElement',
    })

    this.hasOne(models.WidgetChallenge, {
      foreignKey: {
        name: 'widgetId',
        allowNull: false,
      },
      as: 'WidgetChallenge',
    })
  }

  static initHooks(models) {
    this.addHook('afterCreate', async (instance, options) => {
      switch (instance.type) {
        case 'challengeBanner': {
          await instance.createWidgetChallenge(
            {},
            {
              transaction: options.transaction,
            },
          )

          await instance.createWidgetElement(
            {
              name: 'title',
              displayName: 'Title',
              type: 'text',
            },
            {
              transaction: options.transaction,
            },
          )

          await instance.createWidgetElement(
            {
              name: 'text',
              displayName: 'Text',
              type: 'text',
            },
            {
              transaction: options.transaction,
            },
          )

          return
        }

        case 'challengeNotification': {
          await instance.createWidgetElement(
            {
              name: 'title',
              displayName: 'Title',
              type: 'text',
            },
            {
              transaction: options.transaction,
            },
          )

          await instance.createWidgetElement(
            {
              name: 'text',
              displayName: 'Text',
              type: 'text',
            },
            {
              transaction: options.transaction,
            },
          )

          await instance.createWidgetElement(
            {
              name: 'speach',
              displayName: 'Speach',
              type: 'speach',
            },
            {
              transaction: options.transaction,
            },
          )
        }

        default:
          return
      }
    })
  }
}

module.exports = Widget
