const { Model, DataTypes } = require('sequelize')

class WidgetElement extends Model {
  static init(sequelize) {
    return super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        displayName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        type: {
          type: DataTypes.ENUM('text', 'speach'),
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'WidgetElement',
        tableName: 'WidgetElement',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.Widget, {
      foreignKey: {
        name: 'widgetId',
        allowNull: false,
      },
      as: 'Widget',
    })

    this.hasOne(models.WidgetElementText, {
      foreignKey: {
        name: 'widgetElementId',
        allowNull: false,
      },
      as: 'WidgetElementText',
    })

    this.hasOne(models.WidgetElementSpeach, {
      foreignKey: {
        name: 'widgetElementId',
        allowNull: false,
      },
      as: 'WidgetElementSpeach',
    })
  }

  static initHooks(models) {
    this.addHook('afterCreate', async (instance, options) => {
      switch (instance.type) {
        case 'text': {
          switch (instance.name) {
            case 'title': {
              await instance.createWidgetElementText(
                {
                  color: '#fb8c2b',
                  fontSize: 60,
                  fontWeight: 'bold',
                  fontStyle: 'normal',
                  textShadow:
                    '0px 0px 1px rgb(0 0 0),' +
                    '0px 0px 2px rgb(0 0 0),' +
                    '0px 0px 3px rgb(0 0 0),' +
                    '0px 0px 4px #000,' +
                    '0px 0px 5px rgb(0 0 0)',
                },
                {
                  transaction: options.transaction,
                },
              )
              return
            }
            case 'text': {
              await instance.createWidgetElementText(
                {
                  color: '#ffffff',
                  fontSize: 25,
                  fontWeight: 'normal',
                  fontStyle: 'normal',
                  textShadow:
                    '0px 0px 1px rgb(0 0 0),' +
                    '0px 0px 2px rgb(0 0 0),' +
                    '0px 0px 3px rgb(0 0 0),' +
                    '0px 0px 4px #000,' +
                    '0px 0px 5px rgb(0 0 0)',
                },
                {
                  transaction: options.transaction,
                },
              )
              return
            }
            default:
              return
          }
        }

        case 'speach': {
          await instance.createWidgetElementSpeach(
            {},
            {
              transaction: options.transaction,
            },
          )
          return
        }

        default:
          return
      }
    })
  }
}

module.exports = WidgetElement
