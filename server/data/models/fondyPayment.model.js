const { Model, DataTypes } = require('sequelize')

class FondyPayment extends Model {
  static init(sequelize) {
    return super.init(
      {
        orderId: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        amount: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        currency: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        status: {
          type: DataTypes.STRING,
          allowNull: false,
          defaultValue: 'init',
        },
        paymentSystem: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        fondyPaymentId: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        actualAmount: {
          type: DataTypes.DOUBLE,
          allowNull: true,
        },
        actualCurrency: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        challengeId: {
          type: DataTypes.INTEGER,
          allowNull: true,
        }
      },
      {
        sequelize,
        modelName: 'FondyPayment',
        tableName: 'FondyPayment',
      },
    )
  }

  static associate(models) {
    // this.belongsTo(models.Challenge, {
    //   foreignKey: {
    //     name: 'challengeId',
    //     allowNull: false,
    //   },
    //   as: 'Challenge',
    // })
  }
}

module.exports = FondyPayment
