const { Model, DataTypes } = require('sequelize')

class UnitpayPayment extends Model {
  static init(sequelize) {
    return super.init(
      {
        unitpayId: {
          type: DataTypes.STRING,
          allowNull: true,
        },

        status: {
          type: DataTypes.TINYINT(4),
          allowNull: false,
          defaultValue: 0,
        },

        account: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        description: {
          type: DataTypes.STRING,
          allowNull: false,
          defaultValue: '',
        },

        sum: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        currency: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        payerSum: {
          type: DataTypes.DOUBLE,
          allowNull: true,
        },
        payerCurrency: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        profit: {
          type: DataTypes.DOUBLE,
          allowNull: true,
          defaultValue: null,
        },

        dateCreate: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        dateComplete: {
          type: DataTypes.DATE,
          allowNull: true,
          defaultValue: null,
        },

        isRefund: {
          type: DataTypes.TINYINT(1),
          allowNull: false,
          defaultValue: 0,
        },

        isTest: {
          type: DataTypes.TINYINT(1),
          allowNull: false,
          defaultValue: 0,
        },
      },
      {
        sequelize,
        modelName: 'UnitpayPayment',
        tableName: 'UnitpayPayment',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.Challenge, {
      foreignKey: {
        name: 'challengeId',
        allowNull: false,
      },
      as: 'Challenge',
    })
  }

  setCheckedStatus() {
    this.status = 1
    return this.save()
  }

  setCompleteStatus() {
    this.status = 2
    this.dateComplete = new Date()
    return this.save()
  }

  setUnitpayId({ unitpayId }) {
    this.unitpayId = unitpayId
    return this.save()
  }

  setProfit({ profit }) {
    if (typeof profit === 'string') {
      profit = parseFloat(profit)
    }
    this.profit = profit
    return this.save()
  }

  setPayer({ payerSum, payerCurrency }) {
    if (typeof payerSum === 'string') {
      payerSum = parseFloat(payerSum)
    }
    this.payerSum = payerSum
    this.payerCurrency = payerCurrency
    return this.save()
  }
}

module.exports = UnitpayPayment
