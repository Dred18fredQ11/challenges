const { Model, DataTypes } = require('sequelize')

class WidgetElementText extends Model {
  static init(sequelize) {
    return super.init(
      {
        fontFamily: {
          type: DataTypes.STRING,
          allowNull: false,
          defaultValue: 'Roboto',
        },
        color: {
          type: DataTypes.STRING,
          allowNull: false,
          defaultValue: '#ffffff',
        },
        fontSize: {
          type: DataTypes.SMALLINT,
          allowNull: false,
          defaultValue: 25,
        },
        fontWeight: {
          type: DataTypes.ENUM('light', 'normal', 'bold'),
          allowNull: false,
          defaultValue: 'normal',
        },
        fontStyle: {
          type: DataTypes.ENUM('normal', 'italic'),
          allowNull: false,
          defaultValue: 'normal',
        },
        textShadow: {
          type: DataTypes.STRING,
          allowNull: false,
          defaultValue: '0px 0px 1px rgb(0 0 0),'
            + '0px 0px 2px rgb(0 0 0),'
            + '0px 0px 3px rgb(0 0 0),'
            + '0px 0px 4px #000,'
            + '0px 0px 5px rgb(0 0 0)',
        },
      },
      {
        sequelize,
        modelName: 'WidgetElementText',
        tableName: 'WidgetElementText',
      },
    )
  }

  static associate(models) {
    this.belongsTo(
      models.WidgetElement,
      {
        foreignKey: {
          name: 'widgetElementId',
          allowNull: false,
        },
        as: 'WidgetElement',
      },
    )
  }
}

module.exports = WidgetElementText
