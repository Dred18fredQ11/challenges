const { Model, DataTypes, QueryTypes } = require('sequelize')
const { nanoid } = require('nanoid')

class User extends Model {
  static init(sequelize) {
    this.sequelize = sequelize
    return super.init(
      {
        twitchId: {
          type: DataTypes.STRING,
        },
        email: {
          type: DataTypes.STRING,
        },
        login: {
          type: DataTypes.STRING,
        },
        displayName: {
          type: DataTypes.STRING,
        },
        profileImageUrl: {
          type: DataTypes.STRING,
        },
        balance: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 0,
        },
        minRubSum: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 50,
        },
        minUsdSum: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 1,
        },
        minUahSum: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          defaultValue: 1,
        },
        chatbotIsActive: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: 1,
        },
        chatbotFrequency: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 60000,
        }
      },
      {
        sequelize,
        modelName: 'User',
        tableName: 'User',
      },
    )
  }

  static associate(models) {
    this.hasMany(models.Challenge, {
      foreignKey: {
        name: 'performerId',
        allowNull: false,
      },
      as: 'Challenge',
    })

    this.hasMany(models.WidgetApp, {
      foreignKey: {
        name: 'userId',
        allowNull: false,
      },
      as: 'WidgetApp',
    })
  }

  static initHooks(models) {
    this.addHook('afterCreate', async (instance, options) => {
      await instance.createWidgetApp(
        {
          token: nanoid(),
          type: 'mainWidgetApp',
        },
        {
          transaction: options.transaction,
        },
      )

      await instance.createWidgetApp(
        {
          token: nanoid(),
          name: 'notification',
          type: 'notification',
        },
        {
          transaction: options.transaction,
        },
      )
    })
  }

  static async isExists({ id }) {
    const query = await this.sequelize.query(
      'SELECT EXISTS(SELECT id FROM `User` WHERE id=?) AS isExists',
      {
        replacements: [id],
        type: QueryTypes.SELECT,
        plain: true,
        raw: true,
      },
    )
    return Boolean(query?.isExists)
  }

  putBalance({ sum }) {
    this.balance += sum
    return this.save()
  }

  withdrawBalance({ sum }) {
    if (sum > this.balance) {
      throw new Error('Sum greater than balance')
    }
    this.balance -= sum
    return this.save()
  }

  setMinSum({ currency, sum }) {
    const currencyDict = {
      RUB: 'minRubSum',
      USD: 'minUsdSum',
      UAH: 'minUahSum',
    }
    if (!currencyDict[currency]) {
      throw new Error('Unknown currency')
    }
    this[currencyDict[currency]] = sum
    return this.save()
  }
}

module.exports = User
