const { Model, DataTypes } = require('sequelize')

class Challenge extends Model {
  static init(sequelize) {
    return super.init(
      {
        title: {
          type: DataTypes.STRING,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        creatorName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        status: {
          type: DataTypes.ENUM(
            'pending',
            'approved',
            'in progress',
            'done',
            'payed',
            'failed',
            'declined',
          ),
          defaultValue: 'pending',
          allowNull: false,
        },
        isHidden: {
          type: DataTypes.TINYINT(1),
          defaultValue: 1,
          allowNull: false,
        },
        speachFileUrl: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        speachFileUrlCreatedDate: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        isTest: {
          type: DataTypes.TINYINT(0),
          allowNull: false,
          defaultValue: 0,
        },
      },
      {
        sequelize,
        modelName: 'Challenge',
        tableName: 'Challenge',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: {
        name: 'performerId',
        allowNull: false,
      },
      as: 'Performer',
    })

    this.hasMany(models.WidgetChallenge, {
      foreignKey: {
        name: 'challengeId',
        allowNull: true,
      },
      as: 'Widget',
    })

    // this.hasOne(models.FondyPayment, {
    //   foreignKey: {
    //     name: 'challengeId',
    //     allowNull: false,
    //   },
    //   as: 'Payment',
    // })

    // this.hasOne(models.FreeKassaPayment, {
    //   foreignKey: {
    //     name: 'challengeId',
    //     allowNull: false,
    //   },
    //   as: 'Payment',
    // })

    this.hasOne(models.UnitpayPayment, {
      foreignKey: {
        name: 'challengeId',
        allowNull: false,
      },
      as: 'Payment',
    })
  }

  hide() {
    this.isHidden = 1
    return this.save()
  }

  view() {
    this.isHidden = 0
    return this.save()
  }

  updateStatus({ newStatus }) {
    this.status = newStatus
    return this.save()
  }
}

module.exports = Challenge
