const { Model, DataTypes, Sequelize } = require('sequelize')

class WidgetApp extends Model {
  static init(sequelize) {
    return super.init(
      {
        name: {
          type: DataTypes.STRING,
          defaultValue: 'main',
          allowNull: false,
        },
        token: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        type: {
          type: DataTypes.ENUM('mainWidgetApp', 'notification'),
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'WidgetApp',
        tableName: 'WidgetApp',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: {
        name: 'userId',
        allowNull: false,
      },
      as: 'User',
    })

    this.hasMany(models.Widget, {
      foreignKey: {
        name: 'widgetAppId',
        allowNull: false,
      },
      as: 'Widget',
    })
  }

  static initHooks(models) {
    this.addHook('afterCreate', async (instance, options) => {
      switch (instance.type) {
        case 'mainWidgetApp': {
          await instance.createWidget(
            {
              name: 'challengeBanner',
              displayName: 'Challenge Banner',
              type: 'challengeBanner',
            },
            {
              transaction: options.transaction,
            },
          )
          return
        }
        case 'notification': {
          await instance.createWidget(
            {
              name: 'challengeNotification',
              displayName: 'Challenge Notification',
              type: 'challengeNotification',
            },
            {
              transaction: options.transaction,
            },
          )
        }
        default:
          return
      }
    })
  }
}

module.exports = WidgetApp
