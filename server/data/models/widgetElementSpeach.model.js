const { Model, DataTypes } = require('sequelize')

class WidgetElementSpeach extends Model {
  static init(sequelize) {
    return super.init(
      {
        voiceId: {
          type: DataTypes.ENUM('Maxim', 'Tatyana'),
          allowNull: false,
          defaultValue: 'Maxim',
        },
      },
      {
        sequelize,
        modelName: 'WidgetElementSpeach',
        tableName: 'WidgetElementSpeach',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.WidgetElement, {
      foreignKey: {
        name: 'widgetElementId',
        allowNull: false,
      },
      as: 'WidgetElement',
    })
  }
}

module.exports = WidgetElementSpeach
