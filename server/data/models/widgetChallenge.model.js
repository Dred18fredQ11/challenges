const { Model } = require('sequelize')

class WidgetChallenge extends Model {
  static init(sequelize) {
    return super.init(
      {},
      {
        sequelize,
        modelName: 'WidgetChallenge',
        tableName: 'WidgetChallenge',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.Challenge, {
      foreignKey: {
        name: 'challengeId',
        allowNull: true,
      },
      as: 'Challenge',
    })

    this.belongsTo(models.Widget, {
      foreignKey: {
        name: 'widgetId',
        allowNull: true,
      },
      as: 'Widget',
    })
  }
}

module.exports = WidgetChallenge
