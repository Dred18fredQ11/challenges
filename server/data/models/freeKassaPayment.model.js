const { Model, DataTypes } = require('sequelize')

class FreeKassaPayment extends Model {
  static init(sequelize) {
    return super.init(
      {
        sum: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        paidSum: {
          type: DataTypes.DOUBLE,
          allowNull: true,
        },
        orderId: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        currency: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        paidCurrency: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        freeKassaId: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        status: {
          type: DataTypes.TINYINT(4),
          allowNull: false,
          defaultValue: 0,
        },
        dateComplete: {
          type: DataTypes.DATE,
          allowNull: true,
        }
      },
      {
        sequelize,
        modelName: 'FreeKassaPayment',
        tableName: 'FreeKassaPayment',
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.Challenge, {
      foreignKey: {
        name: 'challengeId',
        allowNull: false,
      },
      as: 'Challenge',
    })
  }

  setCheckedStatus() {
    this.status = 1
    return this.save()
  }

  setCompleteStatus() {
    this.status = 2
    this.dateComplete = new Date()
    return this.save()
  }

  setErrorStatus() {
    this.status = 3
    return this.save()
  }

  setFreeKassaData({ freeKassaId, paidSum, paidCurrency }) {
    this.freeKassaId = freeKassaId
    this.paidSum = paidSum
    this.paidCurrency = paidCurrency
    return this.save()
  }
}

module.exports = FreeKassaPayment
