const { parse } = require('url')
const path = require('path')
const express = require('express')
const next = require('next')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
// const rateLimit = require('express-rate-limit')
const requestIp = require('request-ip')

const helmet = require('helmet')

const config = require('./config')
const port = config.port || 3000
const dev = process.env.NODE_ENV !== 'production'

const passport = require('./libs/passport')

// Sequelize instance
const { sequelize } = require('./data')

// Socket handlers
const registerWidgetHandler = require('./handlers/widget')

/**
 * Starts web server app
 */
async function start() {
  const server = express()
  const http = require('http').Server(server)

  // Proxy
  server.set('trust proxy', 1)

  // Socket.IO
  const io = require('socket.io')(http, { cors: {
    origin: "http://localhost:3002",
    methods: ["GET", "POST"]
  }}) // TODO: FOR PRODUCTION
  server.use((req, res, next) => {
    req.io = io
    next()
  })
  io.on('connection', async socket => {
    registerWidgetHandler(io, socket)
  })

  // Middleware
  server.use(bodyParser.json())
  server.use(bodyParser.urlencoded({ extended: true }))
  server.use(cookieParser())
  server.use(passport.initialize())
  server.use(requestIp.mw())
  // server.use(helmet())

  // API Routes
  server.use('/api/auth', require('./routes/auth.routes'))
  server.use('/api/widget', require('./routes/widget.routes'))
  server.use('/api/challenge', require('./routes/challenge.routes'))
  server.use('/api/broadcaster', require('./routes/broadcaster.routes'))

  // server.use('/api/user', require('./routes/user'))
  // server.use('/api/payment', require('./routes/payment'))

  // Adminpanel Client
  server.use(
    '/adminpanel',
    express.static(path.join(__dirname, '../adminpanel', 'build'))
  )
  server.get('/adminpanel', (req, res) => {
    res.sendFile(path.join(__dirname, '../adminpanel', 'build', 'index.html'))
  })
  server.get('/adminpanel/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../adminpanel', 'build', 'index.html'))
  })

  // Widget Client
  server.use('/widget', express.static(path.join(__dirname, '../widget/build')))
  server.get('/widget', (req, res) => {
    res.sendFile(path.join(__dirname, '../widget/build', 'index.html'))
  })

  // Next JS Client
  if (!process.env.NO_CLIENT) {
    const nextApp = next({ dev, dir: './client' })
    await nextApp.prepare()
    const handle = nextApp.getRequestHandler()
    server.get('*', (req, res) => {
      const parsedUrl = parse(req.url, true)
      return handle(req, res, parsedUrl)
    })
  }

  // Error middleware handler
  server.use(require('./utils/errors/handler'))

  http.listen(port, async err => {
    if (err) throw err
    await sequelize.authenticate()
    // await sequelize.sync({ force: true })
    if (process.env.SEQ_FORCE) {
      await sequelize.drop()
      await sequelize.sync({ force: true })
    }
    console.log(`Server is listening at port: ${port}`)
  })
}

start()
