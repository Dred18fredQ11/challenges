/**
 * @typedef {import('http').ServerResponse} ServerResponse
 */

/**
 * Make success response
 * @param {ServerResponse} res server response instance
 * @param {any} data data to response
 * @returns {ServerResponse} server response
 */
exports.handleSuccessResponse = function handleSuccessResponse(res, data) {
  return res.status(200).json({ message: 'Ok', data })
}
