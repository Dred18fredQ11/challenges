const { NotFoundError } = require('./index')

module.exports = function ErrorHandler(err, req, res, next) {
  console.log(err)
  switch (err.constructor) {
    case NotFoundError: {
      return res.status(404).json({
        message: 'Not Found',
        data: {
          description: err.toString(),
        },
      })
    }
    default: {
      return res.status(500).json({
        message: 'Server Error',
        data: {
          description: err.toString(),
        },
      })
    }
  }
}
