exports.NotFoundError = class NotFoundError extends Error {}
exports.PaymentSumNotMatchError = class PaymentSumNotMatchError extends Error {}

module.exports = { ...require('./classes/auth.errors') }
