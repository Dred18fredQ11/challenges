const router = require('express').Router()
const {
  checkUnitpayPayment,
  paidUnitpayPayment,
  findPaymentTarget,
  formPaymentUrl,
  handlePaymentNotification,
} = require('../services/payment')

const { initPayment, findPayment } = require('../services/payment')
const { viewChallenge } = require('../services/challenge.services');

// const { viewChallenge } = require('../services/challenge')
// const {
//   UnitpaySuccessResponse,
//   UnitpayErrorResponse,
// } = require('../responses/payment')
const { userResponse } = require('../responses/user')

// router.get('/test', async (req, res) => {
//   const currency = 'UAH'
//   const amount = 100

//   const { payment, redirectUrl } = await initPayment({ currency, amount })
//   return res.status(200).json({ msg: 'ok', redirectUrl })
// })

// // POST /api/payment/handler
// router.post('/handler', async (req, res) => {
//   try {
//     const {
//       order_id: orderId,
//       order_status: orderStatus,
//       amount,
//     } = req.body
//     const payment = await findPayment({ orderId })
//     if (payment.amount !== amount) {
//       return res.status(400).json({ message: 'Amount not match' })
//     }
//     return res.status(200).json({ message: 'Ok' })
//   } catch(err) {
//     return res.status(500).json({ message: 'error' })
//   }
// })

//https://dropchallenge.com/api/payment/handlerChargeback


// // GET /api/payment/handler
// router.get('/handler', async (req, res, next) => {
//   try {
//     console.log('handle', req.query)
//     const {
//       MERCHANT_ID,
//       AMOUNT,
//       intid,
//       MERCHANT_ORDER_ID,
//       P_EMAIL,
//       P_PHONE,
//       CUR_ID,
//       SIGN,
//       us_orderId,
//     } = req.query

//     const payment = await handlePaymentNotification({
//       merchantId: MERCHANT_ID,
//       freeKassaId: intid,
//       orderId: MERCHANT_ORDER_ID,
//       paidSum: AMOUNT,
//       paidCurrency: CUR_ID,
//     })

//     const challengeId = payment.challengeId
//     await viewChallenge({ challengeId })

//     return res.status(200).send('YES')
//   } catch (err) {
//     console.log('handler', err)
//     return res.status(200).send('Error occurs')
//   }
// })

// router.get('/test', async (req, res) => {
//   console.log(formPaymentUrl({ sum: 1, orderId: 444 }))
//   return res.status(200).send('YES')
// })



function UnitpaySuccessResponse() {
  return {
    result: {
      message: 'Запрос успешно обработан',
    },
  }
}

function UnitpayErrorResponse({ message }) {
  return {
    error: {
      message: `Ошибка платежа ${message ?? ''}`,
    },
  }
}



// GET /api/payment/handler - Handler for UNITPAY
router.get('/handler', async (req, res, next) => {
  try {
    const { method, params } = req.query
    console.log('handler', method, params)
    switch (method) {
      case 'check': {
        await checkUnitpayPayment(params)
        return res.status(200).json(UnitpaySuccessResponse())
      }
      case 'pay': {
        const payment = await paidUnitpayPayment(params)
        const challengeId = payment.challengeId
        await viewChallenge({ challengeId })

        try {
          // TODO: add notification service for widget
          const broadcaster = await findPaymentTarget({
            account: payment.account,
          })

          // const widgetAppList = await findWidgetAppList({
          //   userId: broadcaster.id,
          // })
          // const widgetApp = widgetAppList[0]
          // const token = widgetApp?.token

          const token = await findWidgetAppToken({ userId: broadcasterId })

    const isRoomActive = Boolean(
      req.io.to(`widget:${token}`)?.adapter?.rooms?.size
    )

    if (isRoomActive) {
      const speachUrl = await getChallengeSpeach({ challenge })
      req.io.to(`widget:${token}`).emit('widget:notification', {
        creatorName: challenge.creatorName,
        description: challenge.description,
        speachUrl,
      })
    }

          // req.io.to(`widget:${token}`).emit('widget:sound')
        } catch(err) {
          console.log(err)
        }

        return res.status(200).json(UnitpaySuccessResponse())
      }
      case 'error': {
        return res.status(200).json(UnitpayErrorResponse())
      }
      default: {
        return res.status(200).json(UnitpayErrorResponse())
      }
    }
  } catch (err) {
    return res.status(200).json(UnitpayErrorResponse({ message: err.message }))
  }
})

// GET /api/payment/payment/:account/target
router.get('/payment/:orderId/target', async (req, res, next) => {
  try {
    const { orderId } = req.params
    const user = await findPaymentTarget({ orderId })
    return res.status(200).json({ message: 'Ok', data: userResponse({ user }) })
  } catch (err) {
    return next(err)
  }
})

// // TEST ROUTE
// const { nanoid } = require('nanoid')
// const { sha256 } = require('js-sha256')
// const config = require('../config')
// router.get('/test', async (req, res, next) => {
//   try {
//     const account = nanoid()
//     const sum = 1
//     const desc = 'MY_PAYMENT_DESCRIPTION'

//     const payment = await makePayment({
//       account,
//       sum,
//       desc,
//       clientIp: req.clientIp,
//       challengeId: 1,
//     })

//     return res.redirect(payment.redirectUrl)
//   } catch (err) {
//     return next(err)
//   }
// })

// // TEST ROUTE
// router.get('/test2', (req, res) => {
//   const account = 'order413'
//   const sum = 10
//   const desc = 'MY_PAYMENT_DESCRIPTION'
//   const message = [account, desc, sum, config.unitpay.secretKey].join('{up}')

//   const signature = sha256(message)

//   const url =
//     'https://unitpay.money/api?' +
//     'method=initPayment' +
//     `&params[test]=1` +
//     `&params[paymentType]=card` +
//     `&params[account]=${account}` +
//     `&params[sum]=${sum}` +
//     `&params[projectId]=${config.unitpay.projectId}` +
//     `&params[resultUrl]=http://localhost:3000/payment/success` +
//     `&params[desc]=${desc}` +
//     `&params[ip]=${req.clientIp}` +
//     `&params[secretKey]=${config.unitpay.apiSecretKeyTest}` +
//     `&params[signature]=${signature}`

//   return res.redirect(url)
// })

module.exports = router
