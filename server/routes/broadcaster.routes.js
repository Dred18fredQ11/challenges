const router = require('express').Router()
const { findUser, serializeUser } = require('../services/user.services')
const { handleSuccessResponse } = require('../utils/response')

// GET /api/broadcaster/:broadcasterLogin
router.get(
  '/:broadcasterLogin',
  async (req, res, next) => {
    try {
      const { broadcasterLogin } = req.params
      const broadcaster = await findUser({ login: broadcasterLogin })
      return handleSuccessResponse(res, serializeUser({ user: broadcaster }))
    } catch(err) {
      return next(err)
    }
  }
)

module.exports = router
