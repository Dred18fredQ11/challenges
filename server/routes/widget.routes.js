const router = require('express').Router()
const authMiddleware = require('../middleware/auth.middleware')
const {
  findWidgetApp,
  findWidgetAppToken,
  findWidgetAppList,
  serializeWidgetApp,
  serializeWidgetAppList,

  findWidget,
  findWidgetList,
  setWidgetChallenge,
  removeWidgetChallenge,
  serializeWidget,
  serializeWidgetList,

  findWidgetElement,
  findWidgetElementList,
  serializeWidgetElementList,
  updateWidgetElement,
  serializeWidgetElement,
} = require('../services/widget.services')
const { findChallenge } = require('../services/challenge.services')
const { handleSuccessResponse } = require('../utils/response')

// GET /api/widget/appList
router.get('/appList', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const widgetAppList = await findWidgetAppList({ userId })
    return handleSuccessResponse(res, serializeWidgetAppList({ widgetAppList }))
  } catch (err) {
    return next(err)
  }
})

// GET /api/widget/app/:widgetAppId
router.get('/app/:widgetAppId', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { widgetAppId } = req.params
    const widgetApp = await findWidgetApp({ widgetAppId, userId })
    return handleSuccessResponse(res, serializeWidgetApp({ widgetApp }))
  } catch (err) {
    return next(err)
  }
})

// GET /api/widget/widgetList
// GET /api/widget/widgetList?widgetAppId=id
// GET /api/widget/widgetList?widgetAppId[]=id1&widgetAppId[]=id2
router.get('/widgetList', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { widgetAppId } = req.query
    const widgetList = await findWidgetList({ userId, widgetAppId })
    return handleSuccessResponse(res, serializeWidgetList({ widgetList }))
  } catch (err) {
    return next(err)
  }
})

// PUT /api/widget/widget/:widgetId/show
router.put('/widget/:widgetId/show', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { widgetId } = req.params
    const { challengeId } = req.body
    const widget = await findWidget({ userId, widgetId })
    await setWidgetChallenge({ widget, challengeId })

    const token = await findWidgetAppToken({ userId, name: 'main' })
    const challenge = await findChallenge({ userId, challengeId })
    req.io.to(`widget:${token}`).emit('widget:show', {
      description: challenge.description,
      creatorName: challenge.creatorName,
    })

    return handleSuccessResponse(res, serializeWidget({ widget }))
  } catch (err) {
    return next(err)
  }
})

// PUT /api/widget/widget/:widgetId/hide
router.put('/widget/:widgetId/hide', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { widgetId } = req.params
    const widget = await findWidget({ userId, widgetId })
    await removeWidgetChallenge({ widget })

    const token = await findWidgetAppToken({ userId, name: 'main' })
    req.io.to(`widget:${token}`).emit('widget:hide')

    return handleSuccessResponse(res, serializeWidget({ widget }))
  } catch (err) {
    return next(err)
  }
})

// GET /api/widget/elementList
// GET /api/widget/elementList?widgetAppId=id
// GET /api/widget/elementList?widgetAppId[]=id1&widgetAppId[]=id2
// GET /api/widget/elementList?widgetId=id
// GET /api/widget/elementList?widgetId[]=id1&widgetId[]=id2
router.get('/elementList', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { widgetId, widgetAppId } = req.query
    const elementList = await findWidgetElementList({
      userId,
      widgetId,
      widgetAppId,
    })
    return handleSuccessResponse(
      res,
      serializeWidgetElementList({
        elementList,
      }),
    )
  } catch (err) {
    return next(err)
  }
})

// PUT /api/widget/element/:elementId
router.put('/element/:elementId', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { elementId } = req.params
    const { elementProps } = req.body
    const element = await findWidgetElement({ elementId, userId })
    await updateWidgetElement({ element, elementProps })
    return handleSuccessResponse(res, serializeWidgetElement({ element }))
  } catch (err) {
    return next(err)
  }
})

module.exports = router
