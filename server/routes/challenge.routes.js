const router = require('express').Router()
const authMiddleware = require('../middleware/auth.middleware')
const {
  findChallenge,
  findChallengeList,
  findChallengeCount,
  assignChallenge,
  updateChallengeStatus,
  hideChallenge,
  getChallengeSpeach,
  serializeChallenge,
  serializeChallengeList,
} = require('../services/challenge.services')
const { findWidgetAppToken } = require('../services/widget.services')
const { initUnitpayPayment, makeUnitpayPaymentRequest } = require('../services/payment');
const { handleSuccessResponse } = require('../utils/response')
const { nanoid } = require('nanoid')

// GET /api/challenge/challenge/:challengeId
router.get(
  '/challenge/:challengeId',
  authMiddleware,
  async (req, res, next) => {
    try {
      const { id: userId } = req.user
      const { challengeId } = req.params
      const challenge = await findChallenge({ challengeId, userId })
      return handleSuccessResponse(res, serializeChallenge({ challenge }))
    } catch (err) {
      return next(err)
    }
  },
)

// GET /api/chellenge/count
router.get('/count', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const count = await findChallengeCount({ userId })
    return res.status(200).json({ message: 'Ok', data: count })
  } catch (err) {
    return next(err)
  }
})

// GET /api/challenge/challengeList?offset&limit
router.get('/challengeList', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { offset, limit } = req.query
    const challengeList = await findChallengeList({
      userId,
      offset,
      limit,
    })
    return handleSuccessResponse(res, serializeChallengeList({ challengeList }))
  } catch (err) {
    return next(err)
  }
})

// POST /api/challenge
router.post(
  '/',
  // challengeAssignLimiter,
  async (req, res, next) => {
    try {
      const { broadcasterId, nickname, sum, currency, description } = req.body
      const challenge = await assignChallenge({
        userId: broadcasterId,
        nickname,
        description,
      })

      // const redirectUrl = '/'

      // if (parseFloat(sum) < 50) throw new Error()

      // const { payment, url: redirectUrl } = await initPayment({
      //   sum,
      //   currency: 94,
      //   challengeId: challenge.id,
      // })

      const payment = await initUnitpayPayment({
        account: nanoid(),
        sum,
        currency,
        challengeId: challenge.id,
        description: 'Челлендж для стримера',
      })
      const { resData } = await makeUnitpayPaymentRequest({
        payment,
        clientIp: req.clientIp,
      })
      const { redirectUrl } = resData

      return res.status(200).json({ message: 'Ok', data: { redirectUrl } })
    } catch (err) {
      return next(err)
    }
  },
)

// POST /api/challenge/test
router.post('/test', async (req, res, next) => {
  try {
    const { broadcasterId, nickname, sum, currency, description } = req.body
    const challenge = await assignChallenge(
      {
        userId: broadcasterId,
        nickname,
        description,
      },
      { isTest: true },
    )
    const redirectUrl = 'http://localhost:3000/payment-redirect?' +
      'paymentId=2028386417&account=DbcmqbRl_kKJHTRRNwYPm'

    const token = await findWidgetAppToken({ userId: broadcasterId })

    const isRoomActive = Boolean(
      req.io.to(`widget:${token}`)?.adapter?.rooms?.size
    )

    if (isRoomActive) {
      const speachUrl = await getChallengeSpeach({ challenge })
      req.io.to(`widget:${token}`).emit('widget:notification', {
        creatorName: challenge.creatorName,
        description: challenge.description,
        speachUrl,
      })
    }

    return res.status(200).json({ message: 'Ok', data: { redirectUrl } })
  } catch (err) {
    return next(err)
  }
})

// POST /api/challenge/challenge/:challengeId/speach
router.post(
  '/challenge/:challengeId/speach',
  authMiddleware,
  async (req, res, next) => {
    try {
      const { id: userId } = req.user
      const { challengeId } = req.params
      const challenge = await findChallenge({ challengeId, userId })
      const url = await getChallengeSpeach({ challenge })
      const token = await findWidgetAppToken({ userId })
      req.io.to(`widget:${token}`).emit('widget:speach', { url })
      return res.status(200).json({ message: 'Ok', data: { url } })
    } catch (err) {
      return next(err)
    }
  },
)

// PUT /api/challenge/challenge/:challengeId/hide
router.put(
  '/challenge/:challengeId/hide',
  authMiddleware,
  async (req, res, next) => {
    try {
      const { id: userId } = req.user
      const { challengeId } = req.params
      const challenge = await findChallenge({ challengeId, userId })
      await hideChallenge({ challenge })
      const token = await findWidgetAppToken({ userId })
      req.io.to(`widget:${token}`).emit('widget:hide')
      return res.status(200).json({ message: 'Ok' })
    } catch (err) {
      return next(err)
    }
  },
)

// PUT /api/challenge/challenge/:challengeId/updateStatus
router.put(
  '/challenge/:challengeId/updateStatus',
  authMiddleware,
  async (req, res, next) => {
    try {
      const { id: userId } = req.user
      const { challengeId } = req.params
      const { newStatus } = req.body
      const challenge = await findChallenge({ challengeId, userId })
      await updateChallengeStatus({ challenge, newStatus })

      switch (newStatus) {
        case 'done': {
          break
        }
        case 'failed':
        case 'declined': {
          break
        }
        default: break
      }

      const data = serializeChallenge({ challenge })

      // const challenge = await updateChallengeStatus({
      //   userId,
      //   challengeId,
      //   newStatus,
      // })
      // const data = challengeResponse({ challenge })

      // TODO: refactor
      // if (newStatus === 'done') {
      //   console.log(userId + 'CHALLENGE IS DONE')
      //   // TODO: Add halt workflow
      //   let payment
      //   try {
      //     payment = await findPayment({ challengeId })
      //   } catch (err) {
      //     console.log('transaction', err)
      //     return res.status(200).json({ message: 'Ok', data })
      //   }
      //   const user = await findUser({ userId })
      //   await putOnUserBalance({ user, payment })
      //   console.log(`${user.login} transaction: ${payment?.sum}`)
      // }

      // if (newStatus === 'failed' || newStatus === 'declined') {
      //   console.log(userId + 'CHALLENGE IS DECLINED/FAILED')
      //   let payment
      //   try {
      //     payment = await findPayment({ challengeId })
      //     // await refundUnitpayPayment({
      //     //   sum: payment.profit,
      //     //   unitpayId: payment.unitpayId,
      //     // })
      //   } catch (err) {
      //     console.log('refund', err)
      //     return res.status(200).json({ message: 'Ok', data })
      //   }
      //   console.log(`${user.login} REFUND: ${payment?.profit}`)
      // }

      return res.status(200).json({ message: 'Ok', data })
    } catch (err) {
      return next(err)
    }
  },
)

module.exports = router
