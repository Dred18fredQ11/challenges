const router = require('express').Router()
const authMiddleware = require('../middleware/auth.middleware')
const {
  formTwitchAuthUrl,
  handleTwitchCallback,
  signAuthJWT,
} = require('../services/auth.services')
const { findUser, serializeUser } = require('../services/user.services')
const { handleSuccessResponse } = require('../utils/response')

// GET /api/auth
router.post('/', authMiddleware, async (req, res) => {
  try {
    const { id, login } = req.user
    const user = await findUser({ userId: id, login })
    return handleSuccessResponse(res, serializeUser({ user }, { isFull: true }))
  } catch(err) {
    return next(err)
  }
})

// GET /api/auth/twitch
router.get('/twitch', (req, res) => {
  const url = formTwitchAuthUrl()
  return res.redirect(url)
})

// GET /api/auth/twitch/callback
router.get('/twitch/callback', async (req, res, next) => {
  try {
    const { code } = req.query
    const { user } = await handleTwitchCallback({ code })
    const token = signAuthJWT({ user })
    return res.redirect(`/auth?token=${token}`)
  } catch (err) {
    return next(err)
  }
})

// GET /api/auth/logout
router.post('/logout', (req, res) => {
  res.status(200).json({ message: 'Ok' })
})

module.exports = router