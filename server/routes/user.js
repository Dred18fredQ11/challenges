const router = require('express').Router()
const authMiddleware = require('../middleware/auth.middleware')
const { setMimimumPaymentSum, findUser } = require('../services/user')
const { userResponse } = require('../responses/user')

// PUT /api/user/minPayment
router.put('/minPayment', authMiddleware, async (req, res, next) => {
  try {
    const { id: userId } = req.user
    const { sum, currency } = req.body
    const user = await findUser({ userId })
    const newUser = await setMimimumPaymentSum({ user, currency, sum })
    return res
      .status(200)
      .json({ message: 'Ok', data: userResponse({ user: newUser }) })
  } catch (err) {
    return next(err)
  }
})

module.exports = router
