# DROPCHALLENGE APP

- /server - NodeJS server
- /client - NextJS main-website front-end
- /adminpanel - ReactJS adminpanel front-end

## `.env` template

PORT=3000

SESSION_SECRET=""

DB_SCHEMA_NAME=""
DB_USER=""
DB_PASSWORD=""

TWITCH_CLIENT_ID=""
TWITCH_SECRET=""
TWITCH_CALLBACK_URL="http://localhost:3000/api/auth/twitch/callback"

PAYPAL_CLIENT_ID=""
PAYPAL_SECRET=""
