#!/bin/bash

# Build Adminpanel App
(
  cd adminpanel;
  npm run build;
)

# Build Widget App
(
  cd widget;
  npm run build;
)

# Build Main App
(
  npm run build;
)

