#!/bin/bash

# Install modules for Adminpanel App
(
  cd adminpanel;
  npm install;
)

# Install modules for Widget App
(
  cd widget;
  npm install;
)

# Install modules for Main App
(
  npm install;
)
